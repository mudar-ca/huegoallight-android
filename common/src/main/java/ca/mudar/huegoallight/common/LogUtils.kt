/*
 * Copyright 2014 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * Modifications:
 * -Imported from iosched 2015
 * -Changed package name
 */
package ca.mudar.huegoallight.common

import android.util.Log
import com.google.firebase.crashlytics.FirebaseCrashlytics

object LogUtils {
    private const val LOG_PREFIX = "hgl_"
    private const val LOG_PREFIX_LENGTH = LOG_PREFIX.length
    private const val MAX_LOG_TAG_LENGTH = 23

    @JvmStatic
    fun makeLogTag(str: String): String {
        return if (str.length > MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH) {
            LOG_PREFIX + str.substring(
                0,
                MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH - 1
            )
        } else LOG_PREFIX + str
    }

    fun LOGV(tag: String?, message: String?) {
        if (Log.VERBOSE >= BuildConfig.LOGGING_LEVEL) {
            Log.v(tag, message!!)
        }
    }

    fun LOGV(tag: String?, message: String?, cause: Throwable?) {
        if (Log.VERBOSE >= BuildConfig.LOGGING_LEVEL) {
            Log.v(tag, message, cause)
        }
    }

    fun LOGD(tag: String?, message: String?) {
        if (Log.DEBUG >= BuildConfig.LOGGING_LEVEL) {
            Log.d(tag, message!!)
        }
    }

    fun LOGD(tag: String?, message: String?, cause: Throwable?) {
        if (Log.DEBUG >= BuildConfig.LOGGING_LEVEL) {
            Log.d(tag, message, cause)
        }
    }

    fun LOGI(tag: String?, message: String?) {
        if (Log.INFO >= BuildConfig.LOGGING_LEVEL) {
            Log.i(tag, message!!)
        }
    }

    fun LOGI(tag: String?, message: String?, cause: Throwable?) {
        if (Log.INFO >= BuildConfig.LOGGING_LEVEL) {
            Log.i(tag, message, cause)
        }
    }

    fun LOGW(tag: String?, message: String?) {
        if (Log.WARN >= BuildConfig.LOGGING_LEVEL) {
            Log.w(tag, message!!)
        }
    }

    fun LOGW(tag: String?, message: String?, cause: Throwable?) {
        if (Log.WARN >= BuildConfig.LOGGING_LEVEL) {
            Log.w(tag, message, cause)
        }
    }

    fun LOGE(tag: String?, message: String?) {
        if (Log.ERROR >= BuildConfig.LOGGING_LEVEL) {
            Log.e(tag, message!!)
        }
    }

    fun LOGE(tag: String?, message: String?, cause: Throwable?) {
        if (Log.ERROR >= BuildConfig.LOGGING_LEVEL) {
            Log.e(tag, message, cause)
        }
    }

    @JvmStatic
    fun REMOTE_LOG(cause: Throwable) {
        if (BuildConfig.USE_CRASHLYTICS) {
            FirebaseCrashlytics.getInstance().recordException(cause)
        }
        cause.printStackTrace()
    }
}
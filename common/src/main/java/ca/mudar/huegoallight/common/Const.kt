/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.common

import android.graphics.Color
import android.os.Build
import android.provider.Settings
import android.text.format.DateUtils
import androidx.annotation.ColorInt
import androidx.annotation.RequiresApi

object Const {
    /**
     * Hue
     */
    const val HUE_APP_NAME = "GoalLightShade"
    const val HUE_SDK_LIB = "huesdk"
    const val HUE_WEBSITE = "https://meethue.com/"
    const val BLINK_SHORT_DURATION = 3 // seconds
    const val HUE_DEFAULT_TRANSITION: Long = 400 // millis
    const val HUE_FAST_TRANSITION: Long = 100 // millis

    /**
     * Hockey
     */
    const val LIVE_REFRESH_DELAY = DateUtils.MINUTE_IN_MILLIS
    const val GAME_EXPIRY = 12 * DateUtils.HOUR_IN_MILLIS
    const val NOTIFY_MAX_DELAY = 3 * DateUtils.MINUTE_IN_MILLIS

    @ColorInt
    const val RED_COLOR = Color.RED

    @ColorInt
    const val ALTERNATE_RGB_COLOR = Color.WHITE

    @ColorInt
    const val DEFAULT_RGB_COLOR = RED_COLOR
    const val EAST_TIMEZONE = "America/New_York" // TBD games are set at Eastern Midnight

    /**
     * Firebase Cloud Messaging (FCM)
     */
    const val FCM_DEFAULT_TOPICS = "global"

    /**
     * SharedPreferences
     */
    const val APP_PREFS_NAME = "app_prefs" // No-backup prefs
    const val USER_PREFS_NAME = "user_prefs"
    const val HUE_PREFS_NAME = "hue_shade_prefs"

    /**
     * Other constants
     */
    const val UNKNOWN_VALUE = -1
    const val EMPTY_NBSP_STRING = "\u00A0"
    const val NOTIFY_DEFAULT_CHANNEL_ID = "HueGoalLight"
    const val NOTIFY_SILENT_CHANNEL_ID = "SilentChannel"
    const val MIME_TYPE_PLAIN_TEXT = "text/plain"
    const val APP_JOB_ID = 1000 // Unique value for all app jobs

    /**
     * Compatibility
     */
    val SUPPORTS_OREO = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O
    val SUPPORTS_NOUGAT = Build.VERSION.SDK_INT >= Build.VERSION_CODES.N

    /**
     * Hockey API
     */
    object ApiPaths {
        const val GET_SEASON_CALENDAR = "calendar"
        const val GET_CLUB_CALENDAR = "calendar/{" + ApiArgs.CLUB + "}"
        const val GET_MULTI_LIVE_GAMES = "live/{" + ApiArgs.GAMES + "}"

        @Deprecated("")
        const val GET_LIVE_GAME = "live" // Of the API's default club
    }

    object ApiArgs {
        const val TOKEN = "LiveApiKey"
        const val CLUB = "club"
        const val GAMES = "games_csv"
        const val LAST_UPDATED_AT = "timestamp"
    }

    object ApiValues {
        const val GAMECENTER_LINK = "gamecenter"
        const val LINK_HREF = "href"
        const val LINK_META = "meta"
    }

    /**
     * TabLayout
     */
    object MainTabs {
        const val _COUNT = 2
        const val CURRENT = 0
        const val UPCOMING = 1
    }

    // Assets
    object LocalAssets {
        const val LICENSE = "gpl-3.0-standalone.html"
    }

    /**
     * Typefaces
     */
    object TypeFaces {
        const val _COUNT = 1
        const val SCOREBOARD = "fonts/hockeyboard.ttf"
    }

    object PrefsNames {
        // Hue Bridge
        const val BRIDGE = "prefs_hue_bridge"
        const val BRIDGE_PRIMARY_LIGHTS = "prefs_%s_selected_lights" // "selected" is for legacy
        const val BRIDGE_SECONDARY_LIGHTS = "prefs_%s_secondary_lights"
        const val BRIDGE_RED_LIGHTS = "prefs_%s_red_lights"
        const val BLINK_DURATION = "prefs_blink_duration"

        // Quiet hours
        const val QUIET_HOURS_ENABLED = "prefs_quiet_hours_enabled"
        const val QUIET_HOURS_START = "prefs_quiet_hours_start"
        const val QUIET_HOURS_END = "prefs_quiet_hours_end"

        // Lights board
        const val LIGHTS_BOARD = "prefs_lights_board"

        // Notifications
        const val CATEGORY_NOTIFICATIONS = "prefs_cat_notify"
        const val RINGTONE = "prefs_ringtone"
        const val HAS_VIBRATION = "prefs_has_vibration"
        const val HAS_NOTIFICATIONS = "prefs_has_notify"
        const val SYSTEM_NOTIFICATIONS = "prefs_system_notify"

        // Advanced
        const val BATTERY_OPTIMIZATION_PERMS = "prefs_battery_optimization_perms"

        // Webhook
        const val WEBHOOK_ENABLED = "prefs_webhook_enabled"
        const val WEBHOOK_METHOD = "prefs_webhook_method"
        const val WEBHOOK_URL = "prefs_webhook_url"
        const val WEBHOOK_AUTH_TYPE = "prefs_webhook_auth_type"
        const val WEBHOOK_TOKEN = "prefs_webhook_token"
        const val WEBHOOK_USERNAME = "prefs_webhook_username"
        const val WEBHOOK_PASSWORD = "prefs_webhook_password"

        // NHL
        const val NHL_FOLLOWED_TEAMS = "prefs_nhl_followed"
        const val NHL_TEAM_PREFIX = "key_nhl_"
        const val IGNORE_OPPONENT = "prefs_ignore_opponent"
        const val SEASON_UPDATED_AT = "season_updated_at"

        // Onboarding
        const val IS_ONBOARDING = "is_onboarding"
        const val IS_LIGHTS_DISCOVERY = "is_lights_discovery"
    }

    object PrefsValues {
        const val RINGTONE_SILENT = ""
        const val DEFAULT_BLINK_DURATION = "6"
        const val DEFAULT_QUIET_HOURS_ENABLED = false
        const val DEFAULT_QUIET_HOURS_START = 22 * 60 // minutesOfDay
        const val DEFAULT_QUIET_HOURS_END = 7 * 60 // minutesOfDay
        const val DEFAULT_WEBHOOK_METHOD = "PUT"
        const val DEFAULT_WEBHOOK_AUTH_TYPE = "no_auth"
        const val WEBHOOK_NO_AUTH = "no_auth"
        const val WEBHOOK_BEARER_TOKEN = "bearer_token"
        const val WEBHOOK_BASIC_AUTH = "basic_auth"
    }

    /**
     * Fragments
     */
    object FragmentTags {
        const val PUSHLINK = "f_pushlink"
        const val BRIDGES = "f_bridges"
        const val HUE_ERROR = "f_hue_error"
        const val LIGHTS_BOARD = "f_lights_board"
        const val LIGHTS_BOARD_HELP = "f_lights_board_help"
        const val GAME_DELAY_HELP = "f_game_delay_help"
        const val TEAMS = "f_teams"
        const val SETTINGS = "f_settings"
        const val SETTINGS_NHL_TEAMS = "f_settings_nhl_teams"
        const val SETTINGS_QUIET_HOURS = "f_settings_quiet_hours"
        const val SETTINGS_WEBHOOK = "f_settings_webhook"
        const val TIME_PICKER = "f_time_picker"
        const val DELAY_SEEKBAR = "f_delay_seekbar"
        const val WEBHOOK_LOGGER = "f_webhook_logger"
    }

    /**
     * Request Codes
     */
    object RequestCodes {
        const val NOTIFY_ID = 0
        const val HUE_SETUP = 10
        const val DELAY_SEEKBAR = 20
        const val RINGTONE_PICKER = 30
        const val IGNORE_BATTERY_OPTIMIZATION_SETTINGS = 40
        const val PLAY_SERVICES_RESOLUTION_REQUEST = 9000
    }

    object IntentNames {
        @RequiresApi(api = Build.VERSION_CODES.O)
        const val APP_NOTIFICATION_SETTINGS = Settings.ACTION_APP_NOTIFICATION_SETTINGS
        const val IGNORE_BATTERY_OPTIMIZATION_SETTINGS =
            Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS
    }

    /**
     * Bundle keys
     */
    object BundleKeys {
        const val IS_ONBOARDING = "is_onboarding"
        const val NB_ACCESS_POINTS = "nb_access_points"
        const val GAME_ID = "game_id"
        const val TIME_PERIOD_FIELD = "time_period_field"
        const val HOUR_OF_DAY = "hour_of_day"
        const val MINUTES = "minutes"
        const val SECONDS = "seconds"
        const val DATA_WRAPPER = "data_wrapper"
        const val MESSAGE = "message"

        // HueSetupActivity
        const val AUTO_CONNECT = "auto_connect"
    }

    /*
     * Possible cases:
     * - Onboarding not found
     * - Onboarding one found, auth required
     * - Onboarding list found
     * - Onboarding rotation: started twice?
     * - Main not found
     * - Main one found, auth required
     * - Main list found
     * - Setup, show list
     */
}

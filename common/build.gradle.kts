plugins {
    id("com.android.library")
    id("kotlin-android")
}

android {
    compileSdkVersion(Versions.COMPILE_SDK)
    defaultConfig {
        targetSdkVersion(Versions.TARGET_SDK)
        minSdkVersion(Versions.MIN_SDK)
    }

    buildTypes {
        getByName("release") {
            buildConfigField("int", "LOGGING_LEVEL", "6") // Log.ERROR
            buildConfigField("boolean", "USE_CRASHLYTICS", "true")
        }

        getByName("debug") {
            buildConfigField("int", "LOGGING_LEVEL", "2") // Log.VERBOSE
            buildConfigField("boolean", "USE_CRASHLYTICS", "false")
        }
    }
}

dependencies {
    // platform
    api(platform(project(":depconstraints")))
    implementation(platform(Libs.FIREBASE_BOM))

    implementation(Libs.ANDROIDX_ANNOTATION)
    implementation(Libs.FIREBASE_CRASHLYTICS_KTX)
}

plugins {
    id("com.android.library")
    id("kotlin-android")
}

android {
    compileSdkVersion(Versions.COMPILE_SDK)
    defaultConfig {
        targetSdkVersion(Versions.TARGET_SDK)
        minSdkVersion(Versions.MIN_SDK)
    }

    buildTypes {
        getByName("release") {
            buildConfigField("String", "API_BASE_URL", "\"https://nhlapi.mudar.ca/api/\"")
        }

        getByName("debug") {
            buildConfigField("String", "API_BASE_URL", "\"https://nhlapi.mudar.ca/api/\"")
        }
    }
}

dependencies {
    implementation(project(":common"))
    implementation(project(":dto"))
    implementation(project(":models"))

    api(Libs.SQUARE_RETROFIT)
    api(Libs.SQUARE_OKHTTP3_LOGGING_INTERCEPTOR)
}

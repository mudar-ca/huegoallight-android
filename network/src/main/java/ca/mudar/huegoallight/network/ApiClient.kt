/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.network

import ca.mudar.huegoallight.common.LogUtils.REMOTE_LOG
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.dto.JsonGameList
import ca.mudar.huegoallight.dto.Link
import ca.mudar.huegoallight.models.GameList
import ca.mudar.huegoallight.models.GameScore
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit.SECONDS

object ApiClient {
    private val TAG = makeLogTag("ApiClient")
    val service: NhlApiService
        get() = getService(false)

    @get:Deprecated("")
    val serviceLog: NhlApiService
        get() = getService(true)

    private val moshi = Moshi.Builder()
        .add(Link::class.java, LinkAdapter())
        .addLast(KotlinJsonAdapterFactory())
        .build()

    private val loggingInterceptor = HttpLoggingInterceptor().apply {
        setLevel(BODY)
    }

    private fun buildHttpClient(httpLogging: Boolean): OkHttpClient {
        val httpClientBuilder = OkHttpClient.Builder()
            .connectTimeout(60, SECONDS)
            .readTimeout(60, SECONDS)
        if (httpLogging) {
            httpClientBuilder.addInterceptor(loggingInterceptor)
        }
        return httpClientBuilder.build()
    }

    /**
     * Get the NHL API service
     *
     * @param httpLogging Enable verbose
     * @return NhlApiService
     */
    private fun getService(httpLogging: Boolean): NhlApiService =
        Retrofit.Builder()
            .baseUrl(BuildConfig.API_BASE_URL)
            .client(
                buildHttpClient(httpLogging)
            )
            .addConverterFactory(
                MoshiConverterFactory.create(moshi)
            )
            .build()
            .create(NhlApiService::class.java)

    fun getSeasonCalendar(service: NhlApiService, timestamp: Long): GameList? {
        val result = runCatching {
            service.getSeasonCalendar(timestamp)
                .execute()
        }.getOrNull()

        return result?.body()?.let { dto ->
            GameList(
                data = dto.data
                    ?.filter { it.isValidGame() }
                    ?.map { it.toGame() }
                    ?: emptyList(),
                isDataFiltered = dto.meta?.isDataFiltered ?: false,
                lastUpdatedAt = dto.meta?.timestamp ?: 0
            )
        }
    }

    fun getLiveGames(
        service: NhlApiService,
        token: String,
        games: String,
        callback: (List<GameScore>) -> Unit
    ) {
        service.getLiveGames(token, games).enqueue(object : Callback<JsonGameList> {
            override fun onResponse(call: Call<JsonGameList>, response: Response<JsonGameList>) {
                val result = response.body()?.data
                    ?.filter { it.isValidGame() }
                    ?.map { it.toGameScore() }
                    ?: emptyList()
                callback(result)
            }

            override fun onFailure(call: Call<JsonGameList>, t: Throwable) {
                REMOTE_LOG(t)
                callback(emptyList())
            }
        })
    }
}

/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.network

import ca.mudar.huegoallight.common.Const.ApiArgs
import ca.mudar.huegoallight.common.Const.ApiPaths
import ca.mudar.huegoallight.dto.JsonGame
import ca.mudar.huegoallight.dto.JsonGameList
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface NhlApiService {

    @Headers(CONTENT_TYPE)
    @GET(ApiPaths.GET_SEASON_CALENDAR)
    fun getSeasonCalendar(
        @Query(ApiArgs.LAST_UPDATED_AT) timestamp: Long
    ): Call<JsonGameList>

    @Headers(CONTENT_TYPE)
    @GET(ApiPaths.GET_CLUB_CALENDAR)
    fun getClubCalendar(
        @Path(ApiArgs.CLUB) club: String,
        @Query(ApiArgs.LAST_UPDATED_AT) timestamp: Long
    ): Call<JsonGameList>

    @Headers(CONTENT_TYPE)
    @GET(ApiPaths.GET_MULTI_LIVE_GAMES)
    fun getLiveGames(
        @Header(ApiArgs.TOKEN) token: String,
        @Path(ApiArgs.GAMES) games: String
    ): Call<JsonGameList>

    @Headers(CONTENT_TYPE)
    @GET(ApiPaths.GET_LIVE_GAME)
    fun getDefaultLive(
        @Header(ApiArgs.TOKEN) token: String
    ): Call<JsonGame>

    companion object {
        const val CONTENT_TYPE = "Content-type: application/json"
    }
}

/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.network

import android.util.Patterns
import android.webkit.URLUtil
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.models.WebhookAuth
import ca.mudar.huegoallight.models.WebhookConfig
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Logger
import retrofit2.Callback
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

object WebhookClient {
    private val TAG = makeLogTag("WebhookClient")

    /**
     * Get the Webhook service
     */
    fun getService(auth: WebhookAuth, logger: Logger? = null): WebhookService {
        val clientBuilder = OkHttpClient.Builder().apply {
            connectTimeout(60, TimeUnit.SECONDS)
            readTimeout(60, TimeUnit.SECONDS)
            addInterceptor(WebhookAuthInterceptor(auth))
        }

        logger?.let {
            val loggingInterceptor = HttpLoggingInterceptor(it).apply {
                level = HttpLoggingInterceptor.Level.BODY
            }
            clientBuilder.addInterceptor(loggingInterceptor)
        }

        val retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.API_BASE_URL)
            .client(clientBuilder.build())
            .build()
        return retrofit.create(WebhookService::class.java)
    }

    fun callWebhook(
        service: WebhookService,
        config: WebhookConfig,
        fcmData: FcmDataWrapper,
        callback: Callback<ResponseBody>
    ) {

        if (URLUtil.isHttpsUrl(config.url) && Patterns.WEB_URL.matcher(config.url).matches()) {
            when (config.method) {
                "GET" -> service.callWebhookGet(
                    url = config.url,
                    message = fcmData.message,
                    game = fcmData.game,
                    team = fcmData.team,
                    colors = fcmData.hexColors,
                    timestamp = fcmData.timestamp
                )
                "POST" -> service.callWebhookPost(
                    url = config.url,
                    message = fcmData.message,
                    game = fcmData.game,
                    team = fcmData.team,
                    colors = fcmData.hexColors,
                    timestamp = fcmData.timestamp
                )
                "PUT" -> service.callWebhookPut(
                    url = config.url,
                    message = fcmData.message,
                    game = fcmData.game,
                    team = fcmData.team,
                    colors = fcmData.hexColors,
                    timestamp = fcmData.timestamp
                )
                "PATCH" -> service.callWebhookPatch(
                    url = config.url,
                    message = fcmData.message,
                    game = fcmData.game,
                    team = fcmData.team,
                    colors = fcmData.hexColors,
                    timestamp = fcmData.timestamp
                )
                else -> null
            }?.enqueue(callback)
        }
    }
}

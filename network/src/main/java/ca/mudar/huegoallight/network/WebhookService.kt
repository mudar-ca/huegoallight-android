/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.network

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Query
import retrofit2.http.Url

interface WebhookService {
    @Headers(CONTENT_TYPE_JSON)
    @GET
    fun callWebhookGet(
        @Url url: String,
        @Query("message") message: String?,
        @Query("game") game: String?,
        @Query("team") team: String?,
        @Query("colors[]") colors: List<String>?,
        @Query("timestamp") timestamp: Long?
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST
    fun callWebhookPost(
        @Url url: String,
        @Field("message") message: String?,
        @Field("game") game: String?,
        @Field("team") team: String?,
        @Field("colors[]") colors: List<String>?,
        @Field("timestamp") timestamp: Long?
    ): Call<ResponseBody>

    @FormUrlEncoded
    @PUT
    fun callWebhookPut(
        @Url url: String,
        @Field("message") message: String?,
        @Field("game") game: String?,
        @Field("team") team: String?,
        @Field("colors[]") colors: List<String>?,
        @Field("timestamp") timestamp: Long?
    ): Call<ResponseBody>

    @FormUrlEncoded
    @PATCH
    fun callWebhookPatch(
        @Url url: String,
        @Field("message") message: String?,
        @Field("game") game: String?,
        @Field("team") team: String?,
        @Field("colors[]") colors: List<String>?,
        @Field("timestamp") timestamp: Long?
    ): Call<ResponseBody>

    companion object {
        private const val CONTENT_TYPE_JSON = "Content-type: application/json"
    }
}

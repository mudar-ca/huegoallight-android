/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.network

import ca.mudar.huegoallight.common.Const.ApiValues
import ca.mudar.huegoallight.common.LogUtils.REMOTE_LOG
import ca.mudar.huegoallight.dto.Link
import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonReader.Token
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.ToJson
import okio.Buffer
import java.io.IOException

class LinkAdapter : JsonAdapter<Link>() {
    @FromJson
    override fun fromJson(reader: JsonReader): Link? {
        var href: String? = null
        var meta: String? = null
        try {
            when {
                reader.peek() === Token.NULL -> {
                    reader.nextNull<Unit>()
                    return null
                }
                reader.peek() == Token.STRING -> {
                    // Link member is a plain string
                    return Link(href = reader.nextString())
                }
                reader.peek() === Token.BEGIN_OBJECT -> {
                    // Link member is a Link object, containing `href` and `meta` string members
                    reader.beginObject()
                    while (reader.peek() != Token.END_OBJECT) {
                        if (reader.peek() == Token.NAME) {
                            when (reader.nextName()) {
                                ApiValues.LINK_HREF -> href = reader.nextString()
                                ApiValues.LINK_META -> meta = reader.nextString()
                                else -> reader.skipValue()
                            }
                        }
                    }
                    reader.endObject()
                }
            }
        } catch (e: IOException) {
            REMOTE_LOG(e)
        }

        return when {
            href.isNullOrEmpty() -> null
            else -> Link(href = href, meta = meta)
        }
    }

    @ToJson
    override fun toJson(writer: JsonWriter, value: Link?) {
        value?.let { link ->
            writer.value(Buffer().writeUtf8(link.toString()))
        }
    }
}

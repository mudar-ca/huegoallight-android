/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.network

import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.models.WebhookAuth
import ca.mudar.huegoallight.models.WebhookBarerTokenAuth
import ca.mudar.huegoallight.models.WebhookBasicAuth
import ca.mudar.huegoallight.models.WebhookNoAuth
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.Response

class WebhookAuthInterceptor(private val webhookAuth: WebhookAuth) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()

        val request = when (webhookAuth) {
            is WebhookNoAuth -> original
            is WebhookBarerTokenAuth -> original.newBuilder()
                    .header(name = AUTH_HEADER, value = "$AUTH_BEARER ${webhookAuth.token}")
                    .method(original.method, original.body)
                    .build()
            is WebhookBasicAuth -> original.newBuilder()
                    .addHeader(
                            name = AUTH_HEADER,
                            value = Credentials.basic(webhookAuth.username, webhookAuth.password)
                    )
                    .build()
            else -> original
        }

        return chain.proceed(request)
    }

    companion object {
        private val TAG = makeLogTag("AuthInterceptor")

        private const val AUTH_HEADER = "Authorization"
        private const val AUTH_BEARER = "Bearer"
    }
}

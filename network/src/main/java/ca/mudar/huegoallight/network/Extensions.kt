/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.network

import ca.mudar.huegoallight.common.Const.ApiValues.GAMECENTER_LINK
import ca.mudar.huegoallight.dto.JsonGameDataItem
import ca.mudar.huegoallight.dto.Links
import ca.mudar.huegoallight.models.Game
import ca.mudar.huegoallight.models.GameScore
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

fun String.toFcmDataWrapper(): FcmDataWrapper =
    Moshi.Builder()
        .addLast(KotlinJsonAdapterFactory())
        .build()
        .adapter(FcmDataWrapper::class.java)
        .fromJson(this)
        ?: FcmDataWrapper()

internal fun JsonGameDataItem.isValidGame(): Boolean =
    (id.isNullOrEmpty() ||
            attributes?.startsAt == null ||
            attributes?.away?.slug.isNullOrEmpty() ||
            attributes?.home?.slug.isNullOrEmpty()
            )
        .not()

internal fun JsonGameDataItem.toGame(): Game =
    Game(
        remoteId = id.orEmpty(),
        startsAt = attributes?.startsAt ?: 0,
        awayTeamSlug = attributes?.away?.slug.orEmpty(),
        homeTeamSlug = attributes?.home?.slug.orEmpty(),
        awayTeamScore = attributes?.away?.goals ?: 0,
        homeTeamScore = attributes?.home?.goals ?: 0,
        // TODO Fields should not be initialized
        delay = 0,
        isMuted = false,
        updatedAt = 0,
        url = links?.toGameCenterUrl().orEmpty()
    )

internal fun JsonGameDataItem.toGameScore(): GameScore =
    GameScore(
        remoteId = id.orEmpty(),
        awayTeamScore = attributes?.away?.goals ?: 0,
        homeTeamScore = attributes?.home?.goals ?: 0
    )

internal fun Links.toGameCenterUrl(): String? {
    return when (related?.meta) {
        GAMECENTER_LINK -> related?.href
        else -> null
    }
}

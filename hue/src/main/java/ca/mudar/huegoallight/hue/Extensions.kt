/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.hue

import ca.mudar.huegoallight.models.HueBridge
import ca.mudar.huegoallight.models.HueLight
import ca.mudar.huegoallight.prefs.HuePrefs
import com.squareup.moshi.JsonDataException
import inkapplications.shade.Shade
import inkapplications.shade.constructs.ShadeException
import inkapplications.shade.discover.Device
import inkapplications.shade.lights.Light
import java.util.Locale

fun String.getShortMacAddress(): String =
    replace(":", "")
        .takeLast(6)
        .toLowerCase(Locale.ROOT)

fun Light.toHueLight(id: String): HueLight = HueLight(
    id = id,
    name = name,
    isUnreachable = (state.reachable ?: false).not()
)

fun Device.toHueBridge(): HueBridge = HueBridge(
    id = id.getShortMacAddress(),
    ipAddress = ip
)

suspend fun Shade.isConnected(): Boolean {
    runCatching {
        // Call the api to validate token, regardless of the result object
        groups.getGroups()
    }.exceptionOrNull().let { error ->
        return when (error) {
            null -> true
            is ShadeException -> false
            is JsonDataException -> {
                // FIXME Should not happen, but bridge returns json array instead of object
                false
            }
            else -> throw error
        }
    }
}

internal fun HueBridge.withCachedToken(): HueBridge {
    if (token.isNullOrEmpty()) {
        HuePrefs.lastKnownBridge?.takeIf {
            this.id == it.id
        }?.let {
            return this.copy(token = it.token)
        }
    }
    return this
}

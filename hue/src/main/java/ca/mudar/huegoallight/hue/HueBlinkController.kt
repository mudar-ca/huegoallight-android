/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.hue

import android.graphics.Color
import android.os.Handler
import android.os.HandlerThread
import android.text.format.DateUtils
import androidx.annotation.ColorInt
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ca.mudar.huegoallight.common.Const
import ca.mudar.huegoallight.common.LogUtils
import ca.mudar.huegoallight.prefs.UserPrefs
import com.github.ajalt.colormath.RGB
import inkapplications.shade.constructs.Coordinates
import inkapplications.shade.constructs.percent
import inkapplications.shade.lights.AlertState
import inkapplications.shade.lights.AlertState.L_SELECT
import inkapplications.shade.lights.LightState
import inkapplications.shade.lights.LightStateModification
import org.threeten.bp.Duration

@Suppress("ObjectPropertyName")
object HueBlinkController {
    private val TAG = LogUtils.makeLogTag("HueBlinkController")

    // Wait for 1 second after the default transition to switch lights off
    private const val DELAY_SWITCH_OFF = DateUtils.SECOND_IN_MILLIS
    private val BRIGHTNESS_FULL = 100.percent

    val isBlinking: LiveData<Boolean>
        get() = _isBlinking

    private val _isBlinking = MutableLiveData(false)
    private var previousStates = mutableMapOf<String, LightState>()
    private var handlerThread: HandlerThread? = null

    fun onBridgeDisconnected() {
        _isBlinking.postValue(false)
    }

    fun startBlinking(@ColorInt blinkHues: List<Int>, team: String? = null) {
        if (HueBridgeManager.isLightSelectionEmpty()) {
            return
        }

        // Use short duration for opponents and manual blinking
        val blinkDuration = when {
            UserPrefs.followedNhlTeams.contains(team) -> UserPrefs.blinkDuration
            else -> Const.BLINK_SHORT_DURATION
        }

        // Clear any previous values
        previousStates.clear()

        _isBlinking.postValue(true)

        // Compute 3 values for HueColor.RGB before loop
        val primaryRgbHueColor = getPrimaryColor(blinkHues)
        val secondaryRgbHueColor = getSecondaryColor(blinkHues)
        val redRgbHueColor = convertColorIntToHueColorRGB(Const.RED_COLOR)

        HueBridgeManager.getAllLights {
            it.forEach { (lightId, light) ->
                val blinkRgbHueColor: RGB = when {
                    HueBridgeManager.isPrimaryLight(lightId) -> primaryRgbHueColor
                    HueBridgeManager.isSecondaryLight(lightId) -> secondaryRgbHueColor
                    HueBridgeManager.isRedLight(lightId) -> redRgbHueColor
                    else -> return@forEach // Skip unselected light
                }

                val isAvailable = saveOriginalHueIfAvailable(lightId, light.state)
                if (isAvailable.not()) {
                    LogUtils.LOGV(TAG, "not Available :(")
                    return@forEach
                }

                val modification: LightStateModification =
                    when (LightType.safeValueOf(light.type)) {
                        LightType.COLOR,
                        LightType.EXTENDED_COLOR -> LightStateModification(
                            cieColorCoordinates = Coordinates(blinkRgbHueColor)
                        )
                        LightType.DIMMABLE,
                        LightType.COLOR_TEMPERATURE -> LightStateModification(
                            brightness = BRIGHTNESS_FULL
                        )
                        else -> return@forEach
                    }

                HueBridgeManager.setLightState(
                    lightId = lightId,
                    lightStateModification = modification.copy(
                        on = true,
                        transitionTime = Duration.ofMillis(Const.HUE_FAST_TRANSITION),
                        alert = L_SELECT
                    )
                )
            }

            restoreLightsAfterDelay(blinkDuration)
        }
    }

    private fun restoreLightsAfterDelay(seconds: Int) {
        if (handlerThread == null) {
            handlerThread = HandlerThread(TAG).apply { start() }
        }

        handlerThread?.looper?.let {
            Handler(it).postDelayed({
                restoreLights()
            }, seconds * DateUtils.SECOND_IN_MILLIS)
        }
    }

    private fun restoreLights() {
        HueBridgeManager.getSelectedLights {
            restoreSelectedLights(it)
        }
    }

    private fun restoreSelectedLights(selectedLights: List<String>) {
        var hadLightsOff = false

        val modification = LightStateModification(
            on = true,
            transitionTime = Duration.ofMillis(Const.HUE_DEFAULT_TRANSITION),
            alert = AlertState.NONE
        )
        selectedLights.forEach { lightId ->
            previousStates[lightId]?.let { previousState ->
                hadLightsOff = previousState.on.not() || hadLightsOff

                HueBridgeManager.setLightState(
                    lightId = lightId,
                    lightStateModification = modification.copy(
                        brightness = previousState.brightness,
                        hue = previousState.hue,
                        saturation = previousState.saturation,
                        effect = previousState.effect,
                        cieColorCoordinates = previousState.cieColorCoordinates,
                        colorTemperature = previousState.colorTemperature
                    )
                )
            }
        }

        if (hadLightsOff) {
            // We must wait for the color transition (400ms) before switching off
            switchOffLightsAfterDelay()
        } else {
            onFinished()
        }
    }

    private fun switchOffLightsAfterDelay() {
        handlerThread?.looper?.let {
            Handler(it).postDelayed({
                switchOffLights()
            }, DELAY_SWITCH_OFF)
        }
    }

    private fun switchOffLights() {
        HueBridgeManager.getSelectedLights {
            switchOffSelectedLights(it)
        }
    }

    private fun switchOffSelectedLights(selectedLights: List<String>) {
        val modification = LightStateModification(
            on = false,
            transitionTime = Duration.ofMillis(Const.HUE_DEFAULT_TRANSITION)
        )
        selectedLights.forEach { lightId ->
            previousStates[lightId]?.let { previousState ->
                if (previousState.on.not()) {
                    HueBridgeManager.setLightState(
                        lightId = lightId,
                        lightStateModification = modification
                    )
                }
            }
        }

        onFinished()
    }

    private fun onFinished() {
        _isBlinking.postValue(false)

        handlerThread?.quit()
        handlerThread = null
    }

    private fun getPrimaryColor(@ColorInt blinkHues: List<Int>): RGB =
        convertColorIntToHueColorRGB(blinkHues.first())

    private fun getSecondaryColor(@ColorInt blinkHues: List<Int>): RGB =
        convertColorIntToHueColorRGB(blinkHues.getOrNull(1) ?: Const.ALTERNATE_RGB_COLOR)

    private fun convertColorIntToHueColorRGB(@ColorInt color: Int): RGB =
        RGB(Color.red(color), Color.green(color), Color.blue(color))

    /**
     * Save light's original colors
     *
     * @return true if saved. false if unreachable or already blinking
     */
    private fun saveOriginalHueIfAvailable(lightId: String, lightState: LightState): Boolean {
        /*
         * When triggered manually, we don't check if reachable or already blinking.
         * This also helps fix non-responsive lights that were stuck in blinking AlertMode.
         */
//        if (!isManualBlink && (light.state.reachable != true || light.state.alert != AlertState.NONE)) {
//            LOGW(TAG, "Light " + light.uuid + " is not available. Unreachable or already blinking?")
//
//            // FIXME Issue related to Hue Bridge, where reachable lights report as not. Disabled for now
//            // return false;
//        }
        previousStates[lightId] = lightState
        return true
    }
}

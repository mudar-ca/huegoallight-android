/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.hue

import android.net.wifi.WifiManager
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ca.mudar.huegoallight.common.Const
import ca.mudar.huegoallight.common.LogUtils.REMOTE_LOG
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.models.BridgeLightSelection
import ca.mudar.huegoallight.models.BridgeNotFoundError
import ca.mudar.huegoallight.models.HueBridge
import ca.mudar.huegoallight.models.HueBridgeState
import ca.mudar.huegoallight.models.HueLight
import ca.mudar.huegoallight.models.HueSdkError
import ca.mudar.huegoallight.models.LightSelectionError
import ca.mudar.huegoallight.models.PushlinkExpiredError
import ca.mudar.huegoallight.prefs.HuePrefs
import inkapplications.shade.Shade
import inkapplications.shade.auth.TokenStorage
import inkapplications.shade.constructs.ShadeException
import inkapplications.shade.discover.Device
import inkapplications.shade.lights.AlertState
import inkapplications.shade.lights.Light
import inkapplications.shade.lights.LightStateModification
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

@Suppress("ObjectPropertyName")
object HueBridgeManager : CoroutineScope by CoroutineScope(Dispatchers.Main) {
    private val TAG = makeLogTag("HueBridgeManager")

    // awaitToken duration should be equal to PushlinkViewModel.PROGRESS_BAR_DURATION (30 sec)
    private const val TOKEN_RETRIES = 15
    private const val TOKEN_TIMEOUT_MILLIS: Long = 2000
    private const val HTTP_CLIENT_TIMEOUT: Long = 15 // seconds

    val discoveryResults: LiveData<List<HueBridge>>
        get() = _discoveryResults
    val error: LiveData<HueSdkError?>
        get() = _error
    val state: LiveData<HueBridgeState>
        get() = _state

    private val _discoveryResults = MutableLiveData<List<HueBridge>>()
    private val _error = MutableLiveData<HueSdkError?>()
    private val _state = MutableLiveData(HueBridgeState.Idle)

    private var selectedBridge: HueBridge? = null
    private var lightSelection: BridgeLightSelection? = null
    private var pushlinkJob: Job? = null

    val currentBridgeId: String?
        get() = selectedBridge?.id

    private var cachedBridge: HueBridge?
        get() = HuePrefs.lastKnownBridge
        set(value) {
            HuePrefs.lastKnownBridge = value
        }

    private val tokenStorage: TokenStorage = object : TokenStorage {
        override suspend fun getToken(): String? {
            return selectedBridge?.token ?: cachedBridge?.token
        }

        override suspend fun setToken(token: String?) {
            when {
                token.isNullOrBlank() -> {
                    disconnectFromBridgeIfNecessary()
                    _state.postValue(HueBridgeState.Idle)
                }
                else -> {
                    selectedBridge?.copy(token = token).let {
                        selectedBridge = it
                        cachedBridge = it
                    }
                    _state.postValue(HueBridgeState.Connected)
                }
            }
        }
    }

    private val shade = Shade(
        appId = Const.HUE_APP_NAME,
        storage = tokenStorage,
        client = OkHttpClient.Builder()
            .callTimeout(HTTP_CLIENT_TIMEOUT, TimeUnit.SECONDS)
            .retryOnConnectionFailure(false)
            .build()
    )

    private val upnpDiscoveryListener = object : UpnpDiscoveryListener {
        override fun onCompleted(result: Set<HueBridge>) {
            val mergedList = result.toMutableList()
                .apply { addAll(_discoveryResults.value.orEmpty()) }
                .distinctBy { it.id }
            updateBridgeDiscoveryResults(mergedList)
        }

        override fun onProgress(progress: Float, result: Set<HueBridge>) {
            _state.postValue(HueBridgeState.BridgeDiscoveryRunning)
        }
    }

    fun startBridgeAutoConnection(): Boolean {
        return cachedBridge?.let { bridge ->
            connectToBridge(bridge)
            true
        } ?: false
    }

    fun countBridgeDiscoveryResults(callback: (Int) -> Unit) {
        launch {
            val results: List<Device> = runCatching {
                shade.discovery.getDevices()
            }.getOrNull() ?: emptyList()
            val bridgeList = results.map { it.toHueBridge() }
            _discoveryResults.postValue(bridgeList)
            callback(bridgeList.size)
        }
    }

    fun startBridgeDiscovery(callback: (List<HueBridge>) -> Unit) {
        if (_state.value == HueBridgeState.BridgeDiscoveryRunning) {
            return
        }

        // Using main thread to set _state.value before Observers are created
        _state.value = HueBridgeState.BridgeDiscoveryRunning

        launch {
            val results: List<Device> = try {
                shade.discovery.getDevices()
            } catch (e: Exception) {
                e.printStackTrace()
                emptyList()
            }

            val bridgeList = results.map { it.toHueBridge() }
            updateBridgeDiscoveryResults(bridgeList)
            callback(bridgeList)
        }
    }

    private fun updateBridgeDiscoveryResults(bridgeList: List<HueBridge>) {
        _discoveryResults.postValue(bridgeList)

        if (bridgeList.isEmpty()) {
            _state.postValue(HueBridgeState.Idle)
            _error.postValue(BridgeNotFoundError)
        } else {
            _error.postValue(null)
            _state.postValue(HueBridgeState.BridgeDiscoveryResults)
        }
    }

    fun connectToBridge(index: Int = 0) {
        _discoveryResults.value?.getOrNull(index)?.let { bridge ->
            connectToBridge(bridge.withCachedToken())
        }
    }

    private fun connectToBridge(hueBridge: HueBridge) {
        disconnectFromBridgeIfNecessary()
        _state.postValue(HueBridgeState.Connecting)

        selectedBridge = hueBridge.also {
            shade.setBaseUrl(it.url)
        }
        launch {
            runCatching {
                validateToken()
            }.onSuccess { isConnected ->
                when {
                    isConnected -> onBridgeConnected()
                    else -> onPushlinkRequired()
                }
            }.onFailure {
                REMOTE_LOG(it)
                onConnectionError(BridgeNotFoundError)
            }
        }
    }

    fun forceDisconnectFromBridge() {
        disconnectFromBridgeIfNecessary()

        _discoveryResults.postValue(emptyList())
        _state.postValue(HueBridgeState.Idle)
        _error.postValue(null)
        HueBlinkController.onBridgeDisconnected()
    }

    private fun disconnectFromBridgeIfNecessary() {
        selectedBridge = null
        pushlinkJob?.cancel()
    }

    fun expirePushlink() {
        onConnectionError(PushlinkExpiredError)
    }

    private fun onConnectionError(hueSdkError: HueSdkError) {
        disconnectFromBridgeIfNecessary()
        _state.postValue(HueBridgeState.Idle)
        _error.postValue(hueSdkError)
    }

    private fun onBridgeConnected() {
        _state.postValue(HueBridgeState.Connected)
        cachedBridge = selectedBridge?.also {
            setupBridgeLightSelection(it.id)
        }
    }

    fun scanNetworkForUpnpBridge(wifiManager: WifiManager) {
        UpnpDiscovery(wifiManager, upnpDiscoveryListener).startNetworkScan()
    }

    private suspend fun onPushlinkRequired() {
        _state.postValue(HueBridgeState.Pushlinking)
        runCatching {
            pushlinkJob = launch {
                shade.auth.awaitToken(TOKEN_RETRIES, TOKEN_TIMEOUT_MILLIS)
            }
        }.onFailure {
            when (it) {
                is ShadeException -> expirePushlink()
                else -> onConnectionError(BridgeNotFoundError)
            }
        }
    }

    internal fun setLightState(lightId: String, lightStateModification: LightStateModification) {
        launch {
            runCatching {
                shade.lights.setState(lightId, lightStateModification)
            }.onFailure {
                REMOTE_LOG(it)
            }
        }
    }

    fun getAllHueLights(callback: (List<HueLight>) -> Unit) {
        launch {
            runCatching {
                shade.lights.getLights()
            }.onSuccess { lights ->
                callback(
                    lights.map { it.value.toHueLight(id = it.key) }
                )
            }.onFailure {
                REMOTE_LOG(it)
                callback(emptyList())
            }
        }
    }

    internal fun getAllLights(callback: (Map<String, Light>) -> Unit) {
        launch {
            runCatching {
                shade.lights.getLights()
            }.onSuccess {
                callback(it)
            }.onFailure {
                REMOTE_LOG(it)
                callback(emptyMap())
            }
        }
    }

    fun resetAllLightsAlertMode() {
        launch(Dispatchers.IO) {
            runCatching {
                shade.lights.getLights()
            }.onSuccess { lights ->
                val modification = LightStateModification(alert = AlertState.NONE)
                lights.keys.forEach { lightId ->
                    runCatching {
                        shade.lights.setState(lightId, modification)
                    }
                }
            }
        }
    }

    internal fun getSelectedLights(callback: (List<String>) -> Unit) {
        launch {
            runCatching {
                shade.lights.getLights()
            }.onSuccess { lights ->
                callback(
                    lights.filterKeys { lightSelection?.isSelectedLight(it) ?: false }
                        .map { it.key }
                )
            }.onFailure {
                REMOTE_LOG(it)
                callback(emptyList())
            }
        }
    }

    fun setupBridgeLightSelection() {
        currentBridgeId?.let {
            setupBridgeLightSelection(it)
        }
    }

    private fun setupBridgeLightSelection(bridgeId: String) {
        launch(Dispatchers.IO) {
            if (HuePrefs.isKnownBridge(bridgeId)) {
                lightSelection = HuePrefs.getBridgeLightSelection()
            } else {
                runCatching {
                    shade.lights.getLights()
                }.onSuccess { allLights ->
                    val selectedLights: List<String> = allLights.map { it.key }
                    HuePrefs.setLightSelection(bridgeId, selectedLights)
                    lightSelection = BridgeLightSelection(selectedLights)
                }
            }
        }
    }

    @WorkerThread
    fun isHueBridgeConnected(): Boolean = runBlocking {
        when (selectedBridge) {
            null -> false
            else -> validateToken()
        }
    }

    private suspend fun validateToken(): Boolean =
        shade.auth.validateToken(selectedBridge?.token.orEmpty())

    fun isLightSelectionEmpty(): Boolean {
        return (lightSelection?.isEmptySelection() ?: true)
            .also { isEmpty ->
                if (isEmpty) {
                    _error.postValue(LightSelectionError)
                }
            }
    }

    fun isPrimaryLight(lightId: String) = lightSelection?.isPrimaryLight(lightId) ?: false

    fun isSecondaryLight(lightId: String) = lightSelection?.isSecondaryLight(lightId) ?: false

    fun isRedLight(lightId: String) = lightSelection?.isRedLight(lightId) ?: false
}

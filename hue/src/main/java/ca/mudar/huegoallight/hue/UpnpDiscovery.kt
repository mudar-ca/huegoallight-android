/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.hue

import android.net.wifi.WifiManager
import android.net.wifi.WifiManager.MulticastLock
import ca.mudar.huegoallight.models.HueBridge
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withTimeout
import kotlinx.coroutines.yield
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
import java.net.InetSocketAddress
import java.util.HashSet

internal class UpnpDiscovery(
    private val wifiManager: WifiManager,
    private val listener: UpnpDiscoveryListener
) {

    companion object {
        private const val TAG = "UpnpDiscovery"
        private const val HTTP_SUCCESS = "HTTP/1.1 200"
        private const val PREFIX_HUE_BRIDGE_ID = "hue-bridgeid"
        private const val SCAN_TIMEOUT: Long = 5000
        private const val SCAN_REPEAT = 10
        private const val SOCKET_TIMEOUT = 1000
        private const val PACKET_SIZE = 320
        private const val UPNP_ADDRESS = "239.255.255.250"
        private const val UPNP_PORT = 1900
        private val UPNP_DISCOVERY_QUERY = """
                    M-SEARCH * HTTP/1.1
                    HOST: $UPNP_ADDRESS:$UPNP_PORT
                    MAN: "ssdp:discover"
                    MX: 3
                    ST: ssdp:all
                    """.trimIndent()

        private fun Int.toScanProgress(): Float = this.toFloat() / SCAN_REPEAT
    }

    fun startNetworkScan() {
        GlobalScope.launch(Dispatchers.IO) {
            val lock = acquireMulticastLock()
            val result = findNetworkUpnpHueBridges()
            lock.release()
            listener.onCompleted(result)
        }
    }

    private fun acquireMulticastLock(): MulticastLock =
        wifiManager.createMulticastLock(TAG).apply {
            acquire()
        }

    private suspend fun findNetworkUpnpHueBridges(): Set<HueBridge> {
        val result = HashSet<HueBridge>()
        var socket: DatagramSocket? = null

        try {
            socket = createDatagramSocket()
            socket.send(createDatagramPacket())

            withTimeout(SCAN_TIMEOUT) {
                repeat(SCAN_REPEAT) {
                    listener.onProgress(it.toScanProgress(), result)
                    socket.receiveSuccessfulPacket()?.let { packet ->
                        createHueBridgeIfPossible(packet)?.let { hueBridge ->
                            result.add(hueBridge)
                        }
                    }
                    yield()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            socket?.close()
            return result
        }
    }

    private fun createDatagramSocket(): DatagramSocket = DatagramSocket(null).apply {
        reuseAddress = true
        broadcast = true
        bind(InetSocketAddress(UPNP_PORT))
        soTimeout = SOCKET_TIMEOUT
    }

    private fun createDatagramPacket(): DatagramPacket = DatagramPacket(
        UPNP_DISCOVERY_QUERY.toByteArray(),
        UPNP_DISCOVERY_QUERY.length,
        InetAddress.getByName(UPNP_ADDRESS),
        UPNP_PORT
    )

    private fun DatagramSocket.receiveSuccessfulPacket(): DatagramPacket? {
        val packet = runCatching {
            DatagramPacket(ByteArray(PACKET_SIZE), PACKET_SIZE).also {
                receive(it)
            }
        }.getOrNull() ?: return null

        val status = String(packet.data, 0, HTTP_SUCCESS.length)
        return when {
            status.equals(HTTP_SUCCESS, ignoreCase = true) -> packet
            else -> null
        }
    }

    private fun createHueBridgeIfPossible(packet: DatagramPacket): HueBridge? {
        val response = String(packet.data, 0, packet.length)
        val hueBridgeId = response.lines()
            .firstOrNull { it.startsWith(prefix = PREFIX_HUE_BRIDGE_ID, ignoreCase = true) }
            ?.substringAfter(':')
            ?.trim()

        return hueBridgeId?.let { bridgeId ->
            HueBridge(
                id = bridgeId.getShortMacAddress(),
                ipAddress = packet.address.hostAddress
            )
        }
    }
}

## Title
*30 chars*

Hue Hockey Goal Light

## Short description
*80 chars* 

Make your Philips Hue lights blink in celebration of your NHL team’s goals

## Full description
*4000 chars*

IMPORTANT: Do not install if you don't have a Philips Hue bridge. The app doesn't do much without Hue lights.

Follow your favourite NHL team, and let your Philips Hue notify you when they score a goal…

Open source project, available on GitLab. Source code is released under the GPLv3 license from the Free Software Foundation: https://gitlab.com/mudar-ca/huegoallight-android

Please note that results are not real-time, usual delay is around 30-60 seconds if compared to live TV. There's less delay when streaming the game, or if you are able to pause/rewind your stream (or PVR).

If the app is faster than your game broadcast or stream, lights can blink before you see the goal. To avoid this spoiler, you can delay blinking for up to 1 minute.


LEGAL NOTICE
---
This is a fan project and is not affiliated with, endorsed or sponsored by the NHL, the NHL teams or Philips Lighting B.V.

NHL and the NHL Shield are registered trademarks of the National Hockey League. All NHL logos and marks and NHL team logos and marks depicted herein are the property of the NHL and the respective teams and may not be reproduced without the prior written consent of NHL Enterprises, L.P. © NHL. All Right Reserved.

Philips and Philips Hue are registered trademarks of Koninklijke Philips N.V. © Philips Lighting B.V. All rights reserved.

import com.android.build.api.dsl.ApplicationBuildType
import com.android.build.gradle.BaseExtension
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

buildscript {
    repositories {
        google()
    }
}

plugins {
    id("com.android.application")
    id("com.google.gms.google-services")
    id("kotlin-android")
    id("kotlin-kapt")
    id("com.google.firebase.crashlytics")
}

repositories {
    google()
    mavenCentral()
}

android {
    compileSdk = Versions.COMPILE_SDK
    defaultConfig {
        applicationId = "ca.mudar.huegoallight"
        targetSdk = Versions.TARGET_SDK
        minSdk = Versions.MIN_SDK

        versionCode = Versions.VERSION_CODE
        versionName = Versions.VERSION_NAME

        resourceConfigurations.clear()
        resourceConfigurations.add("en")
        resourceConfigurations.add("fr")
    }

    buildFeatures {
        dataBinding = true
        viewBinding = true
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            isShrinkResources = true
            proguardFiles(*getProguardFileList())

            setCrashlyticsEnabled(true)
        }

        getByName("debug") {
            isMinifyEnabled = false
            applicationIdSuffix = ".d"
            versionNameSuffix = "-build".timestampSuffix()

            setCrashlyticsEnabled(false)
        }

        create("debugCrashlytics") {
            initWith(getByName("debug"))
            applicationIdSuffix = null
            setCrashlyticsEnabled(true)

            matchingFallbacks.add("debug")
        }

        create("debugProguard") {
            initWith(getByName("debug"))
            isMinifyEnabled = true
            isShrinkResources = true
            proguardFiles(*getProguardFileList())

            applicationIdSuffix = null

            matchingFallbacks.add("debug")
        }
    }

    compileOptions {
        targetCompatibility = JavaVersion.VERSION_1_8
        sourceCompatibility = JavaVersion.VERSION_1_8
    }
}

fun ApplicationBuildType.setCrashlyticsEnabled(enabled: Boolean) {
    manifestPlaceholders["enableCrashlytics"] = enabled
}

fun BaseExtension.getProguardFileList(): Array<Any> = arrayOf(
    *(file("./proguard").listFiles() ?: emptyArray()),
    getDefaultProguardFile("proguard-android-optimize.txt"),
    "proguard-rules.pro"
)

fun String.timestampSuffix(): String {
    val formattedTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("ddMMMyyyy.HHmm"))
    return "$this-$formattedTime"
}

dependencies {
    // platform
    implementation(platform(Libs.FIREBASE_BOM))

    implementation(project(":common"))
    implementation(project(":hue"))
    implementation(project(":models"))
    implementation(project(":repository"))
    implementation(project(":network"))
    implementation(project(":prefs"))

    implementation(Libs.COROUTINES_CORE)
    implementation(Libs.COROUTINES_ANDROID)

    // AndroidX
    implementation(Libs.ANDROIDX_CORE_KTX)
    implementation(Libs.ANDROIDX_ANNOTATION)
    implementation(Libs.ANDROIDX_APPCOMPAT)
    implementation(Libs.ANDROIDX_RECYCLERVIEW)
    implementation(Libs.ANDROIDX_CARDVIEW)
    implementation(Libs.ANDROIDX_CONSTRAINT_LAYOUT)
    implementation(Libs.ANDROIDX_VIEWPAGER2)
    implementation(Libs.ANDROIDX_SWIPE_REFRESH_LAYOUT)
    implementation(Libs.ANDROIDX_PALETTE_KTX)

    // Google Material
    implementation(Libs.GOOGLE_MATERIAL)

    // Google Play Services
    implementation(Libs.GOOGLE_PLAY_SERVICES_BASE)

    // Firebase
    implementation(Libs.FIREBASE_ANALYTICS_KTX)
    implementation(Libs.FIREBASE_CORE)
    implementation(Libs.FIREBASE_CRASHLYTICS_KTX)
    implementation(Libs.FIREBASE_MESSAGING_KTX)
    implementation(Libs.FIREBASE_PERF_KTX)

    // https://github.com/mikepenz/AboutLibraries
    implementation(Libs.ABOUT_LIBRARIES)

    // https://github.com/woxblom/DragListView
    implementation(Libs.DRAG_LIST_VIEW)
    // https://github.com/sjwall/MaterialTapTargetPrompt
    implementation(Libs.TAP_TARGET_PROMPT)
    // https://square.github.io/leakcanary
    debugImplementation(Libs.SQUARE_LEAK_CANARY)
}

configurations.all {
    resolutionStrategy {
        failOnVersionConflict()
        preferProjectModules()

        // cache dynamic versions for 10 minutes
        cacheDynamicVersionsFor(600, TimeUnit.SECONDS)
        // don't cache changing modules at all
        cacheChangingModulesFor(0, TimeUnit.SECONDS)
    }
}

apply(plugin = "com.google.gms.google-services")

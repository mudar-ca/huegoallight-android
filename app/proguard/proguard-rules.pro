#-keep class ca.mudar.huegoallight.** { *; }
-keep, includedescriptorclasses class inkapplications.shade.** { *; }

-dontobfuscate

-keep class ca.mudar.huegoallight.dto.base.** { *; }
-keep class ca.mudar.huegoallight.dto.** { *; }
-keep class ca.mudar.huegoallight.hockey.** { *; }
-keep class ca.mudar.huegoallight.network.** { *; }

-dontwarn sun.misc.Unsafe
#-dontwarn com.google.common.collect.MinMaxPriorityQueue

# Exclude R to enable the libraries auto detection (com.mikepenz:aboutlibraries)
-keepclasseswithmembers class **.R$* {
    public static final int define_*;
}

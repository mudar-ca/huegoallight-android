/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.utils

import android.text.format.DateUtils
import ca.mudar.huegoallight.network.LoggerCallback
import ca.mudar.huegoallight.network.SimpleCallback
import ca.mudar.huegoallight.network.WebhookClient
import ca.mudar.huegoallight.network.toFcmDataWrapper
import ca.mudar.huegoallight.prefs.UserPrefs
import okhttp3.ResponseBody
import retrofit2.Callback

object WebhookHelper {
    private val emptyCallback = object : SimpleCallback {}

    private val MESSAGE_TEMPLATE: String
        get() = """
            |{"message": "Hue Goal",
            |"game": 123456789,
            |"team": "MTL",
            |"colors": [[166, 25, 46], [0, 30, 98]],
            |"timestamp": ${System.currentTimeMillis() / DateUtils.SECOND_IN_MILLIS}
            |}
        """.trimMargin()

    fun callWebhookIfAvailable(
        messageData: String = MESSAGE_TEMPLATE,
        callback: Callback<ResponseBody> = emptyCallback
    ) {
        if (UserPrefs.hasWebhook) {
            UserPrefs.webhookConfig?.let { config ->
                val service = when (callback) {
                    is LoggerCallback -> WebhookClient.getService(config.auth, callback.logger)
                    else -> WebhookClient.getService(config.auth)
                }

                WebhookClient.callWebhook(
                    service = service,
                    config = config,
                    fcmData = messageData.toFcmDataWrapper(),
                    callback = callback
                )
            }
        }
    }
}

/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.utils

import android.app.Dialog
import android.app.Notification
import android.app.TimePickerDialog
import android.content.Context
import android.media.AudioManager
import android.net.Uri
import androidx.core.app.NotificationCompat
import ca.mudar.huegoallight.common.Const.SUPPORTS_NOUGAT
import ca.mudar.huegoallight.common.Const.SUPPORTS_OREO
import ca.mudar.huegoallight.prefs.UserPrefs
import ca.mudar.huegoallight.ui.widget.TimePickerDialogCompat

object CompatUtils {

    /**
     * Skip ringtone/vibration for silent games. For Oreo and later, user can adjust
     * notification channel settings
     */
    fun setNotificationBuilderDefaults(
        builder: NotificationCompat.Builder,
        isSilent: Boolean
    ): NotificationCompat.Builder {
        if (isSilent || SUPPORTS_OREO) {
            return builder
        }

        val hasVibration = UserPrefs.hasVibration
        val ringtone: Uri? = UserPrefs.ringtoneUri

        when {
            hasVibration && (ringtone != null) -> builder
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setSound(ringtone, AudioManager.STREAM_NOTIFICATION)
            hasVibration -> builder.setDefaults(Notification.DEFAULT_VIBRATE)
            ringtone != null -> builder.setSound(ringtone)
        }

        return builder
    }

    fun getTimePickerDialog(
        context: Context?,
        listener: TimePickerDialog.OnTimeSetListener,
        hourOfDay: Int,
        minute: Int,
        is24HourView: Boolean
    ): Dialog = when {
        SUPPORTS_NOUGAT -> TimePickerDialog(context, listener, hourOfDay, minute, is24HourView)
        else -> TimePickerDialogCompat(context, listener, hourOfDay, minute, is24HourView)
    }
}

/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.utils

import android.content.Context
import android.text.format.DateFormat
import ca.mudar.huegoallight.prefs.UserPrefs
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

object TimeUtils {

    fun getMinutesOfDay(hourOfDay: Int, minute: Int): Int =
        hourOfDay * 60 + minute

    fun getCalendarInstance(minutesOfDay: Int): Calendar =
        Calendar.getInstance().apply {
            set(Calendar.HOUR_OF_DAY, minutesOfDay / 60)
            set(Calendar.MINUTE, minutesOfDay % 60)
        }

    fun getTimeDisplay(context: Context, minutesOfDay: Int): String =
        getTimeDisplay(context, getCalendarInstance(minutesOfDay))

    private fun getTimeDisplay(context: Context, calendar: Calendar): String {
        val pattern = when {
            DateFormat.is24HourFormat(context) -> "H:mm"
            else -> "K:mm aa"
        }
        return SimpleDateFormat(pattern, Locale.getDefault()).format(calendar.time)
    }

    fun isCurrentlyWithinPeriod(minutesOfDayStart: Int, minutesOfDayEnd: Int): Boolean =
        isCurrentlyWithinPeriod(
            start = getCalendarInstance(minutesOfDayStart),
            end = getCalendarInstance(minutesOfDayEnd)
        )

    private fun isCurrentlyWithinPeriod(start: Calendar, end: Calendar): Boolean {
        val now = Calendar.getInstance()

        if (now == start || now == end || start == end) {
            return true
        } else if (start.before(end)) {
            return start.before(now) && end.after(now)
        } else if (end.before(start)) {
            // Overnight period, inverse the condition
            return !(end.before(now) && start.after(now))
        }

        return false
    }

    fun isCurrentlyQuietHours(): Boolean =
        UserPrefs.hasQuietHours &&
                isCurrentlyWithinPeriod(UserPrefs.quietHoursStart, UserPrefs.quietHoursEnd)
}

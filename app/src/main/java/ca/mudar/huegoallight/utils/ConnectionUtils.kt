/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities.NET_CAPABILITY_INTERNET
import android.net.NetworkCapabilities.NET_CAPABILITY_VALIDATED
import android.view.View
import ca.mudar.huegoallight.R
import com.google.android.material.snackbar.Snackbar

object ConnectionUtils {

    fun hasConnection(context: Context?): Boolean {
        context ?: return false

        val connectivityManager = context.applicationContext
            .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkCapabilities = connectivityManager
            .getNetworkCapabilities(connectivityManager.activeNetwork)

        return networkCapabilities?.run {
            hasCapability(NET_CAPABILITY_INTERNET) && hasCapability(NET_CAPABILITY_VALIDATED)
        } ?: false
    }

    fun checkConnection(view: View?): Boolean {
        view ?: return false

        val hasConnection = hasConnection(view.context)

        if (hasConnection.not()) {
            Snackbar.make(view, R.string.snackbar_no_connection, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.snackbar_btn_retry) { checkConnection(it) }
                .show()
        }

        return hasConnection
    }
}

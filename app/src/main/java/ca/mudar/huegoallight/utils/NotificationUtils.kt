/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.utils

import android.annotation.TargetApi
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.media.AudioAttributes
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.text.format.DateUtils.SECOND_IN_MILLIS
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.Const.IntentNames
import ca.mudar.huegoallight.common.Const.NOTIFY_DEFAULT_CHANNEL_ID
import ca.mudar.huegoallight.common.Const.NOTIFY_SILENT_CHANNEL_ID
import ca.mudar.huegoallight.common.Const.RequestCodes
import ca.mudar.huegoallight.common.Const.SUPPORTS_OREO
import ca.mudar.huegoallight.hockey.NhlTeam
import ca.mudar.huegoallight.prefs.UserPrefs
import ca.mudar.huegoallight.ui.activity.MainActivity

object NotificationUtils {

    fun showNotification(context: ContextWrapper, team: NhlTeam, delay: Int = 0) =
        showNotification(context = context, team = team, isSilent = false, delay = delay)

    fun showSilentNotification(context: ContextWrapper, team: NhlTeam) =
        showNotification(context = context, team = team, isSilent = true, delay = 0)

    /**
     * Create and show a simple notification containing the received FCM message.
     * Silent notifications are displayed without delay and do not trigger blinking.
     */
    private fun showNotification(
        context: ContextWrapper,
        team: NhlTeam,
        isSilent: Boolean,
        delay: Int
    ) {
        val notificationManager = NotificationManagerCompat.from(context)

        if (UserPrefs.hasNotifications.not() ||
            notificationManager.areNotificationsEnabled().not()
        ) {
            return
        }

        val builder = getNotificationBuilder(
            context = context,
            team = team,
            isSilent = isSilent
        ).apply {
            if (delay > 0) {
                setUsesChronometer(true)
                setWhen(System.currentTimeMillis() - delay * SECOND_IN_MILLIS)
            }
        }

        notificationManager.notify(RequestCodes.NOTIFY_ID, builder.build())
    }

    private fun getNotificationBuilder(
        context: Context,
        team: NhlTeam,
        isSilent: Boolean
    ): NotificationCompat.Builder {
        val intent = Intent(context, MainActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        }

        val pendingIntent = PendingIntent.getActivity(
            context,
            0,
            intent,
            PendingIntent.FLAG_ONE_SHOT
        )

        val res = context.resources
        val contentTitle = res.getString(R.string.notify_goal_title)
        val contentText = res.getString(
            R.string.notify_goal_text,
            res.getString(team.teamName)
        )

        val channelId = setupNotificationChannelIfNecessary(context, team, isSilent)
        var builder = NotificationCompat.Builder(context, channelId)
            .setSmallIcon(team.teamNotifyIcon)
            .setContentTitle(contentTitle)
            .setContentText(contentText)
            .setColor(team.color)
            .setAutoCancel(true)
            .setContentIntent(pendingIntent)
        builder = CompatUtils.setNotificationBuilderDefaults(builder, isSilent)

        return builder
    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun setupNotificationChannelIfNecessary(
        context: Context,
        team: NhlTeam,
        isSilent: Boolean
    ): String {
        if (SUPPORTS_OREO.not()) {
            return NOTIFY_DEFAULT_CHANNEL_ID
        }

        val channel = when {
            isSilent -> getSilentNotificationChannel(context)
            else -> getTeamNotificationChannel(context, team)
        }.apply {
            setSound(
                Settings.System.DEFAULT_NOTIFICATION_URI, AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build()
            )
            enableLights(true)
            enableVibration(true)
            lightColor = team.color
            lockscreenVisibility = Notification.VISIBILITY_PUBLIC
        }

        NotificationManagerCompat.from(context).createNotificationChannel(channel)

        return channel.id
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun getTeamNotificationChannel(context: Context, team: NhlTeam): NotificationChannel {
        val isFollowingTeam = UserPrefs.followedNhlTeams.contains(team.slug)
        val channelId = when {
            isFollowingTeam -> team.slug
            else -> NOTIFY_DEFAULT_CHANNEL_ID
        }

        val channelName = context.getString(
            when {
                isFollowingTeam -> team.teamName
                else -> R.string.notify_default_channel
            }
        )

        return NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT)
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun getSilentNotificationChannel(context: Context): NotificationChannel =
        NotificationChannel(
            NOTIFY_SILENT_CHANNEL_ID,
            context.getString(R.string.notify_silent_channel),
            NotificationManager.IMPORTANCE_LOW
        )

    @TargetApi(Build.VERSION_CODES.O)
    fun editAppNotificationsSettings(context: Context) {
        if (SUPPORTS_OREO) {
            val extras = Bundle().apply {
                putString(Settings.EXTRA_APP_PACKAGE, context.packageName)
            }

            val intent = Intent(IntentNames.APP_NOTIFICATION_SETTINGS).apply {
                putExtras(extras)
            }

            context.startActivity(intent)
        }
    }
}

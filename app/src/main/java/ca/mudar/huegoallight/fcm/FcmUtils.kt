/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.fcm

import android.app.Activity
import android.content.ContextWrapper
import android.os.Bundle
import ca.mudar.huegoallight.common.Const.RequestCodes
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.hockey.NhlTeam
import ca.mudar.huegoallight.prefs.HuePrefs
import ca.mudar.huegoallight.prefs.UserPrefs
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.firebase.analytics.FirebaseAnalytics

object FcmUtils {
    private val TAG = makeLogTag("FcmUtils")

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    fun checkPlayServices(activity: Activity): Boolean {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val resultCode = apiAvailability.isGooglePlayServicesAvailable(activity)

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.showErrorDialogFragment(
                        activity,
                        resultCode,
                        RequestCodes.PLAY_SERVICES_RESOLUTION_REQUEST
                ) { activity.finish() }
            }

            return false
        }
        return true
    }

    fun setupAnalyticsUserProperties(context: ContextWrapper) {
        val analytics = FirebaseAnalytics.getInstance(context)
        with(UserPrefs) {
            analytics.setUserProperty("followed_teams_size", followedNhlTeams.size.toString())
            analytics.setUserProperty("quiet_hours_enabled", hasQuietHours.toString())
            analytics.setUserProperty("blink_duration", blinkDuration.toString())
            analytics.setUserProperty("webhook_enabled", hasWebhook.toString())
        }
        with(HuePrefs) {
            analytics.setUserProperty("primary_lights_size", primaryLights.size.toString())
            analytics.setUserProperty("secondary_lights_size", secondaryLights.size.toString())
            analytics.setUserProperty("red_lights_size", redLights.size.toString())
        }
    }

    fun sendAnalyticsTeamFollowedEvent(
        context: ContextWrapper,
        team: String,
        enabled: Boolean,
        onboarding: Boolean = false
    ) {
        val nhlTeam = NhlTeam(team)

        val itemList = when {
            onboarding -> "onboarding"
            else -> "settings"
        }

        val params = Bundle().apply {
            putString(FirebaseAnalytics.Param.CONTENT_TYPE, "nhl_team")
            putString(FirebaseAnalytics.Param.ITEM_ID, nhlTeam.slug)
            putString(FirebaseAnalytics.Param.ITEM_LIST_NAME, itemList)
            putString(FirebaseAnalytics.Param.ITEM_NAME, context.getString(nhlTeam.teamName))
        }
        val eventName = when {
            enabled -> "team_followed"
            else -> "team_unfollowed"
        }

        FirebaseAnalytics.getInstance(context).logEvent(eventName, params)
    }

    fun sendAnalyticsShareEvent(context: ContextWrapper) {
        FirebaseAnalytics.getInstance(context).logEvent(FirebaseAnalytics.Event.SHARE, Bundle.EMPTY)
    }

    fun sendAnalyticsRateEvent(context: ContextWrapper) {
        FirebaseAnalytics.getInstance(context).logEvent("rate_app", Bundle.EMPTY)
    }

    fun sendAnalyticsPushlinkEvent(context: ContextWrapper) {
        FirebaseAnalytics.getInstance(context).logEvent("bridge_pushlink", Bundle.EMPTY)
    }

    fun sendAnalyticsManualBlinkEvent(context: ContextWrapper) {
        FirebaseAnalytics.getInstance(context).logEvent("manual_blink", Bundle.EMPTY)
    }

    fun sendAnalyticsGameClickEvent(context: ContextWrapper) {
        FirebaseAnalytics.getInstance(context).logEvent("game_click", Bundle.EMPTY)
    }

    fun sendAnalyticsGameMuteEvent(context: ContextWrapper) {
        FirebaseAnalytics.getInstance(context).logEvent("game_mute", Bundle.EMPTY)
    }

    fun sendAnalyticsGameDelayEvent(context: ContextWrapper, delay: Int) {
        val params = Bundle().apply {
            putString(FirebaseAnalytics.Param.CONTENT_TYPE, "nhl_game")
            putInt(FirebaseAnalytics.Param.VALUE, delay)
        }

        FirebaseAnalytics.getInstance(context).logEvent("game_delay", params)
    }
}

/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.fcm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ca.mudar.huegoallight.common.Const.FCM_DEFAULT_TOPICS
import ca.mudar.huegoallight.common.LogUtils.REMOTE_LOG
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.models.FcmEvent
import ca.mudar.huegoallight.models.FcmIdleEvent
import ca.mudar.huegoallight.models.FcmRegistrationError
import ca.mudar.huegoallight.models.FcmRegistrationSuccess
import ca.mudar.huegoallight.models.FcmTopicSubscriptionError
import ca.mudar.huegoallight.models.FcmTopicSubscriptionSuccess
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Suppress("ObjectPropertyName")
object FcmRepo : CoroutineScope by CoroutineScope(Dispatchers.IO) {
    private val TAG = makeLogTag("FcmRepo")

    val event: LiveData<FcmEvent>
        get() = _event

    private val _event: MutableLiveData<FcmEvent> = MutableLiveData(FcmIdleEvent)

    fun register() = run {
        FirebaseMessaging.getInstance().token
            .addOnSuccessListener { _event.postValue(FcmRegistrationSuccess) }
            .addOnFailureListener { _event.postValue(FcmRegistrationError(it)) }
    }

    fun toggleTopicSubscription(topic: String, enabled: Boolean) =
        launch {
            try {
                FirebaseMessaging.getInstance().let { fcm ->
                    when {
                        enabled -> fcm.subscribeToTopic(topic)
                        else -> fcm.unsubscribeFromTopic(topic)
                    }
                }.apply {
                    addOnSuccessListener {
                        _event.postValue(FcmTopicSubscriptionSuccess(topic))
                    }
                    addOnFailureListener {
                        _event.postValue(FcmTopicSubscriptionError(topic, it))
                    }
                    addOnCanceledListener {
                        _event.postValue(FcmTopicSubscriptionError(topic, CancellationException()))
                    }
                }
            } catch (e: Exception) {
                _event.postValue(FcmTopicSubscriptionError(topic, e))
                REMOTE_LOG(e)
            }
        }

    fun onNewFcmToken() {
        _event.postValue(FcmRegistrationSuccess)

        toggleTopicSubscription(FCM_DEFAULT_TOPICS, true)
    }
}

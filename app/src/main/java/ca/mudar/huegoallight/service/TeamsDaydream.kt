/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.service

import android.os.Handler
import android.os.Looper
import android.service.dreams.DreamService
import android.text.format.DateUtils
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ServiceLifecycleDispatcher
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.hockey.NhlTeam
import ca.mudar.huegoallight.models.Game
import ca.mudar.huegoallight.prefs.UserPrefs
import ca.mudar.huegoallight.repo.GameRepo
import ca.mudar.huegoallight.ui.widget.FloatingFrameLayout

class TeamsDaydream : DreamService(), LifecycleOwner {

    private lateinit var followedTeams: List<String>
    private val handler = Handler(Looper.getMainLooper())
    private var gameList = listOf<Game>()

    private val dispatcher = ServiceLifecycleDispatcher(this)

    override fun onCreate() {
        dispatcher.onServicePreSuperOnCreate()
        super.onCreate()
    }

    override fun onAttachedToWindow() {
        dispatcher.onServicePreSuperOnStart()
        super.onAttachedToWindow()

        isInteractive = false
        isFullscreen = true

        setContentView(R.layout.daydream)

        followedTeams = UserPrefs.followedNhlTeams

        GameRepo.getInstance(applicationContext)
            .getCurrentGames(followedTeams)
            .apply {
                observe(this@TeamsDaydream, Observer<List<Game>> {
                    gameList = it
                    updateLogos()
                })
            }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        handler.removeCallbacksAndMessages(null)
    }

    override fun onDestroy() {
        dispatcher.onServicePreSuperOnDestroy()
        super.onDestroy()
    }

    /**
     * Implements LifecycleOwner
     */
    override fun getLifecycle(): Lifecycle =
        dispatcher.lifecycle

    private fun updateLogos() {
        if (gameList.isNotEmpty()) {
            val game = gameList.random()

            val awayTeam = NhlTeam(game.awayTeamSlug)
            val homeTeam = NhlTeam(game.homeTeamSlug)

            if (game.startsAt < System.currentTimeMillis()) {
                showTeamsWithScore(awayTeam, homeTeam, game.awayTeamScore, game.homeTeamScore)
            } else {
                showTeamsWithoutScore(awayTeam, homeTeam)
            }
        } else if (followedTeams.isNotEmpty()) {
            // No current games, show the logo of a random followed team
            val slug = followedTeams.random()
            showSingleTeam(NhlTeam(slug))
        }

        handler.postDelayed({
            updateLogos()
            findViewById<FloatingFrameLayout>(R.id.floating_frame).jump()
        }, DAYDREAM_DURATION + FADE_DURATION)
    }

    private fun showSingleTeam(homeTeam: NhlTeam) {
        showTeamsWithScore(homeTeam = homeTeam)
    }

    private fun showTeamsWithoutScore(awayTeam: NhlTeam, homeTeam: NhlTeam) {
        showTeamsWithScore(awayTeam = awayTeam, homeTeam = homeTeam)
    }

    private fun showTeamsWithScore(
        awayTeam: NhlTeam? = null,
        homeTeam: NhlTeam,
        awayScore: Int? = null,
        homeScore: Int? = null
    ) {
        findViewById<ImageView>(R.id.home_logo).setImageResource(homeTeam.teamLogo)

        val vAwayLogo = findViewById<ImageView>(R.id.away_logo)
        val vAwayScore = findViewById<TextView>(R.id.away_score)
        val vHomeScore = findViewById<TextView>(R.id.home_score)

        if (awayTeam == null) {
            // One team only
            vAwayLogo.visibility = View.GONE
        } else {
            // Two teams
            vAwayLogo.visibility = View.VISIBLE
            findViewById<ImageView>(R.id.away_logo).setImageResource(awayTeam.teamLogo)
        }

        if (awayScore != null && homeScore != null) {
            // Game with score
            vAwayScore.visibility = View.VISIBLE
            vHomeScore.visibility = View.VISIBLE
            vAwayScore.text = awayScore.toString()
            vHomeScore.text = homeScore.toString()
        } else {
            // Upcoming game, without score
            vAwayScore.visibility = View.GONE
            vHomeScore.visibility = View.GONE
        }
    }

    companion object {
        // Ref: equals R.dimen.daydream_duration
        private const val DAYDREAM_DURATION = DateUtils.MINUTE_IN_MILLIS
        private const val FADE_DURATION: Long = 500
    }
}

/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.service

import android.content.ContextWrapper
import android.content.Intent
import android.os.Bundle
import androidx.core.app.JobIntentService
import ca.mudar.huegoallight.common.Const.APP_JOB_ID
import ca.mudar.huegoallight.common.Const.BundleKeys
import ca.mudar.huegoallight.common.LogUtils.REMOTE_LOG
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.hockey.NhlTeam
import ca.mudar.huegoallight.hue.HueBlinkController
import ca.mudar.huegoallight.hue.HueBridgeManager
import ca.mudar.huegoallight.network.toFcmDataWrapper
import ca.mudar.huegoallight.utils.NotificationUtils
import ca.mudar.huegoallight.utils.WebhookHelper

class DelayedBlinkService : JobIntentService() {

    override fun onHandleWork(intent: Intent) {
        HueBridgeManager.startBridgeAutoConnection()

        try {
            if (HueBridgeManager.isHueBridgeConnected()) {
                val dataWrapper = intent.getStringExtra(BundleKeys.DATA_WRAPPER)
                    ?: return
                val gameDelay = intent.getIntExtra(BundleKeys.SECONDS, 0)

                val fcmData = dataWrapper.toFcmDataWrapper()
                val team = NhlTeam(fcmData.team ?: "")
                val colors = fcmData.colors

                // startBlinking()
                HueBlinkController.startBlinking(blinkHues = colors, team = team.slug)
                // Display notification
                NotificationUtils.showNotification(
                    context = ContextWrapper(applicationContext),
                    team = team,
                    delay = gameDelay
                )
                // Trigger webhook
                WebhookHelper.callWebhookIfAvailable(dataWrapper)
            }
        } catch (e: Exception) {
            REMOTE_LOG(e)
        }
    }

    companion object {
        private val TAG = makeLogTag("DelayedBlinkService")

        fun enqueueWork(context: ContextWrapper, dataWrapper: String, delay: Int) {
            val intent = Intent(context, DelayedBlinkService::class.java)

            val extras = Bundle().apply {
                putString(BundleKeys.DATA_WRAPPER, dataWrapper)
                putInt(BundleKeys.SECONDS, delay)
            }
            intent.putExtras(extras)

            enqueueWork(
                context.applicationContext,
                DelayedBlinkService::class.java,
                APP_JOB_ID,
                intent
            )
        }
    }
}

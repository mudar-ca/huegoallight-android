/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.service

import android.content.ContextWrapper
import android.content.Intent
import androidx.core.app.JobIntentService
import ca.mudar.huegoallight.common.Const.APP_JOB_ID
import ca.mudar.huegoallight.common.LogUtils.LOGI
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.network.ApiClient
import ca.mudar.huegoallight.prefs.AppPrefs
import ca.mudar.huegoallight.repo.DatabaseHelper
import ca.mudar.huegoallight.utils.ConnectionUtils

class ScheduleUpdateService : JobIntentService() {

    override fun onHandleWork(intent: Intent) {
        if (ConnectionUtils.hasConnection(applicationContext).not()) {
            return
        }

        val startTime = System.currentTimeMillis()
        AppPrefs.init(application)

        ApiClient.getSeasonCalendar(ApiClient.service, AppPrefs.scheduleLastUpdatedAt)
            ?.let { gameList ->
                if (gameList.data.isNotEmpty()) {
                    LOGI(TAG, "Updating season schedule…")
                    val result =
                        DatabaseHelper.updateSeasonSchedule(applicationContext, gameList.data)
                    if (result) {
                        AppPrefs.scheduleLastUpdatedAt = gameList.lastUpdatedAt
                    }
                } else if (gameList.isDataFiltered) {
                    // Data is empty thanks to timestamp filter, update local timestamp value
                    AppPrefs.scheduleLastUpdatedAt = gameList.lastUpdatedAt
                }
            }

        LOGI(TAG, String.format("Sync duration: %d ms", System.currentTimeMillis() - startTime))
    }

    companion object {
        private val TAG = makeLogTag("ScheduleUpdateService ")

        fun enqueueWork(context: ContextWrapper) {
            val intent = Intent(context, ScheduleUpdateService::class.java)

            enqueueWork(
                context.applicationContext,
                ScheduleUpdateService::class.java,
                APP_JOB_ID,
                intent
            )
        }
    }
}

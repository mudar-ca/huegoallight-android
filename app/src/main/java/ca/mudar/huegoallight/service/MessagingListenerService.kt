/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.service

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.ContextWrapper
import android.os.SystemClock
import android.text.format.DateUtils
import androidx.annotation.WorkerThread
import androidx.core.app.AlarmManagerCompat
import ca.mudar.huegoallight.common.Const.NOTIFY_MAX_DELAY
import ca.mudar.huegoallight.common.LogUtils.LOGV
import ca.mudar.huegoallight.common.LogUtils.REMOTE_LOG
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.fcm.FcmRepo
import ca.mudar.huegoallight.hockey.NhlTeam
import ca.mudar.huegoallight.hue.HueBlinkController
import ca.mudar.huegoallight.hue.HueBridgeManager
import ca.mudar.huegoallight.network.toFcmDataWrapper
import ca.mudar.huegoallight.prefs.UserPrefs
import ca.mudar.huegoallight.receiver.DelayedBlinkReceiver
import ca.mudar.huegoallight.repo.GameRepo
import ca.mudar.huegoallight.utils.NotificationUtils
import ca.mudar.huegoallight.utils.TimeUtils
import ca.mudar.huegoallight.utils.WebhookHelper
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import kotlin.math.abs

class MessagingListenerService : FirebaseMessagingService() {

    @WorkerThread
    override fun onNewToken(token: String) {
        super.onNewToken(token)

        with(FcmRepo) {
            onNewFcmToken()

            // Re-subscribe to topics
            UserPrefs.followedNhlTeams.forEach { team ->
                toggleTopicSubscription(team, true)
            }
        }
    }

    @WorkerThread
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        LOGV(TAG, "onMessageReceived")
        super.onMessageReceived(remoteMessage)

        HueBridgeManager.startBridgeAutoConnection()

        if (HueBridgeManager.isHueBridgeConnected()) {
            try {
                val messageData = remoteMessage.data[MESSAGE_DATA_KEY]
                        ?: return
                onBridgeConnectedAndInitialized(messageData)
            } catch (e: Exception) {
                REMOTE_LOG(e)
            }
        }
    }

    private fun onBridgeConnectedAndInitialized(messageData: String) {
        val fcmData = messageData.toFcmDataWrapper()

        val team = NhlTeam(fcmData.team ?: "")
        val gameId = fcmData.game ?: ""
        val colors = fcmData.colors
        val sentAt = fcmData.timestampMillis

        val gameIsMuted: Boolean
        val gameHasDelay: Boolean
        val gameDelay: Int

        GameRepo.getInstance(applicationContext)
                .getGameSync(gameId).let {
                    // Nullable game is allowed, to handle outdated database and debug
                    gameIsMuted = it?.isMuted ?: false
                    gameHasDelay = it?.hasDelay() ?: false
                    gameDelay = it?.delay ?: 0
                }

        if (gameIsMuted) {
            // Skip muted game
            LOGV(TAG, "Game is on mute")
            return
        }

        val hasExpired = abs(System.currentTimeMillis() - sentAt) > NOTIFY_MAX_DELAY
        val isBlinking = HueBlinkController.isBlinking.value == true
        // Message is silent when ignoring opponents's goals, or late notification
        val isSilent = TimeUtils.isCurrentlyQuietHours() ||
                isIgnoredOpponentGoal(team) ||
                hasExpired ||
                isBlinking

        when {
            isSilent -> {
                // Silent notifications do not trigger blinking
                NotificationUtils.showSilentNotification(
                        context = ContextWrapper(applicationContext),
                        team = team
                )
                LOGV(TAG, "Silent notification")
            }
            gameHasDelay -> {
                blinkAndNotifyAfterDelay(messageData, gameDelay)
            }
            else -> {
                // Display notification
                NotificationUtils.showNotification(
                        context = ContextWrapper(applicationContext),
                        team = team
                )
                // startBlinking()
                HueBlinkController.startBlinking(blinkHues = colors, team = team.slug)
                // Trigger webhook
                WebhookHelper.callWebhookIfAvailable(messageData)
            }
        }
    }

    private fun isIgnoredOpponentGoal(nhlTeam: NhlTeam): Boolean =
            UserPrefs.followedNhlTeams.contains(nhlTeam.slug).not() && UserPrefs.ignoreOpponent

    private fun blinkAndNotifyAfterDelay(dataWrapper: String, delay: Int) {
        val context = applicationContext

        val uniqueID = System.currentTimeMillis().toInt()
        val pendingIntent = PendingIntent.getBroadcast(context,
                uniqueID,
                DelayedBlinkReceiver.newIntent(context, dataWrapper, delay),
                PendingIntent.FLAG_UPDATE_CURRENT)

        val triggerAt = SystemClock.elapsedRealtime() + delay * DateUtils.SECOND_IN_MILLIS
        AlarmManagerCompat.setExactAndAllowWhileIdle(
                context.getSystemService(Context.ALARM_SERVICE) as AlarmManager,
                AlarmManager.ELAPSED_REALTIME_WAKEUP,
                triggerAt,
                pendingIntent)
    }

    companion object {
        private val TAG = makeLogTag("MessagingListenerService")
        private const val MESSAGE_DATA_KEY = "dataWrapper"
    }
}

/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.hue.HueBridgeManager

class PushlinkViewModel internal constructor() : ViewModel() {
    val showWrongClickErrorMessage = MutableLiveData<Boolean>(false)

    var progressValue: Int? = null
        set(value) {
            field = value?.also {
                if (value <= 0) {
                    HueBridgeManager.expirePushlink()
                }
            }
        }
    val startValue: Int
        get() = progressValue ?: PROGRESS_BAR_MAX
    val animDuration: Long
        get() = PROGRESS_BAR_DURATION * startValue / PROGRESS_BAR_MAX

    private var totalClicks = 0

    /**
     * In case user clicks on the illustration instead of the physical push-link button!
     */
    fun onIllustrationClick() {
        totalClicks++
        if (totalClicks >= WRONG_CLICK_THRESHOLD) {
            totalClicks = 0
            showWrongClickErrorMessage.value = true
        } else {
            showWrongClickErrorMessage.value = false
        }
    }

    companion object {
        private val TAG = makeLogTag("PushlinkVM")

        private const val WRONG_CLICK_THRESHOLD = 3
        private const val PROGRESS_BAR_MAX = 3000
        const val PROGRESS_BAR_DURATION = 30000L // 30 seconds, authentication timeout
        const val PULSE_DURATION = 2000L // 2 seconds pulse

        fun instanceOf(owner: ViewModelStoreOwner): PushlinkViewModel =
                ViewModelProvider(owner).get(PushlinkViewModel::class.java)
    }
}

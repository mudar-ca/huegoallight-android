/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import ca.mudar.huegoallight.models.Game
import ca.mudar.huegoallight.repo.GameRepo
import ca.mudar.huegoallight.viewmodel.factory.UpcomingGameListViewModelFactory

class UpcomingGameListViewModel internal constructor(
    private val gameRepo: GameRepo,
    val teams: List<String>
) : ViewModel() {

    val games: LiveData<List<Game>>
        get() = _games

    private val _games = MediatorLiveData<List<Game>>()
    private val teamsFollowed = MutableLiveData<List<String>>(teams)

    init {
        val liveGameList = Transformations.switchMap(teamsFollowed) {
            gameRepo.getUpcomingGames(it)
        }
        _games.addSource(liveGameList, _games::setValue)
    }

    fun updateTeams(teams: List<String>) {
        teamsFollowed.postValue(teams)
    }

    companion object {

        fun instanceOf(
            owner: ViewModelStoreOwner,
            gameRepo: GameRepo,
            teams: List<String>
        ): UpcomingGameListViewModel =
            ViewModelProvider(
                owner,
                UpcomingGameListViewModelFactory(gameRepo, teams)
            ).get(UpcomingGameListViewModel::class.java)
    }
}

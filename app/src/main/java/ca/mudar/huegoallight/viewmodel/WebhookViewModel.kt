/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.viewmodel

import android.content.SharedPreferences
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import ca.mudar.huegoallight.common.Const.PrefsNames
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.utils.WebhookHelper
import okhttp3.ResponseBody
import retrofit2.Callback

class WebhookViewModel : ViewModel() {

    val isFabVisible = MutableLiveData(false)
    val isFabEnabled = MutableLiveData(true)

    val prefsChangeListener = SharedPreferences.OnSharedPreferenceChangeListener { prefs, key ->
        if (key == PrefsNames.WEBHOOK_ENABLED) {
            isFabVisible.value = prefs.getBoolean(key, false)
        }
        isFabEnabled.value = true
    }

    fun onWebhookResponse() {
        isFabEnabled.value = true
    }

    fun triggerWebhook(callback: Callback<ResponseBody>) {
        isFabEnabled.value = false
        WebhookHelper.callWebhookIfAvailable(callback = callback)
    }

    companion object {
        private val TAG = makeLogTag("WebhookVM")

        fun instanceOf(owner: ViewModelStoreOwner): WebhookViewModel =
            ViewModelProvider(owner).get(WebhookViewModel::class.java)
    }
}

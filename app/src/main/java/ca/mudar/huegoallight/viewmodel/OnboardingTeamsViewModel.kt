/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.fcm.FcmRepo
import ca.mudar.huegoallight.hockey.HockeyUtils
import ca.mudar.huegoallight.hockey.NhlTeam
import ca.mudar.huegoallight.hue.HueBridgeManager

class OnboardingTeamsViewModel : ViewModel() {

    val teamList: LiveData<List<NhlTeam>>
        get() = _teamList
    val isProgressBarVisible: LiveData<Boolean>
        get() = _isProgressBarVisible
    val isRecyclerVisible: LiveData<Boolean>
        get() = _isRecyclerVisible
    val isDataReady: LiveData<Boolean>
        get() = _isDataReady

    val bridgeDiscoveryCount: Int? // Value to send to MainActivity
        get() = _bridgeDiscoveryCount.value

    val selectedTeam: String?
        get() = _teamList.value?.let { teams ->
            when (teams.size) {
                1 -> teams.first().slug
                else -> null
            }
        }

    private val _teamList = MutableLiveData<List<NhlTeam>>()
    private val _isProgressBarVisible = MutableLiveData<Boolean>(true)
    private val _isRecyclerVisible = MutableLiveData<Boolean>(false)
    private val _isDataReady = MediatorLiveData<Boolean>()
    private var _bridgeDiscoveryCount = MutableLiveData<Int?>(null)

    val onTeamSelectedListener: (String) -> Unit = { team ->
        _teamList.value = listOf(NhlTeam(team))

        if (_isDataReady.value != true) {
            _isProgressBarVisible.value = true
        }
    }

    init {
        _teamList.value = HockeyUtils.nhlTeams

        HueBridgeManager.countBridgeDiscoveryResults { bridgeCount ->
            _bridgeDiscoveryCount.value = bridgeCount
        }

        /*
         * _isDataReady is true once user has selected a team,
         * and bridge discovery is done (regardless of results count)
         */
        _isDataReady.addSource(_bridgeDiscoveryCount) { bridgeCount ->
            _isDataReady.value = bridgeCount != null && selectedTeam != null
        }
        _isDataReady.addSource(_teamList) {
            _isDataReady.value = selectedTeam != null && _bridgeDiscoveryCount.value != null
        }
    }

    fun setupFcm() {
        _isProgressBarVisible.value = true
        FcmRepo.register()
    }

    fun onFcmRegistrationResult() {
        _isProgressBarVisible.value = selectedTeam != null
        _isRecyclerVisible.value = true
    }

    companion object {
        private val TAG = makeLogTag("OnboardingVM")

        fun instanceOf(owner: ViewModelStoreOwner): OnboardingTeamsViewModel =
                ViewModelProvider(owner).get(OnboardingTeamsViewModel::class.java)
    }
}

/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import ca.mudar.huegoallight.common.Const.EMPTY_NBSP_STRING
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.hue.HueBridgeManager
import ca.mudar.huegoallight.models.BridgeLightSelection
import ca.mudar.huegoallight.models.HueLight
import ca.mudar.huegoallight.prefs.HuePrefs
import ca.mudar.huegoallight.viewmodel.factory.LightsBoardViewModelFactory

class LightsBoardViewModel(
    private val lightSelection: BridgeLightSelection
) : ViewModel() {

    // Title binding is needed to avoid DataBindingUtil indexOutOfBounds bug
    val title = EMPTY_NBSP_STRING

    val unusedLights = MutableLiveData<List<HueLight>>()
    val primaryLights = MutableLiveData<List<HueLight>>()
    val secondaryLights = MutableLiveData<List<HueLight>>()
    val redLights = MutableLiveData<List<HueLight>>()

    val readyToScroll: LiveData<Boolean>
        get() = _readyToScroll
    private val _readyToScroll = MutableLiveData<Boolean>(false)

    init {
        HueBridgeManager.resetAllLightsAlertMode()

        HueBridgeManager.getAllHueLights { lights ->
            addLightsToBoard(lights.sortedBy { it.name })
        }
    }

    private fun addLightsToBoard(allLights: List<HueLight>) {
        val unused = mutableListOf<HueLight>()
        val primary = mutableListOf<HueLight>()
        val secondary = mutableListOf<HueLight>()
        val red = mutableListOf<HueLight>()

        for (light in allLights) {
            when {
                lightSelection.isPrimaryLight(light.id) -> primary.add(light)
                lightSelection.isSecondaryLight(light.id) -> secondary.add(light)
                lightSelection.isRedLight(light.id) -> red.add(light)
                else -> unused.add(light)
            }
        }

        unusedLights.postValue(unused)
        primaryLights.postValue(primary)
        secondaryLights.postValue(secondary)
        redLights.postValue(red)

        _readyToScroll.postValue(true)
    }

    override fun onCleared() {
        super.onCleared()

        HueBridgeManager.setupBridgeLightSelection()
    }

    companion object {
        private val TAG = makeLogTag("LightsBoardViewModel")

        fun instanceOf(owner: ViewModelStoreOwner, hueFcmPrefs: HuePrefs): LightsBoardViewModel =
                ViewModelProvider(owner, LightsBoardViewModelFactory(hueFcmPrefs))
                        .get(LightsBoardViewModel::class.java)
    }
}

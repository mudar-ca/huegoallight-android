/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.hue.HueBridgeManager
import ca.mudar.huegoallight.models.HueBridgeState

class BridgeListViewModel internal constructor() : ViewModel() {

    val isProgressBarVisible: LiveData<Boolean>
        get() = _isProgressBarVisible

    private val _isProgressBarVisible = MediatorLiveData<Boolean>()

    init {
        _isProgressBarVisible.addSource(HueBridgeManager.state) { state ->
            _isProgressBarVisible.value = state != HueBridgeState.BridgeDiscoveryResults
        }
    }

    companion object {
        private val TAG = makeLogTag("BridgeListViewModel")

        fun instanceOf(owner: ViewModelStoreOwner): BridgeListViewModel =
                ViewModelProvider(owner).get(BridgeListViewModel::class.java)
    }
}

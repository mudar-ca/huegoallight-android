/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import ca.mudar.huegoallight.common.LogUtils.LOGV
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.hue.HueBridgeManager
import ca.mudar.huegoallight.models.HueSdkError

class HueSetupViewModel internal constructor() : ViewModel() {

    val isErrorMessageVisible = MutableLiveData(false)
    val isRetryButtonVisible = MutableLiveData(false)
    val isOnboardingTitleVisible = MutableLiveData(false)
    val errorMessage = MutableLiveData<String?>()

    override fun onCleared() {
        super.onCleared()

        resetState()
    }

    private fun resetState() {
        isErrorMessageVisible.value = false
        isRetryButtonVisible.value = false
        errorMessage.value = null
    }

    fun onRetryClick() {
        resetState()

        setupHueBridge(false)
    }

    fun onError(hueSdkError: HueSdkError) {
        errorMessage.value = hueSdkError.message

        isErrorMessageVisible.value = true
        isRetryButtonVisible.value = true
    }

    /**
     * Setup Hue SDK and Listener.
     */
    fun setupHueBridge(autoConnect: Boolean = true) {
        LOGV(TAG, "setupHueBridge, autoConnect: $autoConnect")

        HueBridgeManager.startBridgeDiscovery { bridgeDiscoveryResultList ->
            LOGV(TAG, "onBridgeDiscoveryResultList")

            if (autoConnect && bridgeDiscoveryResultList.size == 1) {
                HueBridgeManager.connectToBridge()
            }
        }
    }

    companion object {
        private val TAG = makeLogTag("HueSetupViewModel")

        fun instanceOf(owner: ViewModelStoreOwner): HueSetupViewModel =
                ViewModelProvider(owner).get(HueSetupViewModel::class.java)
    }
}

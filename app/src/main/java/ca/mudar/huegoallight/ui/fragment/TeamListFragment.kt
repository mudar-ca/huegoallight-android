/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.fragment

import android.content.ContextWrapper
import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.Const.UNKNOWN_VALUE
import ca.mudar.huegoallight.common.LogUtils.REMOTE_LOG
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.databinding.FragmentTeamListBinding
import ca.mudar.huegoallight.fcm.FcmRepo
import ca.mudar.huegoallight.fcm.FcmUtils
import ca.mudar.huegoallight.models.FcmEvent
import ca.mudar.huegoallight.models.FcmRegistrationError
import ca.mudar.huegoallight.models.FcmRegistrationSuccess
import ca.mudar.huegoallight.models.FcmTopicSubscriptionError
import ca.mudar.huegoallight.prefs.UserPrefs
import ca.mudar.huegoallight.ui.activity.MainActivity
import ca.mudar.huegoallight.ui.adapter.NhlTeamsAdapter
import ca.mudar.huegoallight.utils.ConnectionUtils
import ca.mudar.huegoallight.viewmodel.OnboardingTeamsViewModel
import com.google.android.material.snackbar.Snackbar

class TeamListFragment : Fragment() {

    private var binding: FragmentTeamListBinding? = null
    private val viewModel by lazy { OnboardingTeamsViewModel.instanceOf(this) }

    private val fcmEventListener = Observer<FcmEvent> { fcmEvent ->
        when (fcmEvent) {
            is FcmTopicSubscriptionError -> {
                // TODO Show snackbar error message before finish()
                REMOTE_LOG(fcmEvent.exception)
            }
            is FcmRegistrationError -> showFcmErrorSnackbar()
            is FcmRegistrationSuccess -> viewModel.onFcmRegistrationResult()
        }
    }

    private val itemDecoration by lazy {
        object : RecyclerView.ItemDecoration() {
            private val topPadding: Int =
                resources.getDimensionPixelSize(R.dimen.onboarding_logo_top_padding)
            private val horizontalPadding: Int =
                resources.getDimensionPixelSize(R.dimen.onboarding_logo_horizontal_padding)

            override fun getItemOffsets(
                outRect: Rect,
                view: View,
                parent: RecyclerView,
                state: RecyclerView.State
            ) {
                with(outRect) {
                    if (parent.getChildAdapterPosition(view) == 0) {
                        left = horizontalPadding
                    }
                    top = topPadding
                    right = horizontalPadding
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_team_list, container, false)

        binding?.let {
            it.lifecycleOwner = viewLifecycleOwner
            it.viewModel = viewModel
            setupRecyclerView(it.recycler)
        }

        FcmRepo.event.observe(viewLifecycleOwner, fcmEventListener)

        viewModel.isDataReady.observe(viewLifecycleOwner, Observer { isReady ->
            if (isReady) {
                finishWhenDone(
                    bridgeCount = viewModel.bridgeDiscoveryCount ?: UNKNOWN_VALUE,
                    team = viewModel.selectedTeam ?: ""
                )
            }
        })

        return binding?.root
    }

    override fun onResume() {
        super.onResume()

        setupFcm()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    private fun setupRecyclerView(recycler: RecyclerView) {
        val nhlTeamsAdapter = NhlTeamsAdapter(viewModel.onTeamSelectedListener)

        viewModel.teamList.observe(viewLifecycleOwner, Observer { results ->
            nhlTeamsAdapter.swapDataset(results)
        })

        with(recycler) {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = nhlTeamsAdapter
            addItemDecoration(itemDecoration)
        }
    }

    private fun setupFcm() {
        if (ConnectionUtils.hasConnection(context)) {
            activity?.apply {
                if (FcmUtils.checkPlayServices(this)) {
                    viewModel.setupFcm()
                }
            }
        } else {
            showConnectionErrorSnackbar()
        }
    }

    /**
     * First Onboarding is closed after user has selected a team,
     * and bridge discovery is done (regardless of results count)
     */
    private fun finishWhenDone(bridgeCount: Int, team: String) {
        if (team.isNotBlank()) {
            // Set as default team and clear Onboarding flag
            UserPrefs.setOnboardingFollowedTeam(team)

            // Subscribe to Team topic
            FcmRepo.toggleTopicSubscription(team, true)
            FcmUtils.sendAnalyticsTeamFollowedEvent(ContextWrapper(context), team, true)
        }

        // Next Onboarding screen: HueSetup
        context?.let {
            startActivity(MainActivity.newIntent(it, true, bridgeCount))
            activity?.finish()
        }
    }

    private fun showConnectionErrorSnackbar() {
        binding?.mainContent?.let { view ->
            Snackbar.make(view, R.string.snackbar_no_connection, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.snackbar_btn_retry) {
                    setupFcm()
                }.show()
        }
    }

    private fun showFcmErrorSnackbar() {
        binding?.mainContent?.let { view ->
            Snackbar.make(view, R.string.snackbar_fcm_onboarding_error, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.btn_ok) {
                    viewModel.onFcmRegistrationResult()
                }.show()
        }
    }

    companion object {
        private val TAG = makeLogTag("TeamListFragment")

        fun newInstance(): TeamListFragment {
            return TeamListFragment()
        }
    }
}

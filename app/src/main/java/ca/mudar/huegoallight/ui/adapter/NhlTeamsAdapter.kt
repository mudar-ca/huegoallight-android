/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.databinding.ItemTeamBinding
import ca.mudar.huegoallight.hockey.NhlTeam
import ca.mudar.huegoallight.ui.widget.BindingViewHolder
import ca.mudar.huegoallight.viewmodel.TeamVM

class NhlTeamsAdapter(
    private val teamSelectionListener: (String) -> Unit
) : RecyclerView.Adapter<BindingViewHolder>() {

    private var dataset = listOf<NhlTeam>()

    private val isClickListenerEnabled
        get() = dataset.size > 1

    override fun getItemCount(): Int =
            dataset.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingViewHolder {
        val binding: ItemTeamBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context),
                R.layout.item_team, parent, false)
        return BindingViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BindingViewHolder, position: Int) {
        dataset.getOrNull(position)?.let { nhlTeam ->
            (holder.binding as? ItemTeamBinding)?.apply {
                viewModel = TeamVM(nhlTeam = nhlTeam)
                onClickListener = View.OnClickListener { onTeamClickListener(holder) }
            }
            holder.itemView.isEnabled = isClickListenerEnabled
        }
    }

    fun swapDataset(data: List<NhlTeam>) {
        val oldSize = dataset.size
        val newSize = data.size
        dataset = data

        when {
            oldSize < newSize -> notifyItemRangeInserted(0, newSize)
            oldSize > newSize -> notifyItemRangeRemoved(newSize, oldSize)
            else -> notifyDataSetChanged()
        }
    }

    private fun onTeamClickListener(holder: BindingViewHolder) {
        holder.itemView.isEnabled = false

        if (isClickListenerEnabled) {
            val position = holder.adapterPosition
            dataset.getOrNull(position)?.let { team ->
                notifyItemMoved(position, 0)
                teamSelectionListener(team.slug)
            }
        }
    }

    companion object {
        private val TAG = makeLogTag("NhlTeamsAdapter")
    }
}

/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.fragment

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.SharedPreferences
import android.media.RingtoneManager
import android.net.Uri
import android.os.Bundle
import android.os.Vibrator
import android.provider.Settings
import android.text.TextUtils
import androidx.preference.CheckBoxPreference
import androidx.preference.Preference
import androidx.preference.PreferenceCategory
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.TwoStatePreference
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.Const.PrefsNames
import ca.mudar.huegoallight.common.Const.PrefsValues
import ca.mudar.huegoallight.common.Const.RequestCodes
import ca.mudar.huegoallight.common.Const.SUPPORTS_OREO
import ca.mudar.huegoallight.common.Const.USER_PREFS_NAME
import ca.mudar.huegoallight.hockey.NhlTeam
import ca.mudar.huegoallight.hue.HueBridgeManager
import ca.mudar.huegoallight.models.HueLight
import ca.mudar.huegoallight.prefs.HuePrefs
import ca.mudar.huegoallight.prefs.UserPrefs
import ca.mudar.huegoallight.ui.activity.HueSetupActivity
import ca.mudar.huegoallight.ui.activity.LightsBoardActivity
import ca.mudar.huegoallight.ui.activity.SettingsNhlTeamsActivity
import ca.mudar.huegoallight.ui.activity.SettingsQuietHoursActivity
import ca.mudar.huegoallight.utils.ConnectionUtils
import ca.mudar.huegoallight.utils.NotificationUtils
import ca.mudar.huegoallight.utils.PermissionsManager
import ca.mudar.huegoallight.utils.TimeUtils
import ca.mudar.huegoallight.utils.requirePreference
import java.util.ArrayList

class SettingsFragment : PreferenceFragmentCompat(),
    SharedPreferences.OnSharedPreferenceChangeListener {

    private lateinit var sharedPrefs: SharedPreferences

    private var ringtonePref: Preference? = null
    private lateinit var bridgePref: Preference
    private lateinit var lightsBoardPref: Preference
    private lateinit var blinkDurationPref: Preference
    private lateinit var batteryOptimizationPermsPref: TwoStatePreference

    private val teamsSummary: String
        get() {
            val teams = UserPrefs.followedNhlTeams

            return when (teams.size) {
                0 -> {
                    // Not following any teams
                    resources.getString(R.string.prefs_summary_teams_followed_none)
                }
                1 -> {
                    // Following a single team: full team name
                    resources.getString(NhlTeam(teams[0]).teamName)
                }
                else -> {
                    // Following multiple teams: short team names
                    val teamNames = ArrayList<String>()
                    for (slug in teams) {
                        teamNames.add(resources.getString(NhlTeam(slug).teamShortName))
                    }
                    TextUtils.join(
                        resources.getString(R.string.prefs_summary_list_delimeter),
                        teamNames
                    )
                }
            }
        }

    private val quietHoursSummary: String
        get() {
            if (sharedPrefs.getBoolean(
                    PrefsNames.QUIET_HOURS_ENABLED,
                    PrefsValues.DEFAULT_QUIET_HOURS_ENABLED
                )
            ) {
                val context = this.context
                    ?: return ""

                val start = TimeUtils.getTimeDisplay(
                    context,
                    sharedPrefs.getInt(
                        PrefsNames.QUIET_HOURS_START,
                        PrefsValues.DEFAULT_QUIET_HOURS_START
                    )
                )
                val end = TimeUtils.getTimeDisplay(
                    context,
                    sharedPrefs.getInt(
                        PrefsNames.QUIET_HOURS_END,
                        PrefsValues.DEFAULT_QUIET_HOURS_END
                    )
                )

                return resources.getString(R.string.prefs_summary_quiet_hours_period, start, end)
            }

            return resources.getString(R.string.prefs_summary_quiet_hours_off)
        }

    private val ringtoneSummary: String
        get() {
            val path = sharedPrefs.getString(PrefsNames.RINGTONE, PrefsValues.RINGTONE_SILENT)
            if (path?.isNotEmpty() == true) {
                val ringtone = RingtoneManager.getRingtone(context, Uri.parse(path))
                if (ringtone == null) {
                    // revert to silent ringtone
                    sharedPrefs.edit().putString(PrefsNames.RINGTONE, PrefsValues.RINGTONE_SILENT)
                        .apply()
                } else {
                    return ringtone.getTitle(context)
                }
            }

            return resources.getString(R.string.prefs_summary_ringtone_silent)
        }

    private val blinkDurationSummary: String
        get() {
            val duration = sharedPrefs.getString(
                PrefsNames.BLINK_DURATION,
                PrefsValues.DEFAULT_BLINK_DURATION
            )

            return resources.getString(R.string.prefs_summary_hue_blink_duration, duration)
        }

    /**
     * This allows the checkbox state to change only if permission changed. UI updates onResume()
     */
    private val prefsChangeListener = Preference.OnPreferenceChangeListener { pref, _ ->
        if (pref.key == PrefsNames.BATTERY_OPTIMIZATION_PERMS) {
            activity?.let {
                PermissionsManager.showBatteryOptimizationSettings(it)
            }
        }

        return@OnPreferenceChangeListener false // to stop UI changes
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        val pm = this.preferenceManager.apply {
            sharedPreferencesName = USER_PREFS_NAME
            sharedPreferencesMode = Context.MODE_PRIVATE
        }

        addPreferencesFromResource(R.xml.prefs_settings)
        ringtonePref = findPreference(PrefsNames.RINGTONE)
        lightsBoardPref = requirePreference(PrefsNames.LIGHTS_BOARD)
        blinkDurationPref = requirePreference(PrefsNames.BLINK_DURATION)
        bridgePref = requirePreference(PrefsNames.BRIDGE)
        batteryOptimizationPermsPref = requirePreference(PrefsNames.BATTERY_OPTIMIZATION_PERMS)

        sharedPrefs = pm.sharedPreferences

        /*
         * Setup a listener whenever a key changes
         */
        sharedPrefs.registerOnSharedPreferenceChangeListener(this)
        batteryOptimizationPermsPref.onPreferenceChangeListener = prefsChangeListener

        removeVibrationIfNotSupported()
    }

    override fun onResume() {
        super.onResume()

        setupSummaries()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RequestCodes.RINGTONE_PICKER) {
            onRingtonePickerResult(resultCode, data)
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        /*
         * Remove the listener
         */
        sharedPrefs.unregisterOnSharedPreferenceChangeListener(this)
    }

    /**
     * Implements SharedPreferences.OnSharedPreferenceChangeListener
     */
    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        when (key) {
            PrefsNames.RINGTONE -> ringtonePref?.summary = ringtoneSummary
            PrefsNames.BLINK_DURATION -> blinkDurationPref.summary = blinkDurationSummary
        }
    }

    override fun onPreferenceTreeClick(preference: Preference?): Boolean {
        val context = context
            ?: return false
        val key = preference?.key
            ?: return false

        if (PrefsNames.QUIET_HOURS_ENABLED == key) {
            startActivity(SettingsQuietHoursActivity.newIntent(context))
            return true
        } else if (PrefsNames.SYSTEM_NOTIFICATIONS == key) {
            NotificationUtils.editAppNotificationsSettings(context)
            return true
        } else if (!ConnectionUtils.checkConnection(view)) {
            return true
        } else if (PrefsNames.NHL_FOLLOWED_TEAMS == key) {
            startActivity(SettingsNhlTeamsActivity.newIntent(context))
            return true
        } else if (PrefsNames.BRIDGE == key) {
            startActivity(HueSetupActivity.newIntent(context = context, autoConnect = false))
            return true
        } else if (PrefsNames.LIGHTS_BOARD == key) {
            startActivity(LightsBoardActivity.newIntent(context))
            return true
        } else if (PrefsNames.RINGTONE == key) {
            showRingtonePreferenceDialog()
            return true
        }

        return super.onPreferenceTreeClick(preference)
    }

    private fun setupSummaries() {
        ringtonePref?.summary = ringtoneSummary
        blinkDurationPref.summary = blinkDurationSummary
        batteryOptimizationPermsPref.isChecked =
            PermissionsManager.checkBatteryOptimizationWhitelist(ContextWrapper(context))

        findPreference<Preference>(PrefsNames.NHL_FOLLOWED_TEAMS)?.summary = teamsSummary
        findPreference<Preference>(PrefsNames.QUIET_HOURS_ENABLED)?.summary = quietHoursSummary

        setupBridgeSummary(HueBridgeManager.currentBridgeId.orEmpty())
        HueBridgeManager.getAllHueLights { setupHueLightsSummary(it) }
    }

    private fun setupBridgeSummary(bridgeId: String) {
        bridgePref.summary = bridgeId
    }

    private fun setupHueLightsSummary(lights: List<HueLight>) {
        lightsBoardPref.summary = getSelectedLightsSummary(lights)
        lightsBoardPref.isEnabled = lights.isNotEmpty()
    }

    private fun removeVibrationIfNotSupported() {
        if (SUPPORTS_OREO) {
            return
        }

        val vibratorService =
            context?.applicationContext?.getSystemService(Context.VIBRATOR_SERVICE) as? Vibrator
        if (vibratorService?.hasVibrator() != true) {
            val vibrationPref = (findPreference(PrefsNames.HAS_VIBRATION) as? CheckBoxPreference)
            vibrationPref?.let { checkBoxPref ->
                checkBoxPref.isChecked = false
                findPreference<PreferenceCategory>(PrefsNames.CATEGORY_NOTIFICATIONS)
                    ?.removePreference(checkBoxPref)
            }
        }
    }

    private fun getSelectedLightsSummary(allLights: List<HueLight>): String {
        if (allLights.isEmpty()) {
            return resources.getString(R.string.prefs_summary_hue_available_lights_error)
        }

        val selectedLights = mutableListOf<String>().apply {
            addAll(HuePrefs.primaryLights)
            addAll(HuePrefs.secondaryLights)
            addAll(HuePrefs.redLights)
        }

        val summaryNames: List<String> = allLights
            .filter { selectedLights.contains(it.id) }
            .map { it.name }
            .sorted()

        return when {
            summaryNames.isNotEmpty() -> summaryNames.joinToString { it }
            else -> resources.getString(R.string.prefs_summary_hue_selected_lights_none)
        }
    }

    private fun showRingtonePreferenceDialog() {
        val intent = Intent(RingtoneManager.ACTION_RINGTONE_PICKER).apply {
            putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION)
            putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, true)
            putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true)
            putExtra(
                RingtoneManager.EXTRA_RINGTONE_DEFAULT_URI,
                Settings.System.DEFAULT_NOTIFICATION_URI
            )
            // Select current value
            putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, UserPrefs.ringtoneUri)
        }

        startActivityForResult(intent, RequestCodes.RINGTONE_PICKER)
    }

    private fun onRingtonePickerResult(resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            UserPrefs.ringtoneUri =
                data?.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI) as? Uri
        }
    }

    companion object {
        fun newInstance(): SettingsFragment {
            return SettingsFragment()
        }
    }
}

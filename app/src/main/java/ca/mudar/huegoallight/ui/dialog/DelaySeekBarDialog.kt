/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.dialog

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.Const.BundleKeys
import ca.mudar.huegoallight.common.Const.FragmentTags
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.ui.widget.RadialTimePickerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class DelaySeekBarDialog : DialogFragment(), DialogInterface.OnClickListener {

    private var seekBarView: RadialTimePickerView? = null
    private var headerView: TextView? = null
    private var gameId: String = ""
    private var delay: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        retainInstance = true

        super.onCreate(savedInstanceState)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_delay_seekbar, null)

        // load gameId & delay
        loadInitialValues(savedInstanceState ?: arguments)

        headerView = view.findViewById(R.id.header)
        headerView?.text = getString(R.string.game_delay_seconds, delay)

        setupSeekBar(view, delay)

        val dialog = MaterialAlertDialogBuilder(requireActivity(), R.style.AppTheme_AlertDialog)
            .setTitle(null)
            .setView(view)
            .setPositiveButton(R.string.btn_ok, this)
            .setNegativeButton(R.string.btn_cancel, null) // no listener
            .setNeutralButton(R.string.btn_help, null) // no listener
            .create()

        setupNeutralButtonListener(dialog)

        return dialog
    }

    override fun onClick(dialog: DialogInterface, which: Int) {
        if (which == Dialog.BUTTON_POSITIVE) {
            (targetFragment as? SeekBarCallback)?.onSeekBarValueChange(gameId, delay)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putString(BundleKeys.GAME_ID, gameId)
        outState.putInt(BundleKeys.SECONDS, delay)
    }

    override fun onDestroyView() {
        if (retainInstance) {
            dialog?.setDismissMessage(null)
        }

        super.onDestroyView()
    }

    private fun loadInitialValues(bundle: Bundle?) {
        gameId = bundle?.getString(BundleKeys.GAME_ID, "") ?: ""
        delay = bundle?.getInt(BundleKeys.SECONDS, 0) ?: 0
    }

    private fun setupSeekBar(view: View, minutes: Int) {
        view.findViewById<RadialTimePickerView>(R.id.picker)?.let { seekBar ->
            seekBar.setCurrentItemShowing(RadialTimePickerView.MINUTES, false)
            seekBar.setOnValueSelectedListener { _, newValue, _ ->
                delay = newValue
                headerView?.text = getString(R.string.game_delay_seconds, delay)
            }

            seekBar.currentMinute = minutes
            seekBarView = seekBar
        }
    }

    /**
     * Handle the neutral button callback manually, to avoid dismissing the dialog
     */
    private fun setupNeutralButtonListener(alertDialog: AlertDialog) {
        alertDialog.setOnShowListener { dialog ->
            (dialog as? AlertDialog)?.getButton(AlertDialog.BUTTON_NEUTRAL)?.setOnClickListener {
                val bottomSheet = GameDelayHelpFragment.newInstance()
                bottomSheet.show(parentFragmentManager, FragmentTags.GAME_DELAY_HELP)
            }
        }
    }

    interface SeekBarCallback {
        fun showSeekBarDialog(gameId: String, delay: Int)

        fun onSeekBarValueChange(gameId: String, delay: Int)
    }

    companion object {
        private val TAG = makeLogTag("DelaySeekBarDialog")

        fun newInstance(id: String, value: Int): DelaySeekBarDialog {
            val dialog = DelaySeekBarDialog()

            val args = Bundle().apply {
                putString(BundleKeys.GAME_ID, id)
                putInt(BundleKeys.SECONDS, value)
            }
            dialog.arguments = args

            return dialog
        }
    }
}

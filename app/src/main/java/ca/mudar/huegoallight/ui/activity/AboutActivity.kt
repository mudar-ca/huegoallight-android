/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.activity

import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.Const.MIME_TYPE_PLAIN_TEXT
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.databinding.ActivityAboutBinding
import ca.mudar.huegoallight.fcm.FcmUtils
import com.mikepenz.aboutlibraries.LibsBuilder

class AboutActivity : AppCompatActivity() {
    private val TAG = makeLogTag("AboutActivity")

    /**
     * Handle clickable views
     */
    private val viewClickListener = View.OnClickListener {
        when (it.id) {
            R.id.about_credits -> showWebsite(R.string.url_mudar_ca)
            R.id.about_source_code -> showWebsite(R.string.url_gitlab)
            R.id.about_logos -> showWebsite(R.string.url_freevector_co)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityAboutBinding = DataBindingUtil
            .setContentView(this, R.layout.activity_about)

        binding.onClickListener = viewClickListener

        setupToolbar(binding.toolbar)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_about, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_rate -> {
                showWebsite(R.string.url_playstore)
                FcmUtils.sendAnalyticsRateEvent(ContextWrapper(this))
                true
            }
            R.id.action_share -> {
                onShareItemSelected()
                FcmUtils.sendAnalyticsShareEvent(ContextWrapper(this))
                true
            }
            R.id.action_eula -> {
                startActivity(EulaActivity.newIntent(this))
                true
            }
            R.id.action_about_libs -> {
                onAboutLibsItemSelected()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setupToolbar(toolbar: Toolbar) {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setTitle(R.string.app_name)
    }

    /**
     * Native sharing
     */
    private fun onShareItemSelected() {
        val extras = Bundle().apply {
            putString(Intent.EXTRA_SUBJECT, resources.getString(R.string.share_intent_title))
            putString(Intent.EXTRA_TEXT, resources.getString(R.string.url_playstore))
        }
        val sendIntent = Intent().apply {
            putExtras(extras)
            action = Intent.ACTION_SEND
            type = MIME_TYPE_PLAIN_TEXT
        }
        startActivity(Intent.createChooser(sendIntent, null))
    }

    /**
     * Show the AboutLibraries acknowledgements activity
     */
    private fun onAboutLibsItemSelected() {
        LibsBuilder()
            .withActivityTitle(getString(R.string.activity_about_libs))
            .withActivityTheme(R.style.AppTheme_AboutLibs)
            .withAutoDetect(false) // For Proguard
            .withFields(R.string::class.java.fields) // For Proguard
            .withLibraries(
                "Crashlytics",
                "GooglePlayServices",
                "material_tap_target_prompt",
                "moshi",
                "OkHttp",
                "Retrofit",
                "jetpack",
                "shade",
                "draglistview"
            ) // Added manually to avoid issues with Proguard
            .withExcludedLibraries(
                "AndroidIconics",
                "constraint_layout",
                "fastadapter",
                "intellijannotations",
                "okio",
                "support_annotations"
            )
            .withAboutIconShown(false)
            .withAboutVersionShown(false)
            .withVersionShown(false)
            .withSortEnabled(true)
            .start(this)
    }

    /**
     * Launch intent to view website
     */
    private fun showWebsite(@StringRes website: Int) {
        val viewIntent = Intent(Intent.ACTION_VIEW).apply {
            data = Uri.parse(resources.getString(website))
        }
        startActivity(viewIntent)
    }

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, AboutActivity::class.java)
        }
    }
}

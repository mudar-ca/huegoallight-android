/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.databinding.FragmentBridgeListBinding
import ca.mudar.huegoallight.hue.HueBridgeManager
import ca.mudar.huegoallight.ui.adapter.BridgesAdapter
import ca.mudar.huegoallight.viewmodel.BridgeListViewModel

class BridgeListFragment : Fragment() {

    private val viewModel by lazy { BridgeListViewModel.instanceOf(this) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentBridgeListBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_bridge_list, container, false)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        setupRecyclerView(binding.recycler)

        return binding.root
    }

    private fun setupRecyclerView(recycler: RecyclerView) {
        val adapter = BridgesAdapter()
        HueBridgeManager.discoveryResults.observe(viewLifecycleOwner, Observer { results ->
            adapter.swapDataset(results)
        })

        recycler.layoutManager = LinearLayoutManager(recycler.context)
        recycler.adapter = adapter
    }

    companion object {
        private val TAG = makeLogTag("BridgeListFragment")

        fun newInstance(): BridgeListFragment {
            return BridgeListFragment()
        }
    }
}

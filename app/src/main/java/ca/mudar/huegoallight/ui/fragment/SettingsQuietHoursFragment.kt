/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.Const.FragmentTags
import ca.mudar.huegoallight.common.Const.PrefsNames
import ca.mudar.huegoallight.common.Const.PrefsValues
import ca.mudar.huegoallight.common.Const.USER_PREFS_NAME
import ca.mudar.huegoallight.models.TimePeriodField
import ca.mudar.huegoallight.prefs.UserPrefs
import ca.mudar.huegoallight.ui.dialog.TimePickerFragment
import ca.mudar.huegoallight.utils.TimeUtils
import ca.mudar.huegoallight.utils.requirePreference

class SettingsQuietHoursFragment : PreferenceFragmentCompat(),
    SharedPreferences.OnSharedPreferenceChangeListener {

    private lateinit var sharedPrefs: SharedPreferences
    private lateinit var startTimePref: Preference
    private lateinit var endTimePref: Preference

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        sharedPrefs = this.preferenceManager
            .apply {
                sharedPreferencesName = USER_PREFS_NAME
                sharedPreferencesMode = Context.MODE_PRIVATE
            }
            .sharedPreferences

        addPreferencesFromResource(R.xml.prefs_quiet_hours)

        startTimePref = requirePreference(PrefsNames.QUIET_HOURS_START)
        endTimePref = requirePreference(PrefsNames.QUIET_HOURS_END)

        sharedPrefs.registerOnSharedPreferenceChangeListener(this)
    }

    override fun onResume() {
        super.onResume()

        setupSummaries()
        toggleEnabled(
            sharedPrefs.getBoolean(
                PrefsNames.QUIET_HOURS_ENABLED,
                PrefsValues.DEFAULT_QUIET_HOURS_ENABLED
            )
        )
    }

    override fun onDestroy() {
        super.onDestroy()

        sharedPrefs.unregisterOnSharedPreferenceChangeListener(this)
    }

    /**
     * Implements OnSharedPreferenceChangeListener
     */
    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        val prefs = sharedPreferences ?: sharedPrefs

        when (key) {
            PrefsNames.QUIET_HOURS_START,
            PrefsNames.QUIET_HOURS_END -> {
                val context = this.context
                    ?: return

                if (PrefsNames.QUIET_HOURS_START == key) {
                    startTimePref.summary = TimeUtils.getTimeDisplay(
                        context,
                        prefs.getInt(
                            PrefsNames.QUIET_HOURS_START,
                            PrefsValues.DEFAULT_QUIET_HOURS_START
                        )
                    )
                }

                // In both cases, we need to update endTime for the overnight suffix
                endTimePref.summary = getEndTimeSuffixIfNecessary(
                    TimeUtils.getTimeDisplay(
                        context,
                        prefs.getInt(
                            PrefsNames.QUIET_HOURS_END,
                            PrefsValues.DEFAULT_QUIET_HOURS_END
                        )
                    )
                )
            }
            PrefsNames.QUIET_HOURS_ENABLED ->
                toggleEnabled(
                    prefs.getBoolean(
                        PrefsNames.QUIET_HOURS_ENABLED,
                        PrefsValues.DEFAULT_QUIET_HOURS_ENABLED
                    )
                )
        }
    }

    override fun onPreferenceTreeClick(preference: Preference?): Boolean =
        when (val key = preference?.key) {
            PrefsNames.QUIET_HOURS_START,
            PrefsNames.QUIET_HOURS_END -> {
                showTimePickerDialog(key)
                true
            }
            else -> super.onPreferenceTreeClick(preference)
        }

    private fun toggleEnabled(enabled: Boolean) {
        startTimePref.isEnabled = enabled
        endTimePref.isEnabled = enabled
    }

    private fun setupSummaries() {
        val context = this.context
            ?: return

        startTimePref.summary = TimeUtils.getTimeDisplay(context, UserPrefs.quietHoursStart)
        endTimePref.summary = getEndTimeSuffixIfNecessary(
            TimeUtils.getTimeDisplay(context, UserPrefs.quietHoursEnd)
        )
    }

    private fun getEndTimeSuffixIfNecessary(endTime: String): String {
        val hasSuffix = UserPrefs.isQuietHoursOvernight()
        return when {
            hasSuffix -> resources.getString(R.string.prefs_summary_quiet_hours_overnight, endTime)
            else -> endTime
        }
    }

    private fun showTimePickerDialog(key: String) {
        val dialog = when (key) {
            PrefsNames.QUIET_HOURS_START -> TimePickerFragment.newInstance(
                TimePeriodField.Start,
                TimeUtils.getCalendarInstance(UserPrefs.quietHoursStart)
            )
            PrefsNames.QUIET_HOURS_END -> TimePickerFragment.newInstance(
                TimePeriodField.End,
                TimeUtils.getCalendarInstance(UserPrefs.quietHoursEnd)
            )
            else -> null
        }
        dialog?.show(parentFragmentManager, FragmentTags.TIME_PICKER)
    }

    companion object {
        fun newInstance(): SettingsQuietHoursFragment {
            return SettingsQuietHoursFragment()
        }
    }
}

/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.fragment

import android.content.Context
import android.net.wifi.WifiManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.databinding.FragmentHueErrorBinding
import ca.mudar.huegoallight.hue.HueBridgeManager
import ca.mudar.huegoallight.models.PushlinkExpiredError
import ca.mudar.huegoallight.utils.ConnectionUtils
import ca.mudar.huegoallight.viewmodel.HueSetupViewModel
import com.google.android.material.snackbar.Snackbar

class HueErrorFragment : Fragment() {

    private var binding: FragmentHueErrorBinding? = null
    private lateinit var activityViewModel: HueSetupViewModel

    private val retryClickListener = View.OnClickListener { view ->
        if (ConnectionUtils.checkConnection(view)) {
            activityViewModel.onRetryClick()

            val wifiManager =
                (view.context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager)
            HueBridgeManager.scanNetworkForUpnpBridge(wifiManager)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activityViewModel = HueSetupViewModel.instanceOf(requireActivity())

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_hue_error, container, false)

        binding?.apply {
            lifecycleOwner = viewLifecycleOwner
            viewModel = activityViewModel
            onClickListener = retryClickListener
        }

        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (HueBridgeManager.error.value is PushlinkExpiredError) {
            showPushlinkErrorSnackbar()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    private fun showPushlinkErrorSnackbar() {
        binding?.mainContent?.let { view ->
            Snackbar.make(view, R.string.snackbar_pushlink_error, Snackbar.LENGTH_LONG).show()
        }
    }

    companion object {
        private val TAG = makeLogTag("HueErrorFragment")

        fun newInstance(): HueErrorFragment {
            return HueErrorFragment()
        }
    }
}

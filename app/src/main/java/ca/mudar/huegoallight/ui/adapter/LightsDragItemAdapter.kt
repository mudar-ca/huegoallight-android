/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.databinding.ItemLightsGridBinding
import ca.mudar.huegoallight.models.HueLight
import ca.mudar.huegoallight.viewmodel.LightVM
import com.woxthebox.draglistview.DragItemAdapter

class LightsDragItemAdapter(
    lights: List<HueLight>
) : DragItemAdapter<HueLight, LightsDragItemAdapter.LightViewHolder>() {

    init {
        this.mItemList = lights
    }

    override fun getUniqueItemId(position: Int): Long {
        val uniqueId: Int = mItemList.getOrNull(position)?.id?.hashCode()
            ?: position
        return uniqueId.toLong()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LightViewHolder {
        val binding: ItemLightsGridBinding = DataBindingUtil
            .inflate(LayoutInflater.from(parent.context), R.layout.item_lights_grid, parent, false)

        return LightViewHolder(binding)
    }

    override fun onBindViewHolder(holder: LightViewHolder, position: Int) {
        mItemList?.elementAtOrNull(position)?.let {
            super.onBindViewHolder(holder, position)

            val viewModel = LightVM(it)
            holder.binding.setVariable(BR.viewModel, viewModel)
            holder.binding.executePendingBindings()
        }
    }

    class LightViewHolder(
        val binding: ViewDataBinding
    ) : DragItemAdapter.ViewHolder(binding.root, binding.root.id, false)

    companion object {
        private val TAG = makeLogTag("LightsDragItemAdapter")
    }
}

/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.Const.BundleKeys
import ca.mudar.huegoallight.common.Const.FragmentTags
import ca.mudar.huegoallight.common.LogUtils.LOGE
import ca.mudar.huegoallight.common.LogUtils.LOGV
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.databinding.ActivityHueSetupBinding
import ca.mudar.huegoallight.hue.HueBridgeManager
import ca.mudar.huegoallight.models.HueBridgeState
import ca.mudar.huegoallight.models.HueSdkError
import ca.mudar.huegoallight.ui.fragment.BridgeListFragment
import ca.mudar.huegoallight.ui.fragment.HueErrorFragment
import ca.mudar.huegoallight.ui.fragment.PushlinkFragment
import ca.mudar.huegoallight.viewmodel.HueSetupViewModel

class HueSetupActivity : AppCompatActivity() {

    private val viewModel by lazy { HueSetupViewModel.instanceOf(this) }

    @Suppress("NON_EXHAUSTIVE_WHEN")
    private val bridgeStateObserver = Observer<HueBridgeState> { state ->
        LOGV(TAG, "Observer, state: $state")

        when (state) {
            HueBridgeState.BridgeDiscoveryRunning,
            HueBridgeState.BridgeDiscoveryResults -> showFragment(FragmentTags.BRIDGES)
            HueBridgeState.Pushlinking -> showFragment(FragmentTags.PUSHLINK)
            HueBridgeState.Connected -> onBridgeConnectedAndInitialized()
        }
    }

    private val bridgeErrorObserver = Observer<HueSdkError?> { hueError ->
        hueError?.let {
            LOGE(TAG, "bridgeErrorObserver, hueError: $hueError")
            viewModel.onError(it)
            showFragment(FragmentTags.HUE_ERROR)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil
            .setContentView<ActivityHueSetupBinding>(this, R.layout.activity_hue_setup)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        val isOnboarding = intent.getBooleanExtra(BundleKeys.IS_ONBOARDING, false)
        val autoConnect = intent.getBooleanExtra(BundleKeys.AUTO_CONNECT, true)

        setupToolbar(toolbar = binding.toolbar, useDefaultToolbar = isOnboarding.not())

        viewModel.isOnboardingTitleVisible.value = isOnboarding
        if (savedInstanceState == null) {
            viewModel.setupHueBridge(autoConnect)
        }

        setupHueBridgeObservers()
    }

    private fun setupHueBridgeObservers() {
        HueBridgeManager.state.observe(this, bridgeStateObserver)
        HueBridgeManager.error.observe(this, bridgeErrorObserver)
    }

    private fun setupToolbar(toolbar: Toolbar, useDefaultToolbar: Boolean) {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayShowTitleEnabled(useDefaultToolbar)
            setDisplayHomeAsUpEnabled(useDefaultToolbar)
        }
    }

    private fun onBridgeConnectedAndInitialized() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    private fun showFragment(tag: String) {
        val fragment = findOrCreateFragmentByTag(supportFragmentManager, tag)

        if (fragment.isVisible.not() && isFinishing.not()) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment, tag)
                .commit()
        }
    }

    private fun findOrCreateFragmentByTag(fm: FragmentManager, tag: String): Fragment {
        return fm.findFragmentByTag(tag) ?: run {
            when (tag) {
                FragmentTags.PUSHLINK -> PushlinkFragment.newInstance()
                FragmentTags.BRIDGES -> BridgeListFragment.newInstance()
                FragmentTags.HUE_ERROR -> HueErrorFragment.newInstance()
                else -> Fragment() // Should not happen
            }
        }
    }

    companion object {
        private val TAG = makeLogTag("HueSetupActivity")

        // Enforce singleTop to avoid multi-instances.
        // Related to quick BusSync events received before unregistering
        private const val INTENT_FLAGS =
            Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP

        fun newIntent(
            context: Context,
            autoConnect: Boolean = true,
            isOnboarding: Boolean = false
        ): Intent {
            val intent = Intent(context, HueSetupActivity::class.java)
            intent.flags = INTENT_FLAGS

            val extras = Bundle().apply {
                putBoolean(BundleKeys.AUTO_CONNECT, autoConnect)
                putBoolean(BundleKeys.IS_ONBOARDING, isOnboarding)
            }

            return intent.apply {
                putExtras(extras)
            }
        }
    }
}

/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.listener

import android.content.Context
import android.content.ContextWrapper
import android.view.Gravity
import android.view.View
import androidx.appcompat.widget.PopupMenu
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.models.Game

class GamePopupMenuCallback(
    val context: Context,
    private val gameClickListener: GameClickListener
) {
    private val TAG = makeLogTag("GamePopupMenuCallback")

    fun showPopupMenu(anchorView: View, game: Game) {
        val context = ContextWrapper(context)
        context.setTheme(R.style.AppTheme_PopupMenu)

        val popup = PopupMenu(context, anchorView, Gravity.NO_GRAVITY)
        val menu = popup.menu
        popup.menuInflater.inflate(R.menu.popup_game_card, menu)

        menu.findItem(R.id.action_mute).isChecked = game.isMuted
        menu.findItem(R.id.action_delay).isEnabled = !game.isMuted

        if (game.hasDelay()) {
            menu.findItem(R.id.action_delay).title =
                context.getString(R.string.action_delay_value, game.delay)
        }

        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.action_delay -> gameClickListener.onDelayMenuItemClick(game)
                R.id.action_mute -> gameClickListener.onMuteMenuItemClick(context, game)
            }

            return@setOnMenuItemClickListener false
        }

        popup.show()
    }
}

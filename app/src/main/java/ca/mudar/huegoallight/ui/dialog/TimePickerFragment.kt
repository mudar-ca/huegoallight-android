/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.dialog

import android.app.Dialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.widget.TimePicker
import androidx.fragment.app.DialogFragment
import ca.mudar.huegoallight.common.Const.BundleKeys
import ca.mudar.huegoallight.common.Const.UNKNOWN_VALUE
import ca.mudar.huegoallight.models.TimePeriodField
import ca.mudar.huegoallight.prefs.UserPrefs
import ca.mudar.huegoallight.utils.CompatUtils
import ca.mudar.huegoallight.utils.TimeUtils
import java.util.Calendar

class TimePickerFragment : DialogFragment(),
        TimePickerDialog.OnTimeSetListener {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val hourOfDay = arguments?.getInt(BundleKeys.HOUR_OF_DAY) ?: 0
        val minutes = arguments?.getInt(BundleKeys.MINUTES) ?: 0

        return CompatUtils.getTimePickerDialog(activity,
                this,
                hourOfDay,
                minutes,
                false)
                .apply {
                    setTitle(null)
                }
    }

    /**
     * Implements TimePickerDialog.OnTimeSetListener
     */
    override fun onTimeSet(view: TimePicker, hourOfDay: Int, minute: Int) {
        val minutesOfDay = TimeUtils.getMinutesOfDay(hourOfDay, minute)

        when (arguments?.getInt(BundleKeys.TIME_PERIOD_FIELD, UNKNOWN_VALUE)) {
            TimePeriodField.Start.value -> UserPrefs.quietHoursStart = minutesOfDay
            TimePeriodField.End.value -> UserPrefs.quietHoursEnd = minutesOfDay
        }
    }

    companion object {
        fun newInstance(field: TimePeriodField, calendar: Calendar): TimePickerFragment {
            val args = Bundle().apply {
                putInt(BundleKeys.TIME_PERIOD_FIELD, field.value)
                putInt(BundleKeys.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY))
                putInt(BundleKeys.MINUTES, calendar.get(Calendar.MINUTE))
            }

            return TimePickerFragment().apply {
                arguments = args
            }
        }
    }
}

/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.dialog

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.Const.BundleKeys
import ca.mudar.huegoallight.common.Const.MIME_TYPE_PLAIN_TEXT
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.databinding.FragmentWebhoolLoggerBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class WebhookLoggerFragment : BottomSheetDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentWebhoolLoggerBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_webhool_logger, container, false)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.message = arguments?.getString(BundleKeys.MESSAGE)

        setupToolbar(binding.toolbar)

        return binding.root
    }

    private fun setupToolbar(toolbar: Toolbar) {
        toolbar.inflateMenu(R.menu.menu_webhook_logger)
        toolbar.setOnMenuItemClickListener { menuItem ->
            if (menuItem.itemId == R.id.action_share) {
                shareLoggerContent()
                return@setOnMenuItemClickListener true
            }
            false
        }
    }

    private fun shareLoggerContent() {
        val extras = Bundle().apply {
            putString(Intent.EXTRA_SUBJECT, resources.getString(R.string.share_logger_title))
            putString(Intent.EXTRA_TEXT, arguments?.getString(BundleKeys.MESSAGE))
        }
        val sendIntent = Intent().apply {
            putExtras(extras)
            action = Intent.ACTION_SEND
            type = MIME_TYPE_PLAIN_TEXT
        }
        startActivity(Intent.createChooser(sendIntent, null))
    }

    companion object {
        private val TAG = makeLogTag("WebhookLoggerFragment")

        fun newInstance(message: String): WebhookLoggerFragment {
            val args = Bundle().apply {
                putString(BundleKeys.MESSAGE, message)
            }

            return WebhookLoggerFragment().apply {
                arguments = args
            }
        }
    }
}

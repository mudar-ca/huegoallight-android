/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.binding

import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.databinding.BindingAdapter
import ca.mudar.huegoallight.BuildConfig
import ca.mudar.huegoallight.R
import java.util.Locale

@BindingAdapter("text")
fun setTextViewText(textView: TextView, @StringRes stringRes: Int?) {
    stringRes?.let { res ->
        when {
            res > 0 -> textView.setText(res)
            else -> textView.text = null
        }
    }
}

@BindingAdapter("drawableTop")
fun setTextViewSource(textView: TextView, @DrawableRes drawableRes: Int?) {
    drawableRes?.let { res ->
        textView.setCompoundDrawablesRelativeWithIntrinsicBounds(0, res, 0, 0)
    }
}

/**
 * Set the team name
 */
@BindingAdapter("appVersionText")
fun setAppVersion(textView: TextView, versionText: String?) {
    versionText ?: return

    textView.text = versionText.format(Locale.ROOT, BuildConfig.VERSION_NAME)
}

/**
 * Set the bridge name
 */
@BindingAdapter("hueBridgeName")
fun setHueBridgeName(textView: TextView, name: String?) {
    name ?: return

    textView.text = textView.resources.getString(R.string.prefs_hue_bridge_name, name)
}

/**
 * Set the bridge IP address
 */
@BindingAdapter("hueBridgeIpAddress")
fun setBridgeIpAddress(textView: TextView, ipAddress: String?) {
    ipAddress ?: return

    textView.text = textView.resources.getString(R.string.prefs_hue_bridge_ip_address, ipAddress)
}

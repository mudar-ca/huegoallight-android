/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.widget

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import androidx.annotation.DrawableRes
import androidx.preference.CheckBoxPreference
import ca.mudar.huegoallight.R

class ScaledCheckBoxPreference
@JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = TypedValueUtils.getAttrOrFallback(
        context,
        androidx.preference.R.attr.checkBoxPreferenceStyle,
        android.R.attr.checkBoxPreferenceStyle
    ),
    defStyleRes: Int = 0
) : CheckBoxPreference(context, attrs, defStyleAttr, defStyleRes) {

    init {

        attrs?.let {
            val typedArray = context.obtainStyledAttributes(
                attrs,
                R.styleable.ScaledCheckBoxPreference,
                defStyleAttr,
                defStyleRes
            )

            val defaultSize = context.resources.getDimensionPixelSize(R.dimen.prefs_icon_size)
            val size = typedArray.getDimensionPixelSize(
                R.styleable.ScaledCheckBoxPreference_scaledCbPrefIconSize,
                defaultSize
            )
            val iconResId = typedArray.getResourceId(
                R.styleable.ScaledCheckBoxPreference_scaledCbPrefIcon,
                0
            )

            typedArray.recycle()

            if (iconResId != 0) {
                icon = getScaledDrawable(iconResId, size, size)
            }
        }
    }

    private fun getScaledDrawable(
        @DrawableRes iconResId: Int,
        newWidth: Int,
        newHeight: Int
    ): Drawable {
        val options = BitmapFactory.Options().apply {
            inScaled = false
        }
        val bitmap = BitmapFactory.decodeResource(context.resources, iconResId, options)

        // Calculate the scale
        val width = bitmap.width
        val height = bitmap.height
        val scaleWidth = newWidth.toFloat() / width
        val scaleHeight = newHeight.toFloat() / height

        // Create a resize matrix
        val matrix = Matrix().apply {
            postScale(scaleWidth, scaleHeight)
        }

        // Get the new Drawable
        val bitmapDrawable = BitmapDrawable(
            context.resources,
            Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true)
        )
        // Recycle the Bitmap
        if (bitmap.isRecycled.not()) {
            bitmap.recycle()
        }

        // Return scaled Drawable
        return bitmapDrawable
    }
}

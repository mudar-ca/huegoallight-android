/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.activity

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatActivity
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.Const.FragmentTags
import ca.mudar.huegoallight.common.Const.PrefsNames
import ca.mudar.huegoallight.common.Const.PrefsValues
import ca.mudar.huegoallight.common.Const.USER_PREFS_NAME
import ca.mudar.huegoallight.databinding.ActivityMasterSwitchSettingsBinding
import ca.mudar.huegoallight.ui.fragment.SettingsQuietHoursFragment

class SettingsQuietHoursActivity : AppCompatActivity(),
    SharedPreferences.OnSharedPreferenceChangeListener, CompoundButton.OnCheckedChangeListener {

    private lateinit var binding: ActivityMasterSwitchSettingsBinding
    private lateinit var sharedPrefs: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMasterSwitchSettingsBinding.inflate(layoutInflater)

        setContentView(binding.root)

        // Prefs listener
        sharedPrefs = getSharedPreferences(USER_PREFS_NAME, Context.MODE_PRIVATE)
        sharedPrefs.registerOnSharedPreferenceChangeListener(this)

        setupSwitchPreference()

        if (savedInstanceState == null) {
            val fragment =
                supportFragmentManager.findFragmentByTag(FragmentTags.SETTINGS_QUIET_HOURS)
                    ?: SettingsQuietHoursFragment.newInstance()

            supportFragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment, FragmentTags.SETTINGS_QUIET_HOURS)
                .commit()
        }
    }

    override fun onDestroy() {
        sharedPrefs.unregisterOnSharedPreferenceChangeListener(this)

        super.onDestroy()
    }

    /**
     * Implements SharedPreferences.OnSharedPreferenceChangeListener
     */
    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        if (PrefsNames.QUIET_HOURS_ENABLED == key) {
            binding.masterSwitch.isChecked = sharedPrefs.getBoolean(
                PrefsNames.QUIET_HOURS_ENABLED,
                PrefsValues.DEFAULT_QUIET_HOURS_ENABLED
            )
        }
    }

    /**
     * Implements CompoundButton.OnCheckedChangeListener
     */
    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        toggleSwitchText(isChecked)

        sharedPrefs.edit()
            .putBoolean(PrefsNames.QUIET_HOURS_ENABLED, isChecked)
            .apply()
    }

    private fun setupSwitchPreference() {
        val isEnabled = sharedPrefs.getBoolean(
            PrefsNames.QUIET_HOURS_ENABLED,
            PrefsValues.DEFAULT_QUIET_HOURS_ENABLED
        )
        binding.masterSwitch.isChecked = isEnabled
        toggleSwitchText(isEnabled)

        binding.masterSwitch.setOnCheckedChangeListener(this)
    }

    private fun toggleSwitchText(isChecked: Boolean) {
        binding.masterSwitch.setText(
            when {
                isChecked -> R.string.prefs_summary_quiet_hours_on
                else -> R.string.prefs_summary_quiet_hours_off
            }
        )
    }

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, SettingsQuietHoursActivity::class.java)
        }
    }
}

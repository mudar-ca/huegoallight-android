/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.widget

import android.content.res.Resources
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.LogUtils.makeLogTag

class GameItemDecoration(resources: Resources) : RecyclerView.ItemDecoration() {
    private val horizontalPadding: Int =
        resources.getDimensionPixelSize(R.dimen.activity_horizontal_margin)
    private val verticalPadding: Int =
        resources.getDimensionPixelSize(R.dimen.activity_vertical_margin)
    private val bottomPadding: Int =
        resources.getDimensionPixelSize(R.dimen.game_list_item_bottom_margin)

    private val maxWidth: Int = resources.getDimensionPixelSize(R.dimen.game_list_item_max_width)
    private val maxWidthTolerance: Int = horizontalPadding * 3

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        with(outRect) {
            if (parent.getChildAdapterPosition(view) == 0) {
                top = verticalPadding
            }
            bottom = bottomPadding

            computeHorizontalPadding(parent.width).let { padding ->
                left = padding
                right = padding
            }
        }
    }

    private fun computeHorizontalPadding(parentWidth: Int): Int {
        return when {
            parentWidth <= maxWidth + maxWidthTolerance -> horizontalPadding
            else -> (parentWidth - maxWidth) / 2
        }
    }

    companion object {
        private val TAG = makeLogTag("GameItemDecoration")
    }
}

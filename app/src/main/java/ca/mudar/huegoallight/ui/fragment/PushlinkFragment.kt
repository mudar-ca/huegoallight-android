/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.fragment

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.ContextWrapper
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.Const.HUE_WEBSITE
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.databinding.FragmentPushlinkBinding
import ca.mudar.huegoallight.fcm.FcmUtils
import ca.mudar.huegoallight.utils.isNightMode
import ca.mudar.huegoallight.viewmodel.PushlinkViewModel
import ca.mudar.huegoallight.viewmodel.PushlinkViewModel.Companion.PROGRESS_BAR_DURATION
import ca.mudar.huegoallight.viewmodel.PushlinkViewModel.Companion.PULSE_DURATION
import com.google.android.material.snackbar.Snackbar

class PushlinkFragment : Fragment() {

    private var binding: FragmentPushlinkBinding? = null
    private val viewModel by lazy { PushlinkViewModel.instanceOf(this) }

    private lateinit var progressBarAnim: ObjectAnimator
    private lateinit var bridgeColorAnim: ValueAnimator

    private val wrongClickObserver = Observer<Boolean> { showMessage ->
        if (showMessage) {
            view?.let {
                Snackbar.make(it, R.string.snackbar_pushlink_wrong_click, Snackbar.LENGTH_LONG)
                    .setAction(R.string.btn_help) {
                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(HUE_WEBSITE)))
                    }
                    .show()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pushlink, container, false)

        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.let {
            animateBridgeColor(it.illustration)
            animateProgressBar(it.progressBar)
        }

        viewModel.showWrongClickErrorMessage.observe(viewLifecycleOwner, wrongClickObserver)

        FcmUtils.sendAnalyticsPushlinkEvent(ContextWrapper(context))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
        progressBarAnim.cancel()
        bridgeColorAnim.cancel()
    }

    private fun animateProgressBar(progressBar: ProgressBar) {
        progressBarAnim = ObjectAnimator
            .ofInt(progressBar, "progress", viewModel.startValue, 0)
            .apply {
                duration = viewModel.animDuration
                interpolator = DecelerateInterpolator()
                addUpdateListener { animator ->
                    viewModel.progressValue = animator.animatedValue as Int
                }
                start()
            }
    }

    private fun animateBridgeColor(imageView: ImageView) {
        val rgb = when (activity?.isNightMode()) {
            true -> 255
            else -> 0
        }

        bridgeColorAnim = ValueAnimator().apply {
            setIntValues(0, 192)
            duration = PULSE_DURATION
            repeatCount = (PROGRESS_BAR_DURATION / PULSE_DURATION).toInt() - 1
            repeatMode = ValueAnimator.REVERSE
            addUpdateListener { animator ->
                val alpha = animator.animatedValue as Int
                imageView.setColorFilter(Color.argb(alpha, rgb, rgb, rgb))
            }
            start()
        }
    }

    companion object {
        private val TAG = makeLogTag("PushlinkFragment")

        fun newInstance(): PushlinkFragment {
            return PushlinkFragment()
        }
    }
}

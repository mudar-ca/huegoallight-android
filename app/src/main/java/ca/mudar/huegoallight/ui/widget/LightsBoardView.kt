/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.widget

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.graphics.PorterDuff
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.models.HueLight
import ca.mudar.huegoallight.models.LightsColumn
import ca.mudar.huegoallight.ui.adapter.LightsDragItemAdapter
import com.woxthebox.draglistview.BoardView
import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt
import kotlin.math.max

class LightsBoardView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = android.R.attr.horizontalScrollViewStyle
) : BoardView(context, attrs, defStyleAttr) {

    private val itemDecoration = object : RecyclerView.ItemDecoration() {
        private val horizontalPadding: Int =
            resources.getDimensionPixelSize(R.dimen.activity_horizontal_margin)
        private val verticalPadding: Int =
            resources.getDimensionPixelSize(R.dimen.activity_vertical_margin)

        override fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            with(outRect) {
                if (parent.getChildAdapterPosition(view) == 0) {
                    top = verticalPadding
                }
                left = horizontalPadding
                right = horizontalPadding
                bottom = verticalPadding
            }
        }
    }

    override fun onFinishInflate() {
        super.onFinishInflate()

        // BoardView config
        setSnapToColumnWhenDragging(true)
        isDragEnabled = true
        setSnapDragItemToTouch(true)
        setSnapToColumnsWhenScrolling(false)
        setColumnWidth(getBestColumnWidth(resources))
        setCustomDragItem(LightDragView(context, R.layout.item_lights_grid))

        setupColumns()
    }

    fun setColumnLights(column: LightsColumn, lights: List<HueLight>) {
        (getRecyclerView(column)?.adapter as? LightsDragItemAdapter)?.itemList = lights
    }

    fun showDiscovery(activity: Activity): Boolean {
        return getRecyclerView(LightsColumn.PrimaryColor)?.layoutManager?.getChildAt(0)
            ?.let { firstChild ->
                val target = firstChild.findViewById<View>(R.id.drag_handle)
                MaterialTapTargetPrompt.Builder(activity)
                    .setTarget(target)
                    .setPrimaryText(R.string.lights_discovery_title)
                    .setSecondaryText(R.string.lights_discovery_summary)
                    .setBackgroundColour(ContextCompat.getColor(activity, R.color.color_primary))
                    .setFocalColour(ContextCompat.getColor(activity, R.color.color_background))
                    .setIcon(R.drawable.ic_drag_handle)
                    .setIconDrawableColourFilter(
                        ContextCompat.getColor(
                            activity,
                            R.color.color_highlight
                        )
                    )
                    .setIconDrawableTintMode(PorterDuff.Mode.SRC_ATOP)
                    .show()

                true
            } ?: false
    }

    /**
     * Scroll to the column with most lights, ignoring the unused-lights column.
     */
    fun scrollToInitialColumn() {
        val columns =
            arrayOf(LightsColumn.PrimaryColor, LightsColumn.SecondaryColor, LightsColumn.RedLight)

        var maxLights = 0
        var selectedColumn = LightsColumn.UnusedLights
        columns.forEach { column ->
            getColumnLights(column).let { lights ->
                if (lights.size > maxLights) {
                    maxLights = lights.size
                    selectedColumn = column
                }
            }
        }
        if (maxLights > 0) {
            // No need to scroll if all 3 columns are empty
            scrollToColumn(selectedColumn.value, false)
        }
    }

    private fun setupColumns() {
        insertLightsColumns(LightsColumn.UnusedLights)
        insertLightsColumns(LightsColumn.PrimaryColor)
        insertLightsColumns(LightsColumn.SecondaryColor)
        insertLightsColumns(LightsColumn.RedLight)
    }

    private fun insertLightsColumns(column: LightsColumn) {
        // Call insertColumn() to add lights to the board and get the created recyclerView
        insertColumn(
            LightsDragItemAdapter(listOf()),
            column.value,
            createHeaderView(column),
            null,
            false
        ).let { recycler ->
            styleRecyclerView(column, recycler)
        }
    }

    private fun getRecyclerView(column: LightsColumn): RecyclerView? =
        getRecyclerView(column.value)

    private fun getColumnLights(column: LightsColumn): List<HueLight> =
        (getRecyclerView(column)?.adapter as? LightsDragItemAdapter)?.itemList ?: listOf()

    private fun styleRecyclerView(column: LightsColumn, recyclerView: RecyclerView) {
        if (column != LightsColumn.UnusedLights) {
            // Set left dotted border to all but first column
            recyclerView.background =
                ContextCompat.getDrawable(context, R.drawable.column_left_border)
        }

        recyclerView.addItemDecoration(itemDecoration)
    }

    private fun createHeaderView(column: LightsColumn): View {
        val header = View.inflate(context, R.layout.header_lights_grid, null)
        val vTitle = header.findViewById<View>(R.id.title) as TextView

        when (column) {
            LightsColumn.UnusedLights -> vTitle.setText(R.string.lights_column_unused)
            LightsColumn.PrimaryColor -> vTitle.setText(R.string.lights_column_primary)
            LightsColumn.SecondaryColor -> vTitle.setText(R.string.lights_column_secondary)
            LightsColumn.RedLight -> {
                vTitle.setText(R.string.lights_column_red)
                vTitle.setTextColor(ContextCompat.getColor(context, R.color.color_highlight))
            }
        }
        return header
    }

    private fun getBestColumnWidth(res: Resources): Int {
        val defaultWidth = res.getDimensionPixelSize(R.dimen.board_column_width)
        val optimalWidth = res.displayMetrics.widthPixels / LightsColumn.COUNT

        return max(defaultWidth, optimalWidth)
    }
}

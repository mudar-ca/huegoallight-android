/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.library.baseAdapters.BR
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.databinding.ItemCurrentGameBinding
import ca.mudar.huegoallight.models.Game
import ca.mudar.huegoallight.ui.listener.GameClickListener
import ca.mudar.huegoallight.ui.listener.GamePopupMenuCallback
import ca.mudar.huegoallight.ui.widget.BindingViewHolder
import ca.mudar.huegoallight.viewmodel.CurrentGameVM

class CurrentGamesAdapter(
    context: Context,
    private val listener: GameClickListener
) : UpcomingGamesAdapter(context) {

    private val TAG = makeLogTag("CurrentGamesAdapter")

    private val popupCallback = GamePopupMenuCallback(context, listener)
    private var currentTime: Long = System.currentTimeMillis()

    override fun getItemViewType(position: Int): Int {
        val startsAt = dataset?.elementAtOrNull(position)?.startsAt
                ?: return TYPE_UPCOMING

        return when {
            startsAt < currentTime -> TYPE_CURRENT
            else -> TYPE_UPCOMING
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingViewHolder {
        return when (TYPE_CURRENT) {
            viewType -> {
                val binding = DataBindingUtil.inflate<ItemCurrentGameBinding>(
                        LayoutInflater.from(parent.context),
                        R.layout.item_current_game,
                        parent,
                        false
                )
                BindingViewHolder(binding)
            }
            else -> super.onCreateViewHolder(parent, viewType)
        }
    }

    override fun onBindViewHolder(holder: BindingViewHolder, position: Int) {
        dataset?.elementAtOrNull(position)?.let {
            holder.binding.setVariable(BR.viewModel, CurrentGameVM(it))
            holder.binding.setVariable(BR.listener, listener)
            holder.binding.setVariable(BR.popupCallback, popupCallback)
        }
    }

    override fun swapDataset(games: List<Game>?) {
        currentTime = System.currentTimeMillis()

        super.swapDataset(games)
    }

    companion object {
        private const val TYPE_UPCOMING = 0
        private const val TYPE_CURRENT = 1
    }
}

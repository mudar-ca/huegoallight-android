/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.listener

import com.woxthebox.draglistview.BoardView

abstract class SimpleBoardViewListener : BoardView.BoardListener {
    override fun onColumnDragChangedPosition(oldPosition: Int, newPosition: Int) {
    }

    override fun onItemChangedColumn(oldColumn: Int, newColumn: Int) {
    }

    override fun onColumnDragStarted(position: Int) {
    }

    override fun onFocusedColumnChanged(oldColumn: Int, newColumn: Int) {
    }

    override fun onItemDragEnded(fromColumn: Int, fromRow: Int, toColumn: Int, toRow: Int) {
    }

    override fun onItemChangedPosition(oldColumn: Int, oldRow: Int, newColumn: Int, newRow: Int) {
    }

    override fun onColumnDragEnded(position: Int) {
    }

    // TODO blink light bulb once
    override fun onItemDragStarted(column: Int, row: Int) {
    }
}

/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.activity

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.Const.FragmentTags
import ca.mudar.huegoallight.common.Const.PrefsNames
import ca.mudar.huegoallight.common.Const.USER_PREFS_NAME
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.databinding.ActivityWebhookSettingsBinding
import ca.mudar.huegoallight.network.LoggerCallback
import ca.mudar.huegoallight.ui.dialog.WebhookLoggerFragment
import ca.mudar.huegoallight.ui.fragment.SettingsWebhookFragment
import ca.mudar.huegoallight.viewmodel.WebhookViewModel
import com.google.android.material.snackbar.Snackbar
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Response

class SettingsWebhookActivity : AppCompatActivity() {

    private lateinit var binding: ActivityWebhookSettingsBinding
    private val viewModel by lazy { WebhookViewModel.instanceOf(this) }
    private lateinit var sharedPrefs: SharedPreferences

    private val triggerWebhookClickListener = View.OnClickListener {
        viewModel.triggerWebhook(responseCallback)
    }

    private val responseCallback = object : LoggerCallback<ResponseBody> {
        private var onScreenMessage = StringBuilder()

        override val logger = HttpLoggingInterceptor.Logger { message ->
            onScreenMessage.appendLine(message)
        }

        override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
            if (response.isSuccessful) {
                Snackbar.make(
                    binding.mainContent,
                    R.string.snackbar_webhook_trigger_success,
                    Snackbar.LENGTH_LONG
                )
                    .setAction(R.string.btn_logger) { showWebhookLogger() }
                    .show()
            } else {
                showWebhookLogger()
            }
            viewModel.onWebhookResponse()
        }

        override fun onFailure(call: Call<ResponseBody>, throwable: Throwable) {
            showWebhookLogger()
            viewModel.onWebhookResponse()
        }

        private fun showWebhookLogger() {
            val message = onScreenMessage.toString()
            onScreenMessage.clear()

            WebhookLoggerFragment.newInstance(message)
                .show(supportFragmentManager, FragmentTags.WEBHOOK_LOGGER)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_webhook_settings)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.onClickListener = triggerWebhookClickListener

        sharedPrefs = getSharedPreferences(USER_PREFS_NAME, Context.MODE_PRIVATE).apply {
            registerOnSharedPreferenceChangeListener(viewModel.prefsChangeListener)
        }

        setupFabVisibility()

        if (savedInstanceState == null) {
            val fragment = SettingsWebhookFragment.newInstance()

            supportFragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment, FragmentTags.SETTINGS_WEBHOOK)
                .commit()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        sharedPrefs.unregisterOnSharedPreferenceChangeListener(viewModel.prefsChangeListener)
    }

    private fun setupFabVisibility() {
        val isEnabled = sharedPrefs.getBoolean(PrefsNames.WEBHOOK_ENABLED, false)
        viewModel.isFabVisible.value = isEnabled
    }

    companion object {
        private val TAG = makeLogTag("WebhookActivity")

        fun newIntent(context: Context): Intent {
            return Intent(context, SettingsWebhookActivity::class.java)
        }
    }
}

/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.widget

import android.animation.ObjectAnimator
import android.content.Context
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.widget.FrameLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import ca.mudar.huegoallight.R
import com.woxthebox.draglistview.DragItem

class LightDragView(context: Context, layoutId: Int) : DragItem(context, layoutId) {

    private val elevation: Float = context.resources.getDimension(R.dimen.board_card_elevation)
    private val maxElevation: Float =
        context.resources.getDimension(R.dimen.board_card_max_elevation)

    override fun onBindDragView(clickedView: View, dragView: View) {
        val title = clickedView.findViewById<TextView>(R.id.title).text
        val statusVisibility = clickedView.findViewById<View>(R.id.status).visibility

        dragView.findViewById<TextView>(R.id.title).text = title
        dragView.findViewById<View>(R.id.status).visibility = statusVisibility

        val dragCard = dragView.findViewById<CardView>(R.id.cardview)

        dragCard.cardElevation = elevation
        dragCard.maxCardElevation = maxElevation
    }

    override fun onMeasureDragView(clickedView: View, dragView: View) {
        val dragCard = dragView.findViewById<CardView>(R.id.cardview)
        val clickedCard = clickedView.findViewById<CardView>(R.id.cardview)

        val widthDiff = dragCard.paddingLeft - clickedCard.paddingLeft +
                dragCard.paddingRight - clickedCard.paddingRight
        val heightDiff = dragCard.paddingTop - clickedCard.paddingTop +
                dragCard.paddingBottom - clickedCard.paddingBottom
        val width = clickedView.measuredWidth + widthDiff
        val height = clickedView.measuredHeight + heightDiff
        dragView.layoutParams = FrameLayout.LayoutParams(width, height)

        val widthSpec = View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY)
        val heightSpec = View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY)
        dragView.measure(widthSpec, heightSpec)
    }

    override fun onStartDragAnimation(dragView: View) {
        val dragCard = dragView.findViewById<CardView>(R.id.cardview)
        val anim =
            ObjectAnimator.ofFloat(dragCard, CARD_ELEVATION, dragCard.cardElevation, maxElevation)
        anim.interpolator = DecelerateInterpolator()
        anim.duration = ANIMATION_DURATION.toLong()
        anim.start()
    }

    override fun onEndDragAnimation(dragView: View) {
        val dragCard = dragView.findViewById<CardView>(R.id.cardview)
        val anim =
            ObjectAnimator.ofFloat(dragCard, CARD_ELEVATION, dragCard.cardElevation, elevation)
        anim.interpolator = DecelerateInterpolator()
        anim.duration = ANIMATION_DURATION.toLong()
        anim.start()
    }

    companion object {
        private const val CARD_ELEVATION = "CardElevation"
    }
}

/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.activity

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.Const.LocalAssets
import ca.mudar.huegoallight.databinding.ActivityEulaBinding
import ca.mudar.huegoallight.utils.isNightMode

class EulaActivity : AppCompatActivity() {

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityEulaBinding = ActivityEulaBinding.inflate(layoutInflater)
        setContentView(binding.root)

        loadWebView(binding.webview)
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun loadWebView(webView: WebView) {
        // Set basic style
        webView.setBackgroundColor(ContextCompat.getColor(this, R.color.color_background))
        webView.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY

        val isNightMode = isNightMode()
        if (isNightMode) {
            webView.settings.javaScriptEnabled = true
        }

        // Open links in external browser
        webView.webViewClient = object : WebViewClient() {

            override fun onPageFinished(view: WebView?, url: String?) {
                if (isNightMode) {
                    webView.evaluateJavascript(
                        "(function(){ document.body.className='dark'; })();",
                        null
                    )
                }
            }

            @Suppress("OverridingDeprecatedMember")
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                url?.let {
                    loadUrl(Uri.parse(it))
                }
                return true
            }

            @TargetApi(Build.VERSION_CODES.N)
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                request?.url?.let {
                    loadUrl(it)
                }
                return true
            }

            private fun loadUrl(uri: Uri) {
                startActivity(Intent(Intent.ACTION_VIEW, uri))
            }
        }

        // Load HTML content from assets
        webView.loadUrl(ASSETS_URI + LocalAssets.LICENSE)
    }

    companion object {
        private const val ASSETS_URI = "file:///android_asset/"

        fun newIntent(context: Context): Intent {
            return Intent(context, EulaActivity::class.java)
        }
    }
}

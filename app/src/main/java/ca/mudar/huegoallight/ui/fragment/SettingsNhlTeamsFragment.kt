/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.fragment

import android.content.Context
import android.content.ContextWrapper
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.UiThread
import androidx.lifecycle.Observer
import androidx.preference.CheckBoxPreference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.TwoStatePreference
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.Const.PrefsNames
import ca.mudar.huegoallight.common.Const.USER_PREFS_NAME
import ca.mudar.huegoallight.fcm.FcmRepo
import ca.mudar.huegoallight.fcm.FcmUtils
import ca.mudar.huegoallight.hockey.NhlTeam
import ca.mudar.huegoallight.models.FcmEvent
import ca.mudar.huegoallight.models.FcmTopicSubscriptionError
import ca.mudar.huegoallight.prefs.UserPrefs
import ca.mudar.huegoallight.utils.ConnectionUtils
import com.google.android.material.snackbar.Snackbar
import java.util.Locale

class SettingsNhlTeamsFragment : PreferenceFragmentCompat(),
    SharedPreferences.OnSharedPreferenceChangeListener {

    private lateinit var sharedPrefs: SharedPreferences

    private val fcmEventListener = Observer<FcmEvent> { fcmEvent ->
        if (fcmEvent is FcmTopicSubscriptionError) {
            onTeamFollowError(fcmEvent.topic)
        }
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        val pm = this.preferenceManager
            .apply {
                sharedPreferencesName = USER_PREFS_NAME
                sharedPreferencesMode = Context.MODE_PRIVATE
            }

        addPreferencesFromResource(R.xml.prefs_nhl_teams)

        sharedPrefs = pm.sharedPreferences
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        FcmRepo.event.observe(viewLifecycleOwner, fcmEventListener)

        return view
    }

    override fun onResume() {
        super.onResume()

        sharedPrefs.registerOnSharedPreferenceChangeListener(this)
    }

    override fun onPause() {
        super.onPause()

        sharedPrefs.unregisterOnSharedPreferenceChangeListener(this)
    }

    /**
     * Implements OnSharedPreferenceChangeListener
     */
    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        key ?: return
        val context = this.context
            ?: return

        if (ConnectionUtils.checkConnection(view).not()) {
            findPreference<CheckBoxPreference>(key)?.let {
                undoPreferenceChange(it)
            }
            return
        }
        if (key.startsWith(PrefsNames.NHL_TEAM_PREFIX).not()) {
            return
        }

        val team = key.replace(PrefsNames.NHL_TEAM_PREFIX, "").toUpperCase(Locale.getDefault())
        val isChecked = findPreference<TwoStatePreference>(key)?.isChecked
            ?: false

        UserPrefs.toggleFollowedTeam(team, isChecked)
        FcmRepo.toggleTopicSubscription(team, isChecked)
        FcmUtils.sendAnalyticsTeamFollowedEvent(ContextWrapper(context), team, isChecked)
    }

    private fun onTeamFollowError(team: String) {
        val prefKey = PrefsNames.NHL_TEAM_PREFIX + team.toLowerCase(Locale.getDefault())
        val teamName = resources.getString(NhlTeam(team).teamShortName)

        activity?.runOnUiThread {
            try {
                val checkBox = findPreference<TwoStatePreference>(prefKey)
                    ?: return@runOnUiThread
                val message = resources.getString(
                    when (checkBox.isChecked) {
                        true -> R.string.snackbar_fcm_topics_subscribe_error
                        false -> R.string.snackbar_fcm_topics_unsubscribe_error
                    }, teamName
                )

                undoPreferenceChange(checkBox)
                checkBox.isEnabled = false

                this@SettingsNhlTeamsFragment.view?.let {
                    Snackbar.make(it, message, Snackbar.LENGTH_LONG)
                        .show()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    /**
     * Toggle prefs listener before and after reverting the value
     */
    @UiThread
    private fun undoPreferenceChange(checkBox: TwoStatePreference) {
        sharedPrefs.unregisterOnSharedPreferenceChangeListener(this@SettingsNhlTeamsFragment)
        checkBox.isChecked = checkBox.isChecked.not()
        sharedPrefs.registerOnSharedPreferenceChangeListener(this@SettingsNhlTeamsFragment)
    }

    companion object {
        fun newInstance(): SettingsNhlTeamsFragment {
            return SettingsNhlTeamsFragment()
        }
    }
}

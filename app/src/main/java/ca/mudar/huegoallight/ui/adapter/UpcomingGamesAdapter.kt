/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.databinding.ItemUpcomingGameBinding
import ca.mudar.huegoallight.models.Game
import ca.mudar.huegoallight.ui.widget.BindingViewHolder
import ca.mudar.huegoallight.viewmodel.UpcomingGameVM

open class UpcomingGamesAdapter(
    protected val context: Context
) : RecyclerView.Adapter<BindingViewHolder>() {

    private val TAG = makeLogTag("UpcomingGamesAdapter")

    protected var dataset: List<Game>? = null
        private set

    open fun swapDataset(games: List<Game>?) {
        dataset = games
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return dataset?.size ?: 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingViewHolder {
        val binding = DataBindingUtil.inflate<ItemUpcomingGameBinding>(
                LayoutInflater.from(parent.context),
                R.layout.item_upcoming_game,
                parent,
                false
        )
        return BindingViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BindingViewHolder, position: Int) {
        dataset?.elementAtOrNull(position)?.let {
            holder.binding.setVariable(BR.viewModel, UpcomingGameVM(it))
        }
    }
}

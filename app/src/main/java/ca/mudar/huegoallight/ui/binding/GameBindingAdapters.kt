/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.binding

import android.graphics.BitmapFactory
import android.graphics.PorterDuff
import android.text.format.DateUtils
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.palette.graphics.Palette
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.hockey.HockeyUtils
import ca.mudar.huegoallight.hockey.NhlTeam
import ca.mudar.huegoallight.models.Game

/**
 * Set the team logo
 */
@BindingAdapter("teamLogo")
fun setTeamLogo(imageView: ImageView, team: NhlTeam?) {
    team?.let {
        imageView.setImageResource(team.teamLogo)
    }
}

/**
 * Set the team name
 */
@BindingAdapter("teamName")
fun setTeamName(textView: TextView, team: NhlTeam?) {
    team?.let {
        textView.setText(team.teamName)
    }
}

/**
 * Set the team score
 */
@BindingAdapter("teamScore")
fun setTeamScore(textView: TextView, team: NhlTeam?) {
    team ?: return

    textView.text = team.goals.toString()
}

/**
 * Set the game teams label.
 * Ex: Montreal Canadiens vs Toronto Maple Leafs
 */
@BindingAdapter("awayTeamName", "homeTeamName", requireAll = true)
fun setGameTeams(textView: TextView, awayTeam: NhlTeam?, homeTeam: NhlTeam?) {
    awayTeam ?: return
    homeTeam ?: return

    val resources = textView.resources
    textView.text = resources.getString(
        R.string.game_team_vs_team,
        resources.getString(awayTeam.teamName),
        resources.getString(homeTeam.teamName)
    )
}

/**
 * Set the game start date and (when available) time
 */
@BindingAdapter("gameStartsAt")
fun setGameDate(textView: TextView, startsAt: Long?) {
    startsAt ?: return

    var formatFlags = DateUtils.FORMAT_ABBREV_RELATIVE or
            DateUtils.FORMAT_SHOW_DATE or
            DateUtils.FORMAT_SHOW_WEEKDAY
    if (!HockeyUtils.isStartTimeTBD(startsAt)) {
        // TBD game time is hidden
        formatFlags = formatFlags or DateUtils.FORMAT_SHOW_TIME
    }

    textView.text = DateUtils.formatDateTime(textView.context, startsAt, formatFlags)
}

/**
 * Set the game background color, based on home team logo palette.
 * @param homeTeam The NhlTeam whose logo will be used to define colors.
 * @param isUpcomingGame Boolean param to determine which child views need to be colored.
 */
@BindingAdapter("gamePalette", "isUpcomingGame", requireAll = true)
fun setGameFooterPalette(
    viewGroup: ViewGroup,
    homeTeam: NhlTeam?,
    isUpcomingGame: Boolean? = false
) {
    homeTeam ?: return

    with(viewGroup) {
        val teamLogo = BitmapFactory.decodeResource(context.resources, homeTeam.teamLogo)
            ?: return

        Palette.from(teamLogo).generate {
            try {
                val swatch = it?.dominantSwatch
                    ?: return@generate

                setBackgroundColor(swatch.rgb)
                val textColor = swatch.bodyTextColor
                when (isUpcomingGame) {
                    true -> {
                        viewGroup.findViewById<TextView>(R.id.team_vs_team)?.setTextColor(textColor)
                        viewGroup.findViewById<TextView>(R.id.date)?.setTextColor(textColor)
                    }
                    else -> {
                        viewGroup.findViewById<TextView>(R.id.away_score)?.setTextColor(textColor)
                        viewGroup.findViewById<TextView>(R.id.away_team)?.setTextColor(textColor)
                        viewGroup.findViewById<TextView>(R.id.home_score)?.setTextColor(textColor)
                        viewGroup.findViewById<TextView>(R.id.home_team)?.setTextColor(textColor)
                    }
                }
            } catch (e: Throwable) {
                e.printStackTrace()
            }
        }
    }
}

/**
 * Toggle the overflow menu icon colors, highlighting it for muted or delayed games
 */
@BindingAdapter("gameSettings")
fun setGameOverflowColors(imageView: ImageView, game: Game?) {
    game ?: return

    @ColorRes val color = when {
        game.isMuted || game.hasDelay() -> R.color.overflow_highlight
        else -> R.color.overflow_default
    }
    imageView.setColorFilter(
        ContextCompat.getColor(imageView.context, color),
        PorterDuff.Mode.SRC_ATOP
    )
}

/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.activity

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.annotation.ColorInt
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.vectordrawable.graphics.drawable.Animatable2Compat
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.Const.ALTERNATE_RGB_COLOR
import ca.mudar.huegoallight.common.Const.BundleKeys
import ca.mudar.huegoallight.common.Const.DEFAULT_RGB_COLOR
import ca.mudar.huegoallight.common.Const.MainTabs
import ca.mudar.huegoallight.common.Const.RequestCodes
import ca.mudar.huegoallight.common.Const.UNKNOWN_VALUE
import ca.mudar.huegoallight.common.LogUtils.LOGE
import ca.mudar.huegoallight.common.LogUtils.LOGV
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.databinding.ActivityMainBinding
import ca.mudar.huegoallight.fcm.FcmUtils
import ca.mudar.huegoallight.hue.HueBlinkController
import ca.mudar.huegoallight.hue.HueBridgeManager
import ca.mudar.huegoallight.models.HueBridgeState
import ca.mudar.huegoallight.models.HueSdkError
import ca.mudar.huegoallight.models.LightSelectionError
import ca.mudar.huegoallight.prefs.UserPrefs
import ca.mudar.huegoallight.service.ScheduleUpdateService
import ca.mudar.huegoallight.ui.adapter.MainTabsAdapter
import ca.mudar.huegoallight.utils.ConnectionUtils
import ca.mudar.huegoallight.utils.TimeUtils
import ca.mudar.huegoallight.utils.WebhookHelper
import ca.mudar.huegoallight.viewmodel.MainViewModel
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val viewModel by lazy { MainViewModel.instanceOf(this) }
    private var hueOptionsItem: MenuItem? = null
    private var hueOptionsItemAnim: AnimatedVectorDrawableCompat? = null

    @Suppress("NON_EXHAUSTIVE_WHEN")
    private val bridgeStateObserver = Observer<HueBridgeState> { state ->
        LOGV(TAG, "Observer, state: $state")

        when (state) {
//            HueBridgeState.BridgeDiscoveryRunning -> {
//            }
//            HueBridgeState.BridgeDiscoveryResults -> {
//            }
//            HueBridgeState.Connecting -> {
//            }
            HueBridgeState.Pushlinking -> {
                onBridgePushlinking()
            }
            HueBridgeState.Connected -> {
                onBridgeConnectedAndInitialized()
            }
            HueBridgeState.Idle -> {
                onBridgeIdle()
            }
        }
    }

    private val bridgeErrorObserver = Observer<HueSdkError?> { hueError ->
        hueError?.let {
            LOGE(TAG, "bridgeErrorObserver, hueError: $hueError")
            onBridgeError(hueError)
        }
    }

    private val lightsBlinkingObserver = Observer<Boolean> { isBlinking ->
        viewModel.isFabEnabled.value = isBlinking.not()
    }

    private val manualBlinkClickListener = View.OnClickListener {
        viewModel.isFabEnabled.value = false
        HueBlinkController.startBlinking(
            blinkHues = MANUAL_BLINK_HUES
        )
        WebhookHelper.callWebhookIfAvailable()

        FcmUtils.sendAnalyticsManualBlinkEvent(ContextWrapper(this))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (showOnboardingIfNecessary(intent)) {
            finish()
            // Avoid duplicate calls to hueSdk
            return
        }

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.viewModel = viewModel
        binding.onClickListener = manualBlinkClickListener
        binding.lifecycleOwner = this

        setSupportActionBar(binding.toolbar)

        setupViewPager()
        setupHueBridgeObservers()

        // Update season schedule, if necessary
        ScheduleUpdateService.enqueueWork(ContextWrapper(this))

        checkQuietMode()
    }

    override fun onResume() {
        super.onResume()

        if (FcmUtils.checkPlayServices(this)) {
            // Avoid displaying the PlayServices dialog followed by the PushLink activity
            if (HueBridgeManager.startBridgeAutoConnection().not()) {
                startActivityForResult(HueSetupActivity.newIntent(this), RequestCodes.HUE_SETUP)
            }
        }

        ConnectionUtils.checkConnection(binding.mainContent)
    }

    override fun onPause() {
        super.onPause()
        toggleHueOptionsItemAnim(false)
    }

    override fun onDestroy() {
        super.onDestroy()

        hueOptionsItemAnim?.clearAnimationCallbacks()
        hueOptionsItemAnim = null
        hueOptionsItem = null
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        val isConnected = HueBridgeManager.state.value == HueBridgeState.Connected

        // When Hue is unavailable, disable Settings and show HueSetup item
        menu.findItem(R.id.action_settings)?.isEnabled = isConnected
        hueOptionsItem = menu.findItem(R.id.action_hue_setup)?.apply {
            isVisible = isConnected.not()
        }
        if (isConnected.not()) {
            binding.toolbar.post { toggleHueOptionsItemAnim(true) }
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> {
                startActivity(SettingsActivity.newIntent(this))
                true
            }
            R.id.action_hue_setup -> {
                startActivityForResult(
                    HueSetupActivity.newIntent(context = this, autoConnect = false),
                    RequestCodes.HUE_SETUP
                )
                true
            }
            R.id.action_about -> {
                startActivity(AboutActivity.newIntent(this))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RequestCodes.HUE_SETUP) {
            if (resultCode != Activity.RESULT_OK) {
                // LOGE(TAG, "Push-link error");
                finish()
            }
        }
    }

    private fun setupHueBridgeObservers() {
        HueBridgeManager.state.observe(this, bridgeStateObserver)
        HueBridgeManager.error.observe(this, bridgeErrorObserver)
        HueBlinkController.isBlinking.observe(this, lightsBlinkingObserver)
    }

    private fun setupViewPager() {
        binding.viewPager.adapter = MainTabsAdapter(this)

        TabLayoutMediator(binding.tabLayout, binding.viewPager,
            TabLayoutMediator.TabConfigurationStrategy { tab, position ->
                tab.text = when (position) {
                    MainTabs.CURRENT -> getString(R.string.tab_title_current_games)
                    MainTabs.UPCOMING -> getString(R.string.tab_title_upcoming_games)
                    else -> null
                }
            }
        ).attach()
        binding.tabLayout.addOnTabSelectedListener(viewModel.tabSelectionListener)
    }

    private fun onBridgePushlinking() {
        if (isActivityResumed()) {
            startActivityForResult(HueSetupActivity.newIntent(this), RequestCodes.HUE_SETUP)
        }
    }

    private fun onBridgeConnectedAndInitialized() {
        viewModel.toggleHueStatus(true)
        invalidateOptionsMenu()
    }

    private fun onBridgeIdle() {
        viewModel.toggleHueStatus(false)
        invalidateOptionsMenu()
    }

    private fun onBridgeError(error: HueSdkError) {
        if (isActivityResumed()) {
            when (error) {
                is LightSelectionError -> onLightSelectionError()
                else -> startActivityForResult(
                    HueSetupActivity.newIntent(this),
                    RequestCodes.HUE_SETUP
                )
            }
        }
    }

    private fun onLightSelectionError() {
        viewModel.toggleHueStatus(false)
        Snackbar.make(
            binding.mainContent,
            R.string.snackbar_hue_lights_selection_error,
            Snackbar.LENGTH_LONG
        ).setAction(R.string.snackbar_btn_settings) {
            startActivity(LightsBoardActivity.newIntent(this@MainActivity))
        }.show()
    }

    /**
     * Shows Onboarding screens: TeamList (#1) or Push-Link/BridgeFinder (#2)
     *
     * @return true if MainActivity should call finish()
     */
    private fun showOnboardingIfNecessary(intent: Intent): Boolean {
        if (UserPrefs.isOnboardingCompleted.not()) {
            // Show team list
            startActivity(OnboardingActivity.newIntent(this))

            // This tells MainActivity to finish() itself
            return true
        } else {
            if (intent.getBooleanExtra(BundleKeys.IS_ONBOARDING, false)) {
                val nbAccessPointsFound =
                    intent.getIntExtra(BundleKeys.NB_ACCESS_POINTS, UNKNOWN_VALUE)
                val autoConnect = nbAccessPointsFound == 1

                startActivityForResult(
                    HueSetupActivity.newIntent(
                        context = this,
                        isOnboarding = true,
                        autoConnect = autoConnect
                    ),
                    RequestCodes.HUE_SETUP
                )

                // Clear value to avoid restarting HueSetup on rotation
                intent.removeExtra(BundleKeys.IS_ONBOARDING)
            }
        }

        return false
    }

    private fun checkQuietMode() {
        if (TimeUtils.isCurrentlyQuietHours()) {
            Snackbar.make(
                binding.mainContent,
                R.string.snackbar_currently_quiet_hours,
                Snackbar.LENGTH_LONG
            ).setAction(R.string.snackbar_btn_settings) {
                startActivity(SettingsQuietHoursActivity.newIntent(this@MainActivity))
            }.show()
        }
    }

    private fun toggleHueOptionsItemAnim(enabled: Boolean) {
        when (enabled) {
            true -> hueOptionsItemAnim = AnimatedVectorDrawableCompat
                .create(
                    ContextWrapper(this),
                    R.drawable.hue_discovery_anim
                )?.apply {
                    hueOptionsItem?.icon = this
                    registerAnimationCallback(object : Animatable2Compat.AnimationCallback() {
                        override fun onAnimationEnd(drawable: Drawable?) {
                            start()
                        }
                    })

                    if (hueOptionsItem?.isVisible == true) {
                        start()
                    }
                }
            false -> hueOptionsItemAnim?.clearAnimationCallbacks()
        }
    }

    private fun isActivityResumed(): Boolean =
        lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)

    companion object {
        private val TAG = makeLogTag("MainActivity")

        @ColorInt
        private val MANUAL_BLINK_HUES = listOf(DEFAULT_RGB_COLOR, ALTERNATE_RGB_COLOR)

        fun newIntent(context: Context, isOnboarding: Boolean, nbAccessPoints: Int): Intent {
            val extras = Bundle().apply {
                putBoolean(BundleKeys.IS_ONBOARDING, isOnboarding)
                putInt(BundleKeys.NB_ACCESS_POINTS, nbAccessPoints)
            }

            return Intent(context, MainActivity::class.java).apply {
                putExtras(extras)
            }
        }
    }
}

/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.text.InputType
import android.util.Patterns
import android.webkit.URLUtil
import androidx.preference.EditTextPreference
import androidx.preference.ListPreference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreferenceCompat
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.Const.PrefsNames
import ca.mudar.huegoallight.common.Const.PrefsValues
import ca.mudar.huegoallight.common.Const.USER_PREFS_NAME
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.utils.requirePreference

class SettingsWebhookFragment : PreferenceFragmentCompat() {

    private lateinit var sharedPrefs: SharedPreferences
    private lateinit var masterSwitchPref: SwitchPreferenceCompat
    private lateinit var urlPref: EditTextPreference
    private lateinit var methodPref: ListPreference
    private lateinit var tokenPref: EditTextPreference
    private lateinit var usernamePref: EditTextPreference
    private lateinit var passwordPref: EditTextPreference

    private val prefsChangeListener = SharedPreferences.OnSharedPreferenceChangeListener { _, key ->
        when (key) {
            PrefsNames.WEBHOOK_ENABLED -> setupMasterSwitchTitle()
            PrefsNames.WEBHOOK_URL -> urlPref.summary = getValidUrlSummary()
            PrefsNames.WEBHOOK_AUTH_TYPE -> setupAuthFieldsVisibility()
            PrefsNames.WEBHOOK_PASSWORD -> passwordPref.summary = getMaskedPasswordSummary()
        }
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        sharedPrefs = this.preferenceManager
            .apply {
                sharedPreferencesName = USER_PREFS_NAME
                sharedPreferencesMode = Context.MODE_PRIVATE
            }
            .sharedPreferences

        addPreferencesFromResource(R.xml.prefs_webhook)

        masterSwitchPref = requirePreference(PrefsNames.WEBHOOK_ENABLED)
        urlPref = requirePreference(PrefsNames.WEBHOOK_URL)
        methodPref = requirePreference(PrefsNames.WEBHOOK_METHOD)
        tokenPref = requirePreference(PrefsNames.WEBHOOK_TOKEN)
        usernamePref = requirePreference(PrefsNames.WEBHOOK_USERNAME)
        passwordPref = requirePreference(PrefsNames.WEBHOOK_PASSWORD)

        sharedPrefs.registerOnSharedPreferenceChangeListener(prefsChangeListener)

        setupOnBindEditTextListeners()
        setupMasterSwitchTitle()
        setupSummaries()
        setupAuthFieldsVisibility()
    }

    override fun onDestroy() {
        super.onDestroy()

        sharedPrefs.unregisterOnSharedPreferenceChangeListener(prefsChangeListener)
    }

    private fun setupMasterSwitchTitle() {
        val enabled = sharedPrefs.getBoolean(PrefsNames.WEBHOOK_ENABLED, false)
        masterSwitchPref.title = when (enabled) {
            true -> masterSwitchPref.switchTextOn
            else -> masterSwitchPref.switchTextOff
        }
    }

    private fun setupOnBindEditTextListeners() {
        urlPref.setOnBindEditTextListener { it.setSingleLine() }
        tokenPref.setOnBindEditTextListener { it.setSingleLine() }
        usernamePref.setOnBindEditTextListener { it.setSingleLine() }

        passwordPref.setOnBindEditTextListener { editText ->
            editText.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
            editText.setSingleLine()
        }
    }

    private fun setupSummaries() {
        urlPref.summary = getValidUrlSummary()
        passwordPref.summary = getMaskedPasswordSummary()
    }

    private fun setupAuthFieldsVisibility() {
        val authType = sharedPrefs.getString(
            PrefsNames.WEBHOOK_AUTH_TYPE,
            PrefsValues.DEFAULT_WEBHOOK_AUTH_TYPE
        )
        when (authType) {
            PrefsValues.WEBHOOK_NO_AUTH -> {
                tokenPref.isVisible = false
                usernamePref.isVisible = false
                passwordPref.isVisible = false
            }
            PrefsValues.WEBHOOK_BEARER_TOKEN -> {
                tokenPref.isVisible = true
                usernamePref.isVisible = false
                passwordPref.isVisible = false
            }
            PrefsValues.WEBHOOK_BASIC_AUTH -> {
                tokenPref.isVisible = false
                usernamePref.isVisible = true
                passwordPref.isVisible = true
            }
        }
    }

    private fun getValidUrlSummary(): String {
        val url: String? = sharedPrefs.getString(PrefsNames.WEBHOOK_URL, null)
        return when {
            url.isNullOrBlank() ->
                getString(R.string.prefs_summary_value_not_set)
            URLUtil.isHttpsUrl(url).not() ->
                getString(R.string.prefs_summary_webhook_url_https_required)
            Patterns.WEB_URL.matcher(url).matches().not() ->
                getString(R.string.prefs_summary_webhook_url_invalid)
            else -> url
        }
    }

    private fun getMaskedPasswordSummary(): String {
        val password = sharedPrefs.getString(PrefsNames.WEBHOOK_PASSWORD, null)
        return when {
            password.isNullOrBlank() -> getString(R.string.prefs_summary_value_not_set)
            else -> password.replace(Regex("."), "*")
        }
    }

    companion object {
        private val TAG = makeLogTag("SettingsWebhookFragment")

        fun newInstance(): SettingsWebhookFragment {
            return SettingsWebhookFragment()
        }
    }
}

/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import ca.mudar.huegoallight.R

/**
 * Ref: Daniel Sandler https://code.google.com/archive/p/android-daydream-samples/
 */
class FloatingFrameLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        animation = AnimationUtils.loadAnimation(context, R.anim.fade_in_out)

        jump()
    }

    fun jump() {
        val parent = parent as? View ?: return

        // Set random position using the view's and its parent's dimensions
        x = Math.random().toFloat() * (parent.measuredWidth - measuredWidth)
        y = Math.random().toFloat() * (parent.measuredHeight - measuredHeight)

        startAnimation(animation)
    }
}

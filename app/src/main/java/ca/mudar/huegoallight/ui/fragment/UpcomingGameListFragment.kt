/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.fragment

import android.content.ContextWrapper
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.databinding.FragmentUpcomingGameListBinding
import ca.mudar.huegoallight.fcm.FcmRepo
import ca.mudar.huegoallight.models.FcmEvent
import ca.mudar.huegoallight.models.FcmTopicSubscriptionError
import ca.mudar.huegoallight.models.FcmTopicSubscriptionSuccess
import ca.mudar.huegoallight.prefs.UserPrefs
import ca.mudar.huegoallight.repo.GameRepo
import ca.mudar.huegoallight.ui.adapter.UpcomingGamesAdapter
import ca.mudar.huegoallight.ui.widget.GameItemDecoration
import ca.mudar.huegoallight.viewmodel.UpcomingGameListViewModel

class UpcomingGameListFragment : Fragment() {

    private var binding: FragmentUpcomingGameListBinding? = null
    private val viewModel: UpcomingGameListViewModel by lazy {
        UpcomingGameListViewModel.instanceOf(
            owner = this,
            gameRepo = GameRepo.getInstance(requireContext()),
            teams = UserPrefs.followedNhlTeams
        )
    }

    private val fcmEventListener = Observer<FcmEvent> { fcmEvent ->
        when (fcmEvent) {
            is FcmTopicSubscriptionSuccess,
            is FcmTopicSubscriptionError -> {
                viewModel.updateTeams(UserPrefs.followedNhlTeams)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil
            .inflate(inflater, R.layout.fragment_upcoming_game_list, container, false)

        FcmRepo.event.observe(viewLifecycleOwner, fcmEventListener)

        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    private fun setupRecyclerView() {
        val contextWrapper = ContextWrapper(context)

        val gamesAdapter = UpcomingGamesAdapter(contextWrapper)
        binding?.recycler?.apply {
            layoutManager = LinearLayoutManager(contextWrapper)
            adapter = gamesAdapter
            addItemDecoration(GameItemDecoration(contextWrapper.resources))
        }

        viewModel.games.observe(viewLifecycleOwner, Observer { gameList ->
            gamesAdapter.swapDataset(gameList)

            binding?.progressBar?.visibility = View.GONE
            binding?.emptyGames?.visibility = when {
                gameList.isEmpty() -> View.VISIBLE
                else -> View.GONE
            }
        })
    }

    companion object {
        private val TAG = makeLogTag("UpcomingGameListFragment")

        fun newInstance(): UpcomingGameListFragment {
            return UpcomingGameListFragment()
        }
    }
}

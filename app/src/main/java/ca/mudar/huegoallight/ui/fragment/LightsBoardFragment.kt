/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.Const.FragmentTags
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.databinding.FragmentLightsBoardBinding
import ca.mudar.huegoallight.hue.HueBridgeManager
import ca.mudar.huegoallight.models.HueLight
import ca.mudar.huegoallight.prefs.HuePrefs
import ca.mudar.huegoallight.prefs.UserPrefs
import ca.mudar.huegoallight.ui.dialog.LightsBoardHelpFragment
import ca.mudar.huegoallight.viewmodel.LightsBoardViewModel

class LightsBoardFragment : Fragment() {

    private var binding: FragmentLightsBoardBinding? = null
    private val viewModel by lazy {
        LightsBoardViewModel.instanceOf(owner = this, hueFcmPrefs = HuePrefs)
    }

    private val scrollReadyListener = Observer<Boolean> { isReady ->
        if (isReady) {
            binding?.lightsBoardView?.post {
                binding?.lightsBoardView?.scrollToInitialColumn()
                showDiscoveryIfNecessary()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil
            .inflate(inflater, R.layout.fragment_lights_board, container, false)

        binding?.let {
            it.lifecycleOwner = viewLifecycleOwner
            it.viewModel = viewModel
        }

        viewModel.readyToScroll.observe(viewLifecycleOwner, scrollReadyListener)

        return binding?.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) =
        inflater.inflate(R.menu.menu_help, menu)

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_help) {
            LightsBoardHelpFragment.newInstance()
                .show(parentFragmentManager, FragmentTags.LIGHTS_BOARD_HELP)

            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onPause() {
        super.onPause()

        HuePrefs.setLightSelection(
            bridgeId = HueBridgeManager.currentBridgeId.orEmpty(),
            primaryLights = getLightsIds(viewModel.primaryLights),
            secondaryLights = getLightsIds(viewModel.secondaryLights),
            redLights = getLightsIds(viewModel.redLights)
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    private fun showDiscoveryIfNecessary() {
        if (UserPrefs.isLightsBoardDiscoveryCompleted.not()) {
            activity?.let { fragmentActivity ->
                binding?.lightsBoardView?.showDiscovery(fragmentActivity)?.let { shown ->
                    UserPrefs.isLightsBoardDiscoveryCompleted = shown
                }
            }
        }
    }

    private fun getLightsIds(lights: LiveData<List<HueLight>>): List<String> =
        lights.value?.map { it.id } ?: listOf()

    companion object {
        private val TAG = makeLogTag("LightsBoardFragment")

        fun newInstance(): LightsBoardFragment {
            return LightsBoardFragment()
        }
    }
}

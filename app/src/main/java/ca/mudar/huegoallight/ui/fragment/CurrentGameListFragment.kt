/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.fragment

import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.URLUtil
import androidx.annotation.UiThread
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.Const.FragmentTags
import ca.mudar.huegoallight.common.Const.LIVE_REFRESH_DELAY
import ca.mudar.huegoallight.common.Const.RequestCodes
import ca.mudar.huegoallight.common.LogUtils.LOGV
import ca.mudar.huegoallight.common.LogUtils.makeLogTag
import ca.mudar.huegoallight.databinding.FragmentCurrentGameListBinding
import ca.mudar.huegoallight.fcm.FcmRepo
import ca.mudar.huegoallight.fcm.FcmUtils
import ca.mudar.huegoallight.models.FcmEvent
import ca.mudar.huegoallight.models.FcmTopicSubscriptionError
import ca.mudar.huegoallight.models.FcmTopicSubscriptionSuccess
import ca.mudar.huegoallight.models.Game
import ca.mudar.huegoallight.network.ApiClient
import ca.mudar.huegoallight.prefs.UserPrefs
import ca.mudar.huegoallight.repo.DatabaseHelper
import ca.mudar.huegoallight.repo.GameRepo
import ca.mudar.huegoallight.ui.adapter.CurrentGamesAdapter
import ca.mudar.huegoallight.ui.dialog.DelaySeekBarDialog
import ca.mudar.huegoallight.ui.listener.GameClickListener
import ca.mudar.huegoallight.ui.widget.GameItemDecoration
import ca.mudar.huegoallight.viewmodel.CurrentGameListViewModel

class CurrentGameListFragment : Fragment(), DelaySeekBarDialog.SeekBarCallback {

    private var binding: FragmentCurrentGameListBinding? = null
    private val viewModel: CurrentGameListViewModel by lazy {
        CurrentGameListViewModel.instanceOf(
            owner = this,
            gameRepo = GameRepo.getInstance(requireContext()),
            teams = UserPrefs.followedNhlTeams
        )
    }

    private var gamesIds: String = ""
    private var lastUpdate: Long = 0

    private val gameClickListener = object : GameClickListener {
        override fun onGameClick(context: Context, game: Game) {
            val gameUrl = game.url
            if (URLUtil.isNetworkUrl(gameUrl)) {
                Uri.parse(gameUrl).let {
                    context.startActivity(Intent(Intent.ACTION_VIEW, it))
                }

                FcmUtils.sendAnalyticsGameClickEvent(ContextWrapper(context))
            }
        }

        override fun onMuteMenuItemClick(context: Context, game: Game) {
            // Toggle mute value
            game.isMuted = !game.isMuted

            GameRepo.getInstance(context).updateGame(game)

            FcmUtils.sendAnalyticsGameMuteEvent(ContextWrapper(context))
        }

        override fun onDelayMenuItemClick(game: Game) {
            showSeekBarDialog(game.remoteId, game.delay)

            FcmUtils.sendAnalyticsGameDelayEvent(ContextWrapper(context), game.delay)
        }
    }

    private val fcmEventListener = Observer<FcmEvent> { fcmEvent ->
        when (fcmEvent) {
            is FcmTopicSubscriptionSuccess,
            is FcmTopicSubscriptionError -> {
                viewModel.updateTeams(UserPrefs.followedNhlTeams)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lastUpdate = 0
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil
            .inflate(inflater, R.layout.fragment_current_game_list, container, false)

        FcmRepo.event.observe(viewLifecycleOwner, fcmEventListener)

        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView()
        setupSwipeRefresh()
    }

    override fun onResume() {
        super.onResume()

        updateLiveScoresIfNecessary()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    /**
     * Implements DelaySeekBarDialog.SeekBarChangeListener
     */
    override fun showSeekBarDialog(gameId: String, delay: Int) {
        DelaySeekBarDialog.newInstance(gameId, delay)
            .apply {
                setTargetFragment(this@CurrentGameListFragment, RequestCodes.DELAY_SEEKBAR)
            }
            .show(parentFragmentManager, FragmentTags.DELAY_SEEKBAR)
    }

    /**
     * Implements DelaySeekBarDialog.SeekBarChangeListener
     */
    override fun onSeekBarValueChange(gameId: String, delay: Int) {
        context?.let {
            GameRepo.getInstance(it).updateGameDelay(gameId = gameId, delay = delay)
        }
    }

    private fun setupRecyclerView() {
        val contextWrapper = ContextWrapper(context)

        val gamesAdapter = CurrentGamesAdapter(contextWrapper, gameClickListener)
        binding?.recycler?.apply {
            layoutManager = LinearLayoutManager(contextWrapper)
            adapter = gamesAdapter
            addItemDecoration(GameItemDecoration(contextWrapper.resources))
        }

        viewModel.games.observe(viewLifecycleOwner, Observer { gameList ->
            binding?.progressBar?.visibility = View.GONE
            gamesAdapter.swapDataset(gameList)

            when {
                gameList.isEmpty() -> {
                    binding?.emptyGames?.visibility = View.VISIBLE
                    binding?.swipeRefresh?.isEnabled = false
                }
                else -> {
                    binding?.emptyGames?.visibility = View.GONE
                    binding?.swipeRefresh?.isEnabled = true

                    gamesIds = gameList.joinToString(separator = ",", transform = { it.remoteId })
                    updateLiveScoresIfNecessary()
                }
            }
        })
    }

    private fun setupSwipeRefresh() {
        binding?.swipeRefresh?.setOnRefreshListener {
            updateLiveScores(context, gamesIds)
        }
    }

    private fun updateLiveScores(context: Context?, games: String) {
        if (games.isBlank()) {
            LOGV(TAG, "No live games found")
            toggleRefreshing(false)
            return
        }

        val token = getString(R.string.nhlapi_LiveApiKey)
        ApiClient.getLiveGames(ApiClient.service, token, games) {
            lastUpdate = System.currentTimeMillis()

            try {
                DatabaseHelper.updateGamesScores(context, it)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            toggleRefreshingOnUiThread(false)
        }
    }

    private fun updateLiveScoresIfNecessary() {
        if ((System.currentTimeMillis() - lastUpdate > LIVE_REFRESH_DELAY) &&
            gamesIds.isNotBlank()
        ) {

            toggleRefreshing(true)
            updateLiveScores(context, gamesIds)
        }
    }

    @UiThread
    private fun toggleRefreshing(isRefreshing: Boolean) {
        binding?.swipeRefresh?.isRefreshing = isRefreshing
    }

    private fun toggleRefreshingOnUiThread(isRefreshing: Boolean) {
        try {
            activity?.runOnUiThread { toggleRefreshing(isRefreshing) }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        private val TAG = makeLogTag("CurrentGameListFragment")

        fun newInstance(): CurrentGameListFragment {
            return CurrentGameListFragment()
        }
    }
}

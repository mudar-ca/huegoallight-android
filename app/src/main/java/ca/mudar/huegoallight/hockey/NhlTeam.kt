/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.hockey

import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.Const

class NhlTeam @JvmOverloads constructor(
    val slug: String,
    var goals: Int = 0,
    var shots: Int = 0,
    @ColorInt val color: Int = Const.DEFAULT_RGB_COLOR
) {

    val teamLogo: Int
        @DrawableRes
        get() {
            when (slug) {
                "ANA" -> return R.drawable.logo_ana
                "ARI" -> return R.drawable.logo_ari
                "BOS" -> return R.drawable.logo_bos
                "BUF" -> return R.drawable.logo_buf
                "CAR" -> return R.drawable.logo_car
                "CBJ" -> return R.drawable.logo_cbj
                "CGY" -> return R.drawable.logo_cgy
                "CHI" -> return R.drawable.logo_chi
                "COL" -> return R.drawable.logo_col
                "DAL" -> return R.drawable.logo_dal
                "DET" -> return R.drawable.logo_det
                "EDM" -> return R.drawable.logo_edm
                "FLA" -> return R.drawable.logo_fla
                "LAK" -> return R.drawable.logo_lak
                "MIN" -> return R.drawable.logo_min
                "MTL" -> return R.drawable.logo_mtl
                "NJD" -> return R.drawable.logo_njd
                "NSH" -> return R.drawable.logo_nsh
                "NYI" -> return R.drawable.logo_nyi
                "NYR" -> return R.drawable.logo_nyr
                "OTT" -> return R.drawable.logo_ott
                "PHI" -> return R.drawable.logo_phi
                "PIT" -> return R.drawable.logo_pit
                "SEA" -> return R.drawable.logo_sea
                "SJS" -> return R.drawable.logo_sjs
                "STL" -> return R.drawable.logo_stl
                "TBL" -> return R.drawable.logo_tbl
                "TOR" -> return R.drawable.logo_tor
                "VAN" -> return R.drawable.logo_van
                "VGK" -> return R.drawable.logo_vgk
                "WPG" -> return R.drawable.logo_wpg
                "WSH" -> return R.drawable.logo_wsh
            }

            return R.drawable.empty_drawable
        }

    val teamName: Int
        @StringRes
        get() {
            when (slug) {
                "ANA" -> return R.string.nhl_team_name_ana
                "ARI" -> return R.string.nhl_team_name_ari
                "BOS" -> return R.string.nhl_team_name_bos
                "BUF" -> return R.string.nhl_team_name_buf
                "CAR" -> return R.string.nhl_team_name_car
                "CBJ" -> return R.string.nhl_team_name_cbj
                "CGY" -> return R.string.nhl_team_name_cgy
                "CHI" -> return R.string.nhl_team_name_chi
                "COL" -> return R.string.nhl_team_name_col
                "DAL" -> return R.string.nhl_team_name_dal
                "DET" -> return R.string.nhl_team_name_det
                "EDM" -> return R.string.nhl_team_name_edm
                "FLA" -> return R.string.nhl_team_name_fla
                "LAK" -> return R.string.nhl_team_name_lak
                "MIN" -> return R.string.nhl_team_name_min
                "MTL" -> return R.string.nhl_team_name_mtl
                "NJD" -> return R.string.nhl_team_name_njd
                "NSH" -> return R.string.nhl_team_name_nsh
                "NYI" -> return R.string.nhl_team_name_nyi
                "NYR" -> return R.string.nhl_team_name_nyr
                "OTT" -> return R.string.nhl_team_name_ott
                "PHI" -> return R.string.nhl_team_name_phi
                "PIT" -> return R.string.nhl_team_name_pit
                "SEA" -> return R.string.nhl_team_name_sea
                "SJS" -> return R.string.nhl_team_name_sjs
                "STL" -> return R.string.nhl_team_name_stl
                "TBL" -> return R.string.nhl_team_name_tbl
                "TOR" -> return R.string.nhl_team_name_tor
                "VAN" -> return R.string.nhl_team_name_van
                "VGK" -> return R.string.nhl_team_name_vgk
                "WPG" -> return R.string.nhl_team_name_wpg
                "WSH" -> return R.string.nhl_team_name_wsh
            }

            return R.string.empty_string
        }

    val teamShortName: Int
        @StringRes
        get() {
            when (slug) {
                "ANA" -> return R.string.nhl_team_short_name_ana
                "ARI" -> return R.string.nhl_team_short_name_ari
                "BOS" -> return R.string.nhl_team_short_name_bos
                "BUF" -> return R.string.nhl_team_short_name_buf
                "CAR" -> return R.string.nhl_team_short_name_car
                "CBJ" -> return R.string.nhl_team_short_name_cbj
                "CGY" -> return R.string.nhl_team_short_name_cgy
                "CHI" -> return R.string.nhl_team_short_name_chi
                "COL" -> return R.string.nhl_team_short_name_col
                "DAL" -> return R.string.nhl_team_short_name_dal
                "DET" -> return R.string.nhl_team_short_name_det
                "EDM" -> return R.string.nhl_team_short_name_edm
                "FLA" -> return R.string.nhl_team_short_name_fla
                "LAK" -> return R.string.nhl_team_short_name_lak
                "MIN" -> return R.string.nhl_team_short_name_min
                "MTL" -> return R.string.nhl_team_short_name_mtl
                "NJD" -> return R.string.nhl_team_short_name_njd
                "NSH" -> return R.string.nhl_team_short_name_nsh
                "NYI" -> return R.string.nhl_team_short_name_nyi
                "NYR" -> return R.string.nhl_team_short_name_nyr
                "OTT" -> return R.string.nhl_team_short_name_ott
                "PHI" -> return R.string.nhl_team_short_name_phi
                "PIT" -> return R.string.nhl_team_short_name_pit
                "SEA" -> return R.string.nhl_team_short_name_sea
                "SJS" -> return R.string.nhl_team_short_name_sjs
                "STL" -> return R.string.nhl_team_short_name_stl
                "TBL" -> return R.string.nhl_team_short_name_tbl
                "TOR" -> return R.string.nhl_team_short_name_tor
                "VAN" -> return R.string.nhl_team_short_name_van
                "VGK" -> return R.string.nhl_team_short_name_vgk
                "WPG" -> return R.string.nhl_team_short_name_wpg
                "WSH" -> return R.string.nhl_team_short_name_wsh
            }

            return R.string.empty_string
        }

    val teamNotifyIcon: Int
        @DrawableRes
        get() = HockeyUtils.getTeamNotifyIcon(slug)
}

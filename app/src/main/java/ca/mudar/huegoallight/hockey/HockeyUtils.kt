/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.hockey

import ca.mudar.huegoallight.R
import ca.mudar.huegoallight.common.Const
import java.util.Calendar
import java.util.GregorianCalendar
import java.util.TimeZone

object HockeyUtils {

    /**
     * Returns NHL Eastern conference teams, sorted by city
     */
    val easternConferenceTeams: Array<String>
        get() = arrayOf(
            "BOS",
            "BUF",
            "CAR",
            "CBJ",
            "DET",
            "FLA",
            "MTL",
            "NJD",
            "NYI",
            "NYR",
            "OTT",
            "PHI",
            "PIT",
            "TBL",
            "TOR",
            "WSH"
        )

    /**
     * Returns NHL Western conference teams, sorted by city
     */
    val westernConferenceTeams: Array<String>
        get() = arrayOf(
            "ANA",
            "ARI",
            "CGY",
            "CHI",
            "COL",
            "DAL",
            "EDM",
            "LAK",
            "MIN",
            "NSH",
            "SJS",
            "SEA",
            "STL",
            "VAN",
            "VGK",
            "WPG"
        )

    /**
     * Returns NHL teams, sorted by short name
     */
    val nhlTeams: List<NhlTeam>
        get() {
            val teams = arrayOf(
                "COL",
                "CHI",
                "CBJ",
                "STL",
                "BOS",
                "MTL",
                "VAN",
                "WSH",
                "ARI",
                "NJD",
                "ANA",
                "CGY",
                "PHI",
                "VGK",
                "CAR",
                "NYI",
                "WPG",
                "LAK",
                "SEA",
                "TBL",
                "TOR",
                "EDM",
                "FLA",
                "PIT",
                "NSH",
                "NYR",
                "DET",
                "BUF",
                "OTT",
                "SJS",
                "DAL",
                "MIN"
            )

            return teams.map { NhlTeam(it) }
        }

    fun getTeamNotifyIcon(team: String): Int {
        return R.drawable.ic_notify_default
    }

    /**
     * Check if start time is known. The API sends Midnight (Eastern) for TBD game time
     *
     * @return true if startTime is to-be-determined
     */
    fun isStartTimeTBD(timestamp: Long): Boolean {
        val calendar = GregorianCalendar()
        calendar.timeZone = TimeZone.getTimeZone(Const.EAST_TIMEZONE)
        calendar.timeInMillis = timestamp

        return calendar.get(Calendar.HOUR_OF_DAY) == 0 && calendar.get(Calendar.MINUTE) == 0
    }
}

<?xml version="1.0" encoding="utf-8"?>

<!--
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<resources>
    <string name="app_name">Hue Goal Light</string>

    <!--Activities and Services-->
    <string name="activity_onboarding">Welcome Aboard</string>
    <string name="activity_settings">Settings</string>
    <string name="activity_about">About</string>
    <string name="activity_eula">EULA</string>
    <string name="activity_about_libs">Acknowledgements</string>
    <string name="activity_hue_setup">Hue Bridge Connection</string>
    <string name="activity_lights_board">Blink Setup for Hue Lights</string>
    <string name="activity_nhl_teams_settings">Hockey Teams</string>
    <string name="activity_quiet_hours_settings">Quiet Hours</string>
    <string name="service_daydream">@string/app_name</string>

    <!--Pushlink-->
    <string name="press_pushlink">Press the push-link button on the bridge…</string>
    <string name="pushlink_onboarding_title">Now connect to your Philips&#160;Hue bridge</string>

    <!--BridgeFinder-->
    <string name="prefs_hue_bridge_name">Hue bridge %1$s</string>
    <string name="prefs_hue_bridge_ip_address">IP address: %1$s</string>

    <!--Onboarding-->
    <string name="onboarding_intro">First, select your favourite Hockey&#160;team</string>
    <string name="onboarding_desc">Make your Philips Hue lights blink in celebration of your team’s&#160;goals…</string>

    <!--Menus-->
    <string name="action_hue_setup">Hue setup</string>
    <string name="action_settings">Settings</string>
    <string name="action_help">Help</string>
    <string name="action_about">About</string>
    <string name="action_share">Share</string>
    <string name="action_rate">Rate</string>
    <string name="action_eula">License</string>
    <string name="action_about_libs">Open source libraries</string>
    <string name="action_delay">Game delay</string>
    <string name="action_delay_value">Game delay (%ds)</string>
    <string name="action_mute">Mute</string>

    <!--Main Tabs-->
    <string name="tab_title_current_games">Current</string>
    <string name="tab_title_upcoming_games">Upcoming</string>

    <!--Current games-->
    <string name="empty_current_games">No games scheduled</string>

    <!--Upcoming games-->
    <string name="game_team_vs_team">%1$s vs %2$s</string>
    <string name="empty_upcoming_games">@string/empty_current_games</string>

    <!--Game Delay-->
    <string name="game_delay_seconds">%d s</string>
    <!--Game Delay help-->
    <string name="delay_help_spoiler_title">Antispoiler Game Delay</string>
    <string name="delay_help_spoiler_summary">If the app is faster than your game broadcast or&#160;stream, lights can blink before you see the&#160;goal. To avoid this spoiler, you can delay blinking for up&#160;to 1&#160;minute.\nThis is a per-game setting.</string>
    <string name="delay_help_notify_title">Chronometer</string>
    <string name="delay_help_notify_summary">The app’s notifications include a&#160;chronometer to&#160;help you determine the&#160;best delay value. When lights blink before you see a&#160;goal, check the chronometer on your device at&#160;the moment you see the&#160;late goal on your&#160;TV.</string>

    <!--About-->
    <string name="share_intent_title">Discover Hue Goal Light on Google Play</string>
    <string name="about_subtitle">@string/onboarding_desc</string>
    <string name="about_credits">Android app developed by Mudar&#160;Noufal.</string>
    <string name="about_source_code">Open source project, available on GitLab. Source code is released under the GPLv3 license from the Free Software Foundation.</string>
    <string name="about_logos">The teams logos were redesigned for this app to respect trademarks. They are based mostly on assets from the FreeVector website.</string>
    <string name="about_disclaimer">This app is a fan project and is not affiliated with, endorsed or sponsored by the NHL, the NHL teams or Philips Lighting B.V.</string>
    <string name="about_trademarks_nhl">NHL and the NHL Shield are registered trademarks of the National Hockey League. All NHL logos and marks and NHL team logos and marks depicted herein are the property of the NHL and the respective teams and may not be reproduced without the prior written consent of NHL Enterprises, L.P. ©&#160;NHL. All Right Reserved.</string>
    <string name="about_trademarks_philips">Philips and Philips Hue are registered trademarks of Koninklijke Philips N.V. ©&#160;Philips Lighting B.V. All rights reserved.</string>
    <string name="about_version">Version %s</string>

    <!--Dialogs-->
    <string name="dialog_title_delay">Set antispoiler delay</string>

    <!--Snackbars-->
    <string name="snackbar_pushlink_error">The push-link button on the Hue Bridge was not pressed.</string>
    <string name="snackbar_pushlink_wrong_click">Do you have physical access to a Philips Hue bridge?</string>
    <string name="snackbar_hue_error">Philips Hue error: %s</string>
    <string name="snackbar_fcm_onboarding_error">Firebase Cloud Messaging error, please check your Team settings a bit later.</string>
    <string name="snackbar_fcm_topics_subscribe_error">Could not follow %s. Firebase Cloud Messaging error, please try later.</string>
    <string name="snackbar_fcm_topics_unsubscribe_error">Could not unfollow %s. Firebase Cloud Messaging error, please try later.</string>
    <string name="snackbar_hue_lights_selection_error">No lights are selected</string>
    <string name="snackbar_btn_settings">@string/action_settings</string>
    <string name="snackbar_btn_retry">@string/btn_retry</string>
    <string name="snackbar_no_connection">Internet connection is&#160;unavailable…</string>
    <string name="snackbar_currently_quiet_hours">Currently in the middle of quiet hours.</string>

    <!--Buttons-->
    <string name="btn_ok">OK</string>
    <string name="btn_cancel">Cancel</string>
    <string name="btn_retry">Retry</string>
    <string name="btn_help">Help</string>

    <!--Notifications-->
    <string name="notify_goal_title">Hue Goal!</string>
    <string name="notify_goal_text">%s have scored</string>
    <string name="notify_default_channel">Default</string>
    <string name="notify_silent_channel">Silent notifications</string>

    <!--Lights board-->
    <string name="lights_column_primary">Color #1</string>
    <string name="lights_column_secondary">Color #2</string>
    <string name="lights_column_red">Red</string>
    <string name="lights_column_unused">None</string>
    <string name="lights_status_unreachable">Unreachable</string>
    <!--Lights board help-->
    <string name="lights_discovery_title">Drag &amp; Drop</string>
    <string name="lights_discovery_summary">Move items across the 4&#160;columns to&#160;disable lights or&#160;assign team colors to&#160;each light.</string>
    <string name="lights_help_drag_title">@string/lights_discovery_title</string>
    <string name="lights_help_drag_summary">Move items across the 4&#160;columns to&#160;disable lights (left column) or&#160;assign team primary/secondary colors to&#160;each light. You can also use the original goal light color: red.</string>
    <string name="lights_help_colors_title">Team Colors</string>
    <string name="lights_help_colors_summary">The app has 2&#160;predefined colors for each of the 30&#160;teams.\nEx:&#160;When Montreal Canadiens score, color&#160;#1 lights will blink red while color&#160;#2 lights will blink blue. For St.&#160;Louis Blues, colors are blue/gold.</string>

    <!--Settings-->
    <string name="prefs_cat_notify">Device Notifications</string>
    <string name="prefs_cat_hue">Philips Hue</string>
    <string name="prefs_cat_advanced">Permissions</string>
    <string name="prefs_cat_nhl_east">East coast</string>
    <string name="prefs_cat_nhl_west">West coast</string>

    <string name="prefs_title_hue_blink_duration">Blink duration</string>
    <string name="prefs_summary_hue_blink_duration">%s seconds</string>
    <string name="prefs_dialog_title_hue_blink_duration">Select blink duration</string>
    <string name="prefs_title_hue_selected_lights">Selected lights</string>
    <string name="prefs_summary_hue_selected_lights_none">None</string>
    <string name="prefs_summary_hue_available_lights_error">Error: no lights found</string>
    <string name="prefs_title_hue_bridge">Hue bridge</string>
    <string name="prefs_summary_hue_bridge_none">None</string>
    <string name="prefs_summary_hue_bridge_error">No bridges connected</string>

    <string name="prefs_title_has_notifications">Notifications</string>
    <string name="prefs_title_has_vibration">Vibrate</string>
    <string name="prefs_title_ringtone">Sound</string>
    <string name="prefs_summary_ringtone_silent">Silent</string>

    <string name="prefs_title_battery_optimization">Battery optimization whitelist</string>
    <string name="prefs_summary_battery_optimization">Recommended for notifications reliability</string>

    <string name="prefs_title_teams_followed">Followed teams</string>
    <string name="prefs_title_ignore_opponent">Ignore opponents’ goals</string>
    <string name="prefs_summary_off_ignore_opponent">Bad news travels fast</string>
    <string name="prefs_summary_on_ignore_opponent">No news is good news</string>
    <string name="prefs_summary_teams_followed_none">None</string>

    <string name="prefs_title_master_switch_on">On</string>
    <string name="prefs_title_master_switch_off">Off</string>
    <string name="prefs_summary_value_not_set">Not set</string>

    <string name="prefs_title_quiet_hours">Quiet hours</string>
    <string name="prefs_summary_quiet_hours_on">On</string>
    <string name="prefs_summary_quiet_hours_off">Off</string>
    <string name="prefs_summary_quiet_hours_period">%1$s to %2$s</string>
    <string name="prefs_title_quiet_hours_start">Start time</string>
    <string name="prefs_title_quiet_hours_end">End time</string>
    <string name="prefs_summary_quiet_hours_overnight">%s next day</string>

    <!--Duration Preferences-->
    <string-array name="prefs_blink_duration_entries">
        <item>3 seconds</item>
        <item>6 seconds</item>
        <item>9 seconds</item>
        <item>12 seconds</item>
        <item>15 seconds</item>
    </string-array>

    <!--Snackbar messages-->
    <string name="snackbar_api_error_code">Server error: code %d</string>
    <string name="snackbar_api_error_message">Server error: %1$s [%2$d]</string>
    <string name="snackbar_host_unknown_error_message">Server connection error [%1$d]</string>

    <!--NHL Teams, names-->
    <string name="nhl_team_name_ana">Anaheim Ducks</string>
    <string name="nhl_team_name_ari">Arizona Coyotes</string>
    <string name="nhl_team_name_bos">Boston Bruins</string>
    <string name="nhl_team_name_buf">Buffalo Sabres</string>
    <string name="nhl_team_name_car">Carolina Hurricanes</string>
    <string name="nhl_team_name_cbj">Columbus Blue Jackets</string>
    <string name="nhl_team_name_cgy">Calgary Flames</string>
    <string name="nhl_team_name_chi">Chicago Blackhawks</string>
    <string name="nhl_team_name_col">Colorado Avalanche</string>
    <string name="nhl_team_name_dal">Dallas Stars</string>
    <string name="nhl_team_name_det">Detroit Red Wings</string>
    <string name="nhl_team_name_edm">Edmonton Oilers</string>
    <string name="nhl_team_name_fla">Florida Panthers</string>
    <string name="nhl_team_name_lak">Los Angeles Kings</string>
    <string name="nhl_team_name_min">Minnesota Wild</string>
    <string name="nhl_team_name_mtl">Montreal Canadiens</string>
    <string name="nhl_team_name_njd">New Jersey Devils</string>
    <string name="nhl_team_name_nsh">Nashville Predators</string>
    <string name="nhl_team_name_nyi">New York Islanders</string>
    <string name="nhl_team_name_nyr">New York Rangers</string>
    <string name="nhl_team_name_ott">Ottawa Senators</string>
    <string name="nhl_team_name_phi">Philadelphia Flyers</string>
    <string name="nhl_team_name_pit">Pittsburgh Penguins</string>
    <string name="nhl_team_name_sea">Seattle Kraken</string>
    <string name="nhl_team_name_sjs">San Jose Sharks</string>
    <string name="nhl_team_name_stl">St. Louis Blues</string>
    <string name="nhl_team_name_tbl">Tampa Bay Lightning</string>
    <string name="nhl_team_name_tor">Toronto Maple Leafs</string>
    <string name="nhl_team_name_van">Vancouver Canucks</string>
    <string name="nhl_team_name_vgk">Vegas Golden Knights</string>
    <string name="nhl_team_name_wpg">Winnipeg Jets</string>
    <string name="nhl_team_name_wsh">Washington Capitals</string>

    <!--NHL Teams, short names-->
    <string name="nhl_team_short_name_ott">Senators</string>

    <!--Accessibility-->
    <string name="desc_away_logo">Away team</string>
    <string name="desc_home_logo">Home team</string>
    <string name="desc_logo">Logo</string>
    <string name="desc_nhl_east">@string/prefs_cat_nhl_east</string>
    <string name="desc_nhl_west">@string/prefs_cat_nhl_west</string>
    <string name="desc_btn_retry">@string/btn_retry</string>
    <string name="desc_btn_blink">Blink Hue lights</string>
    <string name="desc_push_link">@string/press_pushlink</string>
    <string name="desc_bridge">Hue bridge</string>
    <string name="desc_drag_handle">Drag and drop handle</string>
    <string name="desc_about_disclaimer">Disclaimer</string>
    <string name="desc_about_copyright_nhl">NHL copyright</string>
    <string name="desc_about_copyright_philips">Philips copyright</string>
    <string name="desc_about_credits">Credits</string>
    <string name="desc_about_source_code">Source code</string>
    <string name="desc_about_logos">Logos</string>
    <string name="desc_card_overflow">Game settings</string>
    <string name="desc_team_list">Team list</string>
    <string name="desc_bridge_list">Hue Bridge list</string>
    <string name="desc_game_list">Game list</string>

</resources>

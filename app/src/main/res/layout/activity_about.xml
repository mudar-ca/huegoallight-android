<?xml version="1.0" encoding="utf-8"?>

<!--
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:bind="http://schemas.android.com/apk/res-auto">

    <data>

        <variable
            name="onClickListener"
            type="android.view.View.OnClickListener" />
    </data>

    <androidx.coordinatorlayout.widget.CoordinatorLayout xmlns:app="http://schemas.android.com/apk/res-auto"
        android:id="@+id/main_content"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:fitsSystemWindows="true">

        <com.google.android.material.appbar.AppBarLayout
            android:id="@+id/appbar"
            android:layout_width="match_parent"
            android:layout_height="256dp"
            android:fitsSystemWindows="true"
            android:theme="@style/ThemeOverlay.MaterialComponents.Dark.ActionBar">

            <com.google.android.material.appbar.CollapsingToolbarLayout
                android:id="@+id/collapsing_toolbar"
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:fitsSystemWindows="true"
                app:contentScrim="?attr/colorPrimary"
                app:expandedTitleMarginEnd="@dimen/activity_horizontal_margin"
                app:expandedTitleMarginStart="@dimen/keyline_3"
                app:layout_scrollFlags="scroll|exitUntilCollapsed"
                app:statusBarScrim="?attr/colorPrimary">

                <ImageView
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:fitsSystemWindows="true"
                    android:importantForAccessibility="no"
                    android:scaleType="centerCrop"
                    android:src="@drawable/header_about"
                    app:layout_collapseMode="parallax" />

                <androidx.appcompat.widget.Toolbar
                    android:id="@+id/toolbar"
                    android:layout_width="match_parent"
                    android:layout_height="?attr/actionBarSize"
                    android:background="@color/transparent"
                    app:layout_collapseMode="pin"
                    app:popupTheme="@style/AppTheme.PopupMenu">

                </androidx.appcompat.widget.Toolbar>

            </com.google.android.material.appbar.CollapsingToolbarLayout>

        </com.google.android.material.appbar.AppBarLayout>

        <androidx.core.widget.NestedScrollView
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            app:layout_behavior="@string/appbar_scrolling_view_behavior">

            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:orientation="vertical"
                android:paddingTop="24dp"
                android:paddingBottom="@dimen/activity_vertical_margin">

                <TextView
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="@dimen/keyline_3"
                    android:layout_marginEnd="@dimen/activity_horizontal_margin"
                    android:layout_marginBottom="16dp"
                    android:lineSpacingMultiplier="1.1"
                    android:text="@string/about_subtitle"
                    android:textColor="@color/color_secondary"
                    android:textSize="18sp"
                    android:textStyle="italic" />

                <LinearLayout
                    style="@style/AboutRow"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:orientation="horizontal">

                    <ImageView
                        android:layout_width="24dp"
                        android:layout_height="24dp"
                        android:contentDescription="@string/desc_about_disclaimer"
                        android:src="@drawable/ic_info"
                        app:tint="?android:textColorPrimary" />

                    <TextView
                        style="@style/AboutContentText"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:text="@string/about_disclaimer" />
                </LinearLayout>

                <LinearLayout
                    style="@style/AboutRow"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:orientation="horizontal">

                    <ImageView
                        android:layout_width="24dp"
                        android:layout_height="24dp"
                        android:contentDescription="@string/desc_about_copyright_nhl"
                        android:src="@drawable/ic_copyright"
                        app:tint="?android:textColorPrimary" />

                    <TextView
                        style="@style/AboutContentText"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:text="@string/about_trademarks_nhl" />
                </LinearLayout>

                <LinearLayout
                    style="@style/AboutRow"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:orientation="horizontal">

                    <ImageView
                        android:layout_width="24dp"
                        android:layout_height="24dp"
                        android:contentDescription="@string/desc_about_copyright_philips"
                        android:src="@drawable/ic_copyright"
                        app:tint="?android:textColorPrimary" />

                    <TextView
                        style="@style/AboutContentText"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:text="@string/about_trademarks_philips" />
                </LinearLayout>

                <View
                    android:layout_width="match_parent"
                    android:layout_height="1dp"
                    android:layout_marginLeft="16dp"
                    android:layout_marginTop="12dp"
                    android:layout_marginRight="16dp"
                    android:layout_marginBottom="12dp"
                    android:background="@color/bg_divider" />

                <LinearLayout
                    android:id="@+id/about_credits"
                    style="@style/AboutRow"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:background="?attr/selectableItemBackground"
                    android:onClick="@{onClickListener}"
                    android:orientation="horizontal">

                    <ImageView
                        android:layout_width="24dp"
                        android:layout_height="24dp"
                        android:contentDescription="@string/desc_about_credits"
                        android:src="@drawable/ic_people"
                        app:tint="@color/color_secondary" />

                    <TextView
                        style="@style/AboutContentText"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:text="@string/about_credits" />
                </LinearLayout>

                <LinearLayout
                    android:id="@+id/about_source_code"
                    style="@style/AboutRow"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:background="?attr/selectableItemBackground"
                    android:onClick="@{onClickListener}"
                    android:orientation="horizontal">

                    <ImageView
                        android:layout_width="24dp"
                        android:layout_height="24dp"
                        android:contentDescription="@string/desc_about_source_code"
                        android:src="@drawable/ic_gitlab_logo"
                        app:tint="@color/color_secondary" />

                    <TextView
                        style="@style/AboutContentText"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:text="@string/about_source_code" />
                </LinearLayout>

                <LinearLayout
                    android:id="@+id/about_logos"
                    style="@style/AboutRow"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:background="?attr/selectableItemBackground"
                    android:onClick="@{onClickListener}"
                    android:orientation="horizontal">

                    <ImageView
                        android:layout_width="24dp"
                        android:layout_height="24dp"
                        android:contentDescription="@string/desc_about_logos"
                        android:src="@drawable/ic_hockey_sticks"
                        app:tint="@color/color_secondary" />

                    <TextView
                        style="@style/AboutContentText"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:text="@string/about_logos" />
                </LinearLayout>

                <TextView
                    android:id="@+id/about_version"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="24dp"
                    android:gravity="center"
                    android:textColor="@color/color_secondary"
                    android:textSize="12sp"
                    android:textStyle="italic"
                    bind:appVersionText="@{@string/about_version}" />

            </LinearLayout>

        </androidx.core.widget.NestedScrollView>

    </androidx.coordinatorlayout.widget.CoordinatorLayout>
</layout>
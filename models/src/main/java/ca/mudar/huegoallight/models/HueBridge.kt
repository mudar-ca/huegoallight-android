/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.models

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

data class HueBridge(
    val id: String,
    val ipAddress: String,
    val token: String? = null
) {
    companion object {
        private val jsonAdapter: JsonAdapter<HueBridge>
            get() = Moshi.Builder()
                .addLast(KotlinJsonAdapterFactory())
                .build()
                .adapter(HueBridge::class.java)

        fun fromJson(json: String): HueBridge? {
            return when {
                json.isNotBlank() -> jsonAdapter.fromJson(json)
                else -> null
            }
        }
    }

    // Note: Hue bridges don't support https :(
    val url: String = "http://$ipAddress/"

    fun toJson(): String {
        return jsonAdapter.toJson(this)
    }
}

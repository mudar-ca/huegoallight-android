/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = ConstDb.Tables.GAMES,
        indices = [Index(value = [ConstDb.GameColumns.REMOTE_ID], unique = true)])
class Game(
    @ColumnInfo(name = ConstDb.GameColumns.REMOTE_ID)
    var remoteId: String,

    @ColumnInfo(name = ConstDb.GameColumns.STARTS_AT, index = true)
    var startsAt: Long,

    @ColumnInfo(name = ConstDb.GameColumns.AWAY_TEAM_SLUG, index = true)
    var awayTeamSlug: String,

    @ColumnInfo(name = ConstDb.GameColumns.HOME_TEAM_SLUG, index = true)
    var homeTeamSlug: String,

    @ColumnInfo(name = ConstDb.GameColumns.AWAY_TEAM_SCORE)
    var awayTeamScore: Int = 0,

    @ColumnInfo(name = ConstDb.GameColumns.HOME_TEAM_SCORE)
    var homeTeamScore: Int = 0,

    @ColumnInfo(name = ConstDb.GameColumns.DELAY)
    var delay: Int = 0,

    @ColumnInfo(name = ConstDb.GameColumns.IS_MUTED)
    var isMuted: Boolean = false,

    @ColumnInfo(name = ConstDb.GameColumns.UPDATED_AT)
    var updatedAt: Long,

    @ColumnInfo(name = ConstDb.GameColumns.URL)
    var url: String
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = ConstDb.GameColumns.ID)
    var id: Long = 0

    fun hasDelay(): Boolean {
        return delay > 0
    }
}

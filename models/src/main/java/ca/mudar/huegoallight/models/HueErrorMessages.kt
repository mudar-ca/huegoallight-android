/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.models

object HueErrorMessages {
    const val BRIDGE_NOT_FOUND = "Bridge not found"
    const val PUSHLINK_EXPIRED = "The push-link button was not pressed"
    const val CONNECTION_ERROR = "Connection error"
    const val HUE_BRIDGE_ERROR = "Hue Bridge error"
    const val EMPTY_LIGHT_SELECTION = "Hue Bridge error"
    const val UNKNOWN_ERROR = "Unknown error"
}

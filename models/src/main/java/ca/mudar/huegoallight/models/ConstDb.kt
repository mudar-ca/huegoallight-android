/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.models

object ConstDb {
    const val DATABASE_NAME = "goallight.db"
    const val DATABASE_VERSION = 7

    object Tables {
        const val GAMES = "games"
    }

    object GameColumns {
        const val ID = "id"
        const val REMOTE_ID = "remote_id"
        const val STARTS_AT = "starts_at"
        const val AWAY_TEAM_SLUG = "away_team_slug"
        const val HOME_TEAM_SLUG = "home_team_slug"
        const val AWAY_TEAM_SCORE = "away_team_score"
        const val HOME_TEAM_SCORE = "home_team_score"
        const val DELAY = "delay"
        const val IS_MUTED = "is_muted"
        const val UPDATED_AT = "updated_at"
        const val URL = "url"
    }
}

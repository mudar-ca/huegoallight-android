plugins {
    id("com.android.library")
    id("kotlin-android")
    id("kotlin-kapt")
}

android {
    compileSdkVersion(Versions.COMPILE_SDK)
    defaultConfig {
        targetSdkVersion(Versions.TARGET_SDK)
        minSdkVersion(Versions.MIN_SDK)
    }
}

dependencies {
    implementation(platform(project(":depconstraints")))

    implementation(Libs.ANDROIDX_ANNOTATION)
    implementation(Libs.ANDROIDX_ROOM_KTX)
    implementation(Libs.SQUARE_MOSHI)
}

plugins {
    id("com.android.library")
    id("kotlin-android")
    id("kotlin-kapt")
}

android {
    compileSdkVersion(Versions.COMPILE_SDK)
    defaultConfig {
        targetSdkVersion(Versions.TARGET_SDK)
        minSdkVersion(Versions.MIN_SDK)
    }
}

dependencies {
    api(platform(project(":depconstraints")))
    kapt(platform(project(":depconstraints")))

    api(Libs.SQUARE_MOSHI)
    api(Libs.SQUARE_RETROFIT_CONVERTER_MOSHI)
    kapt(Libs.SQUARE_MOSHI_CODEGEN)

    implementation(Libs.KOTLIN_STDLIB)
}

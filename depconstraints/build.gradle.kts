plugins {
    id("java-platform")
    id("maven-publish")
}

val androidxAnnotation = "1.2.0"
val androidxAppcompat = "1.3.1"
val androidxArchCore = "2.1.0"
val androidxCardview = "1.0.0"
val androidxConstraintLayout = "2.1.1"
val androidxCore = "1.6.0"
val androidxCoreSplashScreen = "1.0.0-alpha02"
val androidxFragment = "1.3.6"
val androidxLifecycle = "2.3.1"
val androidxPalette = "1.0.0"
val androidxPreference = "1.1.1"
val androidxRecyclerview = "1.2.1"
val androidxRoom = "2.3.0"
val androidxSwipeRefreshLayout = "1.1.0"
val androidxViewpager2 = "1.0.0"
val firebaseBom = "28.4.2"
val firebaseCrashlytics = "18.2.1"
val googlePlayServices = "17.6.0"
val googleMaterial = "1.4.0"
val kotlinxCoroutines = "1.5.2"
//val shade = "1.2.0"
val shade = "feature~validate-token-SNAPSHOT"
// https://square.github.io/leakcanary
val squareLeakCanary = "2.7"
// https://github.com/square/moshi
val squareMoshi = "1.11.0"
// https://github.com/square/okhttp
val squareOkhttp3 = "4.9.2"
// https://github.com/square/okio
val squareOkio = "2.9.0"
// http://square.github.io/retrofit/
val squareRetrofit = "2.9.0"
// UI libraries
// https://github.com/mikepenz/AboutLibraries
val aboutLibraries = "7.0.4@aar"
// https://github.com/woxblom/DragListView
val dragListView = "1.7.2"
// https://github.com/sjwall/MaterialTapTargetPrompt
val tapTargetPrompt = "3.3.0"

dependencies {
    constraints {
        api("${Libs.ANDROIDX_ANNOTATION}:$androidxAnnotation")
        api("${Libs.ANDROIDX_APPCOMPAT}:$androidxAppcompat")
        api("${Libs.ANDROIDX_ARCH_CORE_COMMON}:$androidxArchCore")
        api("${Libs.ANDROIDX_ARCH_CORE_RUNTIME}:$androidxArchCore")
        api("${Libs.ANDROIDX_CARDVIEW}:$androidxCardview")
        api("${Libs.ANDROIDX_CORE}:$androidxCore")
        api("${Libs.ANDROIDX_CORE_KTX}:$androidxCore")
        api("${Libs.ANDROIDX_CORE_SPLASH_SCREEN}:$androidxCoreSplashScreen")
        api("${Libs.ANDROIDX_FRAGMENT}:$androidxFragment")
        api("${Libs.ANDROIDX_FRAGMENT_KTX}:$androidxFragment")
        api("${Libs.ANDROIDX_CONSTRAINT_LAYOUT}:$androidxConstraintLayout")
        api("${Libs.ANDROIDX_LIFECYCLE_LIVEDATA}:$androidxLifecycle")
        api("${Libs.ANDROIDX_PALETTE_KTX}:$androidxPalette")
        api("${Libs.ANDROIDX_PREFERENCE_KTX}:$androidxPreference")
        api("${Libs.ANDROIDX_RECYCLERVIEW}:$androidxRecyclerview")
        api("${Libs.ANDROIDX_ROOM_COMPILER}:$androidxRoom")
        api("${Libs.ANDROIDX_ROOM_KTX}:$androidxRoom")
        api("${Libs.ANDROIDX_ROOM_RUNTIME}:$androidxRoom")
        api("${Libs.ANDROIDX_SWIPE_REFRESH_LAYOUT}:$androidxSwipeRefreshLayout")
        api("${Libs.ANDROIDX_VIEWPAGER2}:$androidxViewpager2")
        api("${Libs.FIREBASE_BOM}:$firebaseBom")
        api("${Libs.GOOGLE_PLAY_SERVICES_BASE}:$googlePlayServices")
        api("${Libs.GOOGLE_MATERIAL}:$googleMaterial")
        api("${Libs.COROUTINES_ANDROID}:$kotlinxCoroutines")
        api("${Libs.COROUTINES_CORE}:$kotlinxCoroutines")
        api("${Libs.SHADE}:$shade")
        api("${Libs.KOTLIN_STDLIB}:${Versions.KOTLIN}")
        api("${Libs.SQUARE_LEAK_CANARY}:$squareLeakCanary")
        api("${Libs.SQUARE_MOSHI}:$squareMoshi")
        api("${Libs.SQUARE_MOSHI_CODEGEN}:$squareMoshi")
        api("${Libs.SQUARE_OKHTTP3}:$squareOkhttp3")
        api("${Libs.SQUARE_OKHTTP3_LOGGING_INTERCEPTOR}:$squareOkhttp3")
        api("${Libs.SQUARE_OKIO}:$squareOkio")
        api("${Libs.SQUARE_RETROFIT}:$squareRetrofit")
        api("${Libs.SQUARE_RETROFIT_CONVERTER_MOSHI}:$squareRetrofit")
        // UI libraries
        api("${Libs.ABOUT_LIBRARIES}:$aboutLibraries")
        api("${Libs.DRAG_LIST_VIEW}:$dragListView")
        api("${Libs.TAP_TARGET_PROMPT}:$tapTargetPrompt")
    }
}

publishing {
    publications {
        create<MavenPublication>("myPlatform") {
            from(components["javaPlatform"])
        }
    }
}

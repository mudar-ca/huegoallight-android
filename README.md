## Hockey Goal Light for Philips Hue

[![Hue Goal Light][img_github]][link_huegoallight_playstore]

### Make your Philips Hue lights blink in celebration of your team’s goals
   
Follow your favourite NHL team, and let your Philips Hue notify you when they score a goal...

Please note that results are not real-time, usual delay is around 30-60 seconds if compared to live TV. There's less delay when streaming the game, or if you are able to pause/rewind your stream (or PVR). And when things are working too well, the app can be a spoiler!

Currently works for the 2016 playoffs. Also hoping to add support for the World Cup of Hockey in September 2016.

## Legal Notice
This is a fan project and is not affiliated with, endorsed or sponsored by the NHL, the NHL teams or Philips Lighting B.V.

NHL and the NHL Shield are registered trademarks of the National Hockey League. All NHL logos and marks and NHL team logos and marks depicted herein are the property of the NHL and the respective teams and may not be reproduced without the prior written consent of NHL Enterprises, L.P. © NHL. All Right Reserved.

Philips and Philips Hue are registered trademarks of Koninklijke Philips N.V. © Philips Lighting B.V. All rights reserved.

## Features
* Built for hockey fans who own a Philips Hue light.
* Follow Multiple NHL teams.
* Blinking Hue lights and device notification when a goal is scored. 
* Diehard fans have the option to ignore goals scored by the enemy!
* Support for Android [Daydream][link_support_daydream].

**Important:** If you don't have a Philips Hue bridge, please do not try to install this app. You will be disappointed as it doesn't do much without Hue lights!

## Links

* [Website][link_huegoallight_website]
* [Privacy policy][link_huegoallight_privacy]
* [Hue Goal Light on Google Play][link_huegoallight_playstore]

[![Android app on Google Play][img_playstore_badge]][link_huegoallight_playstore]

## Credits

* Developed by [Mudar Noufal][link_mudar_ca] &lt;<mn@mudar.ca>&gt;

The Android app includes (thanks!) libraries and derivative work of the following projects:

* [Shade](https://github.com/InkApplications/Shade): &copy; Ink Applications
* [Retrofit](https://square.github.io/retrofit/): retrofit2, adapter-rxjava, converter-gson. &copy; Square Inc.
* [OkHttp](https://square.github.io/okhttp/): okhttp3, logging-interceptor. &copy; Square Inc.
* [Moshi](https://github.com/square/moshi): &copy; Square Inc.
* [Google Play Services](https://github.com/google/gson): Google Cloud Messaging (GCM). &copy; Google Inc.
* [Material Tap Target Prompt](https://github.com/sjwall/MaterialTapTargetPrompt): &copy; Samuel Wall.
* [DragListView](https://github.com/woxblom/DragListView): &copy; Magnus Woxblom.
* [AboutLibraries](https://github.com/mikepenz/AboutLibraries): &copy; Mike Penz.
* [AndroidX](https://developer.android.com/jetpack/androidx/): appcompat, architecture, liveData, Room. &copy; AOSP.
* [AOSP](https://source.android.com/) &copy; The Android Open Source Project.

These libraries are all released under the [Apache License v2.0][link_apache], except for Shade licenced as [MIT][link_shade_license]

## Code license

    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

[![Android app on Google Play][img_devices]][link_huegoallight_playstore]

[link_huegoallight_playstore]: https://play.google.com/store/apps/details?id=ca.mudar.huegoallight
[link_huegoallight_website]: http://huegoallight.mudar.ca/
[link_huegoallight_privacy]: http://huegoallight.mudar.ca/privacy.html
[link_mudar_ca]: https://www.mudar.ca/
[link_gpl]: https://www.gnu.org/licenses/gpl.html
[link_apache]: https://www.apache.org/licenses/LICENSE-2.0
[link_shade_license]: https://raw.githubusercontent.com/InkApplications/Shade/master/LICENSE
[link_support_daydream]: https://support.google.com/nexus/answer/2818748
[img_github]: http://huegoallight.mudar.ca/assets/img/huegoallight_github.png
[img_devices]: http://huegoallight.mudar.ca/assets/img/huegoallight_devices.png
[img_playstore_badge]: http://huegoallight.mudar.ca/assets/img/en_app_rgb_wo_60.png

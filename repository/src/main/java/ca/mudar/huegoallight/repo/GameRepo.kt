/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.repo

import android.content.Context
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import ca.mudar.huegoallight.common.Const.GAME_EXPIRY
import ca.mudar.huegoallight.models.Game
import java.util.concurrent.Executors

class GameRepo private constructor(private val gameDao: GameDao) {

    fun getCurrentGames(teams: List<String>): LiveData<List<Game>> {
        val teamFilter = when {
            teams.isNotEmpty() -> teams.toTypedArray()
            else -> arrayOf("")
        }

        val startOffset = System.currentTimeMillis() - 2 * GAME_EXPIRY
        val endOffset = System.currentTimeMillis() + GAME_EXPIRY

        return gameDao.getCurrentGames(
            teams = teamFilter,
            startOffset = startOffset.toString(),
            endOffset = endOffset.toString()
        )
    }

    fun getUpcomingGames(teams: List<String>): LiveData<List<Game>> {
        if (teams.isEmpty()) {
            return getAllTeamsUpcomingGames()
        }

        val startOffset = System.currentTimeMillis() + GAME_EXPIRY

        return gameDao.getUpcomingGames(
            teams = teams.toTypedArray(),
            startOffset = startOffset.toString()
        )
    }

    private fun getAllTeamsUpcomingGames(): LiveData<List<Game>> {
        val startOffset = System.currentTimeMillis() + GAME_EXPIRY

        return gameDao.getAllTeamsUpcomingGames(startOffset = startOffset.toString())
    }

    fun getGame(gameId: String): LiveData<Game?> {
        return gameDao.getGame(gameId)
    }

    @WorkerThread
    fun getGameSync(gameId: String): Game? {
        return gameDao.getGameSync(gameId)
    }

    fun updateGame(game: Game) {
        // Use executor to avoid accessing database on the main thread
        Executors.newSingleThreadExecutor().execute {
            gameDao.updateGame(game)
        }
    }

    fun updateGameDelay(gameId: String, delay: Int) {
        // Use executor to avoid accessing database on the main thread
        Executors.newSingleThreadExecutor().execute {
            gameDao.setGameDelay(gameId, delay)
        }
    }

    companion object {
        // For Singleton instantiation
        @Volatile
        private var instance: GameRepo? = null

        fun getInstance(context: Context) =
            instance ?: synchronized(this) {
                instance ?: GameRepo(AppDatabase.getInstance(context).gameDao())
                    .also { instance = it }
            }
    }
}

/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.repo

import android.content.Context
import ca.mudar.huegoallight.models.Game
import ca.mudar.huegoallight.models.GameScore
import java.util.concurrent.Executors

object DatabaseHelper {

    fun updateSeasonSchedule(context: Context?, data: List<Game>): Boolean {
        if (data.isEmpty() || context == null) {
            return false
        }

        val db = AppDatabase.getInstance(context)

        var result = false
        db.runInTransaction {
            // Update database and set lastUpdated timestamp
            data.forEach {
                db.gameDao().insertGame(it)
            }
            result = true
        }

        return result
    }

    fun updateGamesScores(context: Context?, data: List<GameScore>) {
        if (data.isEmpty() || context == null) {
            return
        }

        val timestamp = System.currentTimeMillis()
        val db = AppDatabase.getInstance(context)
        val gameDao = db.gameDao()

        Executors.newSingleThreadExecutor().execute {
            db.runInTransaction {
                data.forEach { game ->
                    gameDao.setGameScore(
                        gameId = game.remoteId,
                        awayTeamScore = game.awayTeamScore,
                        homeTeamScore = game.homeTeamScore,
                        updatedAt = timestamp
                    )
                }
            }
        }
    }
}

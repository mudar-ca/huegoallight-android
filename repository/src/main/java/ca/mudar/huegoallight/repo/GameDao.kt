/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.repo

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import ca.mudar.huegoallight.models.ConstDb.GameColumns
import ca.mudar.huegoallight.models.ConstDb.Tables
import ca.mudar.huegoallight.models.Game

@Dao
interface GameDao {
    @Query(
        "SELECT * FROM " + Tables.GAMES +
                " WHERE " + GameColumns.STARTS_AT + " > :startOffset " +
                " AND " + GameColumns.STARTS_AT + " < :endOffset " +
                " AND (" + GameColumns.AWAY_TEAM_SLUG + " IN(:teams) " +
                " OR " + GameColumns.HOME_TEAM_SLUG + " IN(:teams) " +
                " ) " +
                " ORDER BY " + GameColumns.STARTS_AT + " ASC "
    )
    fun getCurrentGames(
        teams: Array<String>,
        startOffset: String,
        endOffset: String
    ): LiveData<List<Game>>

    @Query(
        "SELECT * FROM " + Tables.GAMES +
                " WHERE " + GameColumns.STARTS_AT + " > :startOffset " +
                " AND (" + GameColumns.AWAY_TEAM_SLUG + " IN(:teams) " +
                " OR " + GameColumns.HOME_TEAM_SLUG + " IN(:teams) " +
                " ) " +
                " ORDER BY " + GameColumns.STARTS_AT + " ASC "
    )
    fun getUpcomingGames(teams: Array<String>, startOffset: String): LiveData<List<Game>>

    @Query(
        "SELECT * FROM " + Tables.GAMES +
                " WHERE " + GameColumns.STARTS_AT + " > :startOffset " +
                " ORDER BY " + GameColumns.STARTS_AT + " ASC "
    )
    fun getAllTeamsUpcomingGames(startOffset: String): LiveData<List<Game>>

    @Query(
        "SELECT * FROM " + Tables.GAMES +
                " WHERE " + GameColumns.REMOTE_ID + " = :gameId "
    )
    fun getGame(gameId: String): LiveData<Game?>

    @Query(
        "SELECT * FROM " + Tables.GAMES +
                " WHERE " + GameColumns.REMOTE_ID + " = :gameId "
    )
    fun getGameSync(gameId: String): Game?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertGame(game: Game): Long

    @Update
    fun updateGame(game: Game): Int

    @Query(
        "UPDATE " + Tables.GAMES + " SET " +
                GameColumns.AWAY_TEAM_SCORE + " = :awayTeamScore , " +
                GameColumns.HOME_TEAM_SCORE + " = :homeTeamScore , " +
                GameColumns.UPDATED_AT + " = :updatedAt " +
                " WHERE " + GameColumns.REMOTE_ID + " = :gameId "
    )
    fun setGameScore(gameId: String, awayTeamScore: Int, homeTeamScore: Int, updatedAt: Long): Int

    @Query(
        "UPDATE " + Tables.GAMES + " SET " +
                GameColumns.DELAY + " = :delay" +
                " WHERE " + GameColumns.REMOTE_ID + " = :gameId "
    )
    fun setGameDelay(gameId: String, delay: Int): Int
}

plugins {
    id("com.android.library")
    id("kotlin-android")
    id("kotlin-kapt")
}

android {
    compileSdkVersion(Versions.COMPILE_SDK)
    defaultConfig {
        targetSdkVersion(Versions.TARGET_SDK)
        minSdkVersion(Versions.MIN_SDK)
    }
}

dependencies {
    kapt(platform(project(":depconstraints")))

    implementation(project(":common"))
    implementation(project(":models"))

    kapt(Libs.ANDROIDX_ROOM_COMPILER)
    implementation(Libs.ANDROIDX_ROOM_KTX)
    implementation(Libs.ANDROIDX_ROOM_RUNTIME)
    implementation(Libs.ANDROIDX_LIFECYCLE_LIVEDATA)
}

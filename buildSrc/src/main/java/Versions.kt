/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

object Versions {
    const val VERSION_CODE = 57
    const val VERSION_NAME = "1.9.1"

    const val COMPILE_SDK = 31
    const val TARGET_SDK = 30
    const val MIN_SDK = 23

    const val ANDROID_GRADLE_PLUGIN = "7.0.2"
    const val FIREBASE_CRASHLYTICS_GRADLE = "2.7.1"
    const val GOOGLE_SERVICES = "4.3.10"
    const val KOTLIN = "1.4.21"
}

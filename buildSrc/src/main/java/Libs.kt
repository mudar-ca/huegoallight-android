/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

object Libs {
    const val ANDROIDX_ANNOTATION = "androidx.annotation:annotation"
    const val ANDROIDX_ARCH_CORE_COMMON = "androidx.arch.core:core-common"
    const val ANDROIDX_ARCH_CORE_RUNTIME = "androidx.arch.core:core-runtime"
    const val ANDROIDX_APPCOMPAT = "androidx.appcompat:appcompat"
    const val ANDROIDX_CARDVIEW = "androidx.cardview:cardview"
    const val ANDROIDX_CONSTRAINT_LAYOUT = "androidx.constraintlayout:constraintlayout"
    const val ANDROIDX_CORE = "androidx.core:core"
    const val ANDROIDX_CORE_KTX = "androidx.core:core-ktx"
    const val ANDROIDX_CORE_SPLASH_SCREEN = "androidx.core:core-splashscreen"
    const val ANDROIDX_FRAGMENT = "androidx.fragment:fragment"
    const val ANDROIDX_FRAGMENT_KTX = "androidx.fragment:fragment-ktx"
    const val ANDROIDX_LIFECYCLE_LIVEDATA = "androidx.lifecycle:lifecycle-livedata"
    const val ANDROIDX_PALETTE_KTX = "androidx.palette:palette-ktx"
    const val ANDROIDX_PREFERENCE_KTX = "androidx.preference:preference-ktx"
    const val ANDROIDX_RECYCLERVIEW = "androidx.recyclerview:recyclerview"
    const val ANDROIDX_ROOM_COMPILER = "androidx.room:room-compiler"
    const val ANDROIDX_ROOM_KTX = "androidx.room:room-ktx"
    const val ANDROIDX_ROOM_RUNTIME = "androidx.room:room-runtime"
    const val ANDROIDX_SWIPE_REFRESH_LAYOUT = "androidx.swiperefreshlayout:swiperefreshlayout"
    const val ANDROIDX_VIEWPAGER2 = "androidx.viewpager2:viewpager2"
    const val COROUTINES_ANDROID = "org.jetbrains.kotlinx:kotlinx-coroutines-android"
    const val COROUTINES_CORE = "org.jetbrains.kotlinx:kotlinx-coroutines-core"
    const val FIREBASE_BOM = "com.google.firebase:firebase-bom"
    const val FIREBASE_ANALYTICS_KTX = "com.google.firebase:firebase-analytics-ktx"
    const val FIREBASE_CORE = "com.google.firebase:firebase-core"
    const val FIREBASE_CRASHLYTICS_KTX = "com.google.firebase:firebase-crashlytics-ktx"
    const val FIREBASE_MESSAGING_KTX = "com.google.firebase:firebase-messaging-ktx"
    const val FIREBASE_PERF_KTX = "com.google.firebase:firebase-perf-ktx"
    const val GOOGLE_PLAY_SERVICES_BASE = "com.google.android.gms:play-services-base"
    const val GOOGLE_MATERIAL = "com.google.android.material:material"
    const val KOTLIN_STDLIB = "org.jetbrains.kotlin:kotlin-stdlib"
//    const val SHADE = "com.github.Inkapplications.Shade:shade"
    const val SHADE = "com.github.mudar.Shade:shade"
    const val SQUARE_LEAK_CANARY = "com.squareup.leakcanary:leakcanary-android"
    const val SQUARE_MOSHI = "com.squareup.moshi:moshi-kotlin"
    const val SQUARE_MOSHI_CODEGEN = "com.squareup.moshi:moshi-kotlin-codegen"
    const val SQUARE_OKHTTP3 = "com.squareup.okhttp3:okhttp"
    const val SQUARE_OKHTTP3_LOGGING_INTERCEPTOR = "com.squareup.okhttp3:logging-interceptor"
    const val SQUARE_OKIO = "com.squareup.okio:okio"
    const val SQUARE_RETROFIT = "com.squareup.retrofit2:retrofit"
    const val SQUARE_RETROFIT_CONVERTER_MOSHI = "com.squareup.retrofit2:converter-moshi"
    const val ABOUT_LIBRARIES = "com.mikepenz:aboutlibraries"
    const val DRAG_LIST_VIEW = "com.github.woxthebox:draglistview"
    const val TAP_TARGET_PROMPT = "uk.co.samuelwall:material-tap-target-prompt"
}

plugins {
    id("com.android.library")
    id("kotlin-android")
}

android {
    compileSdkVersion(Versions.COMPILE_SDK)
    defaultConfig {
        targetSdkVersion(Versions.TARGET_SDK)
        minSdkVersion(Versions.MIN_SDK)
    }
}

dependencies {
    implementation(project(":common"))
    implementation(project(":models"))

    api (Libs.ANDROIDX_PREFERENCE_KTX)
}

/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.prefs

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import androidx.preference.PreferenceManager
import ca.mudar.huegoallight.common.Const.PrefsNames
import ca.mudar.huegoallight.common.Const.PrefsValues
import ca.mudar.huegoallight.common.Const.USER_PREFS_NAME
import ca.mudar.huegoallight.models.WebhookAuth
import ca.mudar.huegoallight.models.WebhookBarerTokenAuth
import ca.mudar.huegoallight.models.WebhookBasicAuth
import ca.mudar.huegoallight.models.WebhookConfig
import ca.mudar.huegoallight.models.WebhookNoAuth
import java.util.Locale

object UserPrefs {
    private lateinit var prefs: SharedPreferences

    fun init(app: Application) {
        setDefaultValues(app)
        prefs = app.getSharedPreferences(USER_PREFS_NAME, Context.MODE_PRIVATE)
    }

    private fun edit() = prefs.edit()

    private fun setDefaultValues(context: Context) = PreferenceManager.setDefaultValues(
        context,
        USER_PREFS_NAME,
        Context.MODE_PRIVATE,
        R.xml.prefs_defaults,
        false
    )

    val followedNhlTeams: List<String>
        get() = prefs.getStringSet(PrefsNames.NHL_FOLLOWED_TEAMS, null)?.toList() ?: listOf()

    val ignoreOpponent: Boolean
        get() = prefs.getBoolean(PrefsNames.IGNORE_OPPONENT, true)

    val hasQuietHours
        get() = prefs.getBoolean(
            PrefsNames.QUIET_HOURS_ENABLED,
            PrefsValues.DEFAULT_QUIET_HOURS_ENABLED
        )

    var quietHoursStart: Int
        get() = prefs.getInt(PrefsNames.QUIET_HOURS_START, PrefsValues.DEFAULT_QUIET_HOURS_START)
        set(value) = edit().putInt(PrefsNames.QUIET_HOURS_START, value).apply()

    var quietHoursEnd: Int
        get() = prefs.getInt(PrefsNames.QUIET_HOURS_END, PrefsValues.DEFAULT_QUIET_HOURS_END)
        set(value) = edit().putInt(PrefsNames.QUIET_HOURS_END, value).apply()

    val blinkDuration: Int
        get() = prefs.getString(PrefsNames.BLINK_DURATION, null)?.toIntOrNull()
            ?: PrefsValues.DEFAULT_BLINK_DURATION.toInt()

    val hasNotifications: Boolean
        get() = prefs.getBoolean(PrefsNames.HAS_NOTIFICATIONS, true)

    val hasVibration: Boolean
        get() = prefs.getBoolean(PrefsNames.HAS_VIBRATION, true)

    var ringtoneUri: Uri?
        get() = prefs.getString(PrefsNames.RINGTONE, null)?.let { Uri.parse(it) }
        set(value) = edit().putString(PrefsNames.RINGTONE, value?.toString()).apply()

    val hasWebhook
        get() = prefs.getBoolean(PrefsNames.WEBHOOK_ENABLED, false)

    val webhookConfig: WebhookConfig?
        get() = prefs.getString(PrefsNames.WEBHOOK_URL, null)?.let { url ->
            val webhookAuth: WebhookAuth =
                when (prefs.getString(PrefsNames.WEBHOOK_AUTH_TYPE, null)) {
                    PrefsValues.WEBHOOK_BEARER_TOKEN ->
                        WebhookBarerTokenAuth(prefs.getString(PrefsNames.WEBHOOK_TOKEN, null) ?: "")
                    PrefsValues.WEBHOOK_BASIC_AUTH ->
                        WebhookBasicAuth(
                            username = prefs.getString(PrefsNames.WEBHOOK_USERNAME, null) ?: "",
                            password = prefs.getString(PrefsNames.WEBHOOK_PASSWORD, null) ?: ""
                        )
                    else -> WebhookNoAuth
                }
            WebhookConfig(
                method = prefs.getString(PrefsNames.WEBHOOK_METHOD, null)
                    ?: PrefsValues.DEFAULT_WEBHOOK_METHOD,
                url = url,
                auth = webhookAuth
            )
        }

    val isOnboardingCompleted: Boolean
        get() = prefs.getBoolean(PrefsNames.IS_ONBOARDING, false)

    var isLightsBoardDiscoveryCompleted: Boolean
        get() = prefs.getBoolean(PrefsNames.IS_LIGHTS_DISCOVERY, false)
        set(value) = edit().putBoolean(PrefsNames.IS_LIGHTS_DISCOVERY, value).apply()

    fun isQuietHoursOvernight(): Boolean =
        quietHoursStart >= quietHoursEnd

    fun toggleFollowedTeam(team: String, followed: Boolean) {
        val teams = mutableSetOf<String>().apply {
            // We must not modify the set instance returned by this call
            val followedTeams: Set<String> =
                prefs.getStringSet(PrefsNames.NHL_FOLLOWED_TEAMS, null) ?: setOf()

            addAll(followedTeams)
            when {
                followed -> add(team)
                else -> remove(team)
            }
        }

        edit().putStringSet(PrefsNames.NHL_FOLLOWED_TEAMS, teams).apply()
    }

    /**
     * Enable the native Settings checkbox, add to teamList, and set onboarding flag.
     */
    fun setOnboardingFollowedTeam(team: String) = edit()
        .putBoolean(PrefsNames.IS_ONBOARDING, true)
        .putBoolean(PrefsNames.NHL_TEAM_PREFIX + team.toLowerCase(Locale.getDefault()), true)
        .putStringSet(PrefsNames.NHL_FOLLOWED_TEAMS, setOf(team))
        .apply()
}

/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.prefs

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import ca.mudar.huegoallight.common.Const.HUE_PREFS_NAME
import ca.mudar.huegoallight.common.Const.PrefsNames.BRIDGE
import ca.mudar.huegoallight.common.Const.PrefsNames.BRIDGE_PRIMARY_LIGHTS
import ca.mudar.huegoallight.common.Const.PrefsNames.BRIDGE_RED_LIGHTS
import ca.mudar.huegoallight.common.Const.PrefsNames.BRIDGE_SECONDARY_LIGHTS
import ca.mudar.huegoallight.models.BridgeLightSelection
import ca.mudar.huegoallight.models.HueBridge

object HuePrefs {
    private lateinit var prefs: SharedPreferences

    fun init(app: Application) {
        prefs = app.getSharedPreferences(HUE_PREFS_NAME, Context.MODE_PRIVATE)
    }

    private fun edit() = prefs.edit()

    var lastKnownBridge: HueBridge?
        get() = HueBridge.fromJson(prefs.getString(BRIDGE, null).orEmpty())
        set(value) = edit()
            .putString(BRIDGE, value?.toJson())
            .apply()

    val primaryLights: List<String>
        get() = getBridgeLightSelection(BRIDGE_PRIMARY_LIGHTS)

    val secondaryLights: List<String>
        get() = getBridgeLightSelection(BRIDGE_SECONDARY_LIGHTS)

    val redLights: List<String>
        get() = getBridgeLightSelection(BRIDGE_RED_LIGHTS)

    fun setLightSelection(
        bridgeId: String,
        primaryLights: List<String>,
        secondaryLights: List<String> = listOf(),
        redLights: List<String> = listOf()
    ) = edit()
        .putStringSet(BRIDGE_PRIMARY_LIGHTS.format(bridgeId), primaryLights.toSet())
        .putStringSet(BRIDGE_SECONDARY_LIGHTS.format(bridgeId), secondaryLights.toSet())
        .putStringSet(BRIDGE_RED_LIGHTS.format(bridgeId), redLights.toSet())
        .apply()

    fun isKnownBridge(bridgeId: String): Boolean =
        prefs.contains(BRIDGE_PRIMARY_LIGHTS.format(bridgeId)) ||
                prefs.contains(BRIDGE_SECONDARY_LIGHTS.format(bridgeId)) ||
                prefs.contains(BRIDGE_RED_LIGHTS.format(bridgeId))

    private fun getBridgeLightSelection(template: String): List<String> =
        lastKnownBridge?.id?.let { bridgeId ->
            prefs.getStringSet(template.format(bridgeId), null)?.toList()
        } ?: emptyList()

    fun getBridgeLightSelection(): BridgeLightSelection =
        BridgeLightSelection(primaryLights, secondaryLights, redLights)
}

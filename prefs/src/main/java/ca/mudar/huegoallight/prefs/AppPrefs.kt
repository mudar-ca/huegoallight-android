/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.prefs

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import ca.mudar.huegoallight.common.Const.APP_PREFS_NAME
import ca.mudar.huegoallight.common.Const.PrefsNames.SEASON_UPDATED_AT

object AppPrefs {
    private lateinit var prefs: SharedPreferences

    fun init(app: Application) {
        prefs = app.getSharedPreferences(APP_PREFS_NAME, Context.MODE_PRIVATE)
    }

    private fun edit() = prefs.edit()

    var scheduleLastUpdatedAt: Long
        get() = prefs.getLong(SEASON_UPDATED_AT, 0L)
        set(value) = edit().putLong(SEASON_UPDATED_AT, value).apply()
}
